#include "FlightContextDialog.h"
#include <boost/log/trivial.hpp>
#include "DofInfo.h"
// #include "AircraftsViewModel.h"
// #include "EnabledConfig.h"
using namespace std::chrono;

template<class T> void ShowEnable(T *t) {
    t->Show();
    t->Enable();
}
template<class T> void HideDisable(T *t) {
    t->Hide();
    t->Disable();
}

FlightContextDialog::FlightContextDialog(wxWindow *parent, FlightContext const &flight_context)
    : vfrp::wxfb::FlightContextDialog(parent) {
    dofDatePickerl->SetValue(wxDateTime{system_clock::to_time_t(flight_context.dateOfFlight())}.ToUTC());
    if( flight_context.sunset() )
        sunsetTimePickerl->SetValue(wxDateTime{system_clock::to_time_t(flight_context.sunset().value())}.ToUTC());
    if( flight_context.eobt() )
        eobTimePickerl->SetValue(wxDateTime{system_clock::to_time_t(flight_context.eobt().value())}.ToUTC());
    if( flight_context.origin() ) {
        oQnhSpinner->SetValue(flight_context.origin().value().qnh_.numerical_value_in(u_hPa));
        oTempSpinner->SetValue(flight_context.origin().value().temperature_.numerical_value_in(u_cel));
    }
    if( flight_context.destination() ) {
        dQnhSpinner->SetValue(flight_context.destination().value().qnh_.numerical_value_in(u_hPa));
        dTempSpinner->SetValue(flight_context.destination().value().temperature_.numerical_value_in(u_cel));
    }
    toggleDetailButton->SetValue(flight_context.eobt() && flight_context.origin() && flight_context.destination());
    showHideDetail(flight_context.eobt() && flight_context.origin() && flight_context.destination());
    BOOST_LOG_TRIVIAL(trace) << "constructed FlightContextDialog";
}

FlightContextDialog::~FlightContextDialog() noexcept {
    BOOST_LOG_TRIVIAL(trace) << " destructed FlightContextDialog";
}

FlightContext FlightContextDialog::get() const {
    auto dof    = system_clock::from_time_t(dofDatePickerl->GetValue().FromUTC().GetTicks());
    auto sunset = system_clock::from_time_t(sunsetTimePickerl->GetValue().FromUTC().GetTicks());
    if( toggleDetailButton->GetValue() ) {
        FieldAtmosphere origin{oQnhSpinner->GetValue() * u_hPa, oTempSpinner->GetValue() * u_cel};
        FieldAtmosphere dest{dQnhSpinner->GetValue() * u_hPa, dTempSpinner->GetValue() * u_cel};
        return {dof, system_clock::from_time_t(eobTimePickerl->GetValue().FromUTC().GetTicks()), sunset, origin, dest};
    } else
        return {dof, sunset};
}

void FlightContextDialog::toggleDetail(wxCommandEvent &) {
    showHideDetail(toggleDetailButton->GetValue());
}

void FlightContextDialog::showHideDetail(bool const show) {
    if( show ) {
        ShowEnable(eobTimePickerl);
        ShowEnable(oQnhSpinner);
        ShowEnable(oTempSpinner);
        ShowEnable(dQnhSpinner);
        ShowEnable(dTempSpinner);
        Layout();
        GetSizer()->Fit(this);
    } else {
        HideDisable(eobTimePickerl);
        HideDisable(oQnhSpinner);
        HideDisable(oTempSpinner);
        HideDisable(dQnhSpinner);
        HideDisable(dTempSpinner);
    }
}