#include "ManageAircraftDialog.h"
#include <boost/log/trivial.hpp>
#include "AircraftsViewModel.h"
#include "EnabledConfig.h"

ManageAircraftsDialog::ManageAircraftsDialog(wxWindow *parent, AircraftsViewModel &aircraftsViewModel)
    : vfrp::wxfb::ManageAircraftsDialog(parent)
{
    aircraftsViewCtrl->AssociateModel(&aircraftsViewModel);
    // disnae resize: GetSizer()->Fit(this);
    // TODO: ...
    BOOST_LOG_TRIVIAL(trace) << "constructed ManageAircraftsDialog";
}

ManageAircraftsDialog::~ManageAircraftsDialog() noexcept {
    BOOST_LOG_TRIVIAL(trace) << " destructed ManageAircraftsDialog";
}

void ManageAircraftsDialog::newAircraft( wxCommandEvent& event ) {
    try {
        auto &model = dynamic_cast<AircraftsViewModel&>(*(aircraftsViewCtrl->GetModel()));
        model.newAircraft();
        event.StopPropagation();
    }  catch (std::exception const &ex) {
        throwOverFence(ex.what());
    }
}

void ManageAircraftsDialog::save() {
    auto model = dynamic_cast<AircraftsViewModel*>( aircraftsViewCtrl->GetModel());
    assert(model);
    EnabledConfig config{*(wxConfigBase::Get())};
    auto marker = config.setPath(CONFKEY_AIRCRAFTDATA);
    model->save(config.configBase());
}

wxObjectDataPtr<AircraftsViewModel> ManageAircraftsDialog::aircraftsViewModel() {
    EnabledConfig config{*(wxConfigBase::Get())};
    auto marker = config.setPath(CONFKEY_AIRCRAFTDATA);
    return wxObjectDataPtr<AircraftsViewModel>{new AircraftsViewModel{config.configBase()}};
}

