#include "ManageAirfieldsDialog.h"
#include <wx/object.h>
#include "AirfieldsViewModel.h"
#include "EnabledConfig.h"
#include <algorithm>
#include "plain-values.h"

ManageAirfieldsDialog::ManageAirfieldsDialog(wxWindow *parent, AirfieldsViewModel &airfields_model)
    : vfrp::wxfb::ManageAirfieldsDialog(parent) {
    airfieldsViewCtrl->AssociateModel(&airfields_model);
    // disnae resize: GetSizer()->Fit(this);
    // TODO: ...?
}

void ManageAirfieldsDialog::save() {
    auto &model = dynamic_cast<AirfieldsViewModel&>( *(airfieldsViewCtrl->GetModel()));
    EnabledConfig config{*(wxConfigBase::Get())};
    auto marker = config.setPath(CONFKEY_AIRFIELDSDATA);
    model.save(config.configBase());
}

wxObjectDataPtr<AirfieldsViewModel> ManageAirfieldsDialog::airfielsdViewModel() {
    EnabledConfig config{*(wxConfigBase::Get())};
    auto marker = config.setPath(CONFKEY_AIRFIELDSDATA);
    return wxObjectDataPtr<AirfieldsViewModel>{&AirfieldsViewModel::instance(config.configBase())};
}

void ManageAirfieldsDialog::icaoChanged(wxCommandEvent &event) {
    textCtrlChanged(icaoTextCtrl->GetValue(),AirfieldsViewModel::Columns::icao);
    event.StopPropagation();
}

void ManageAirfieldsDialog::nameChanged(wxCommandEvent &event) {
    textCtrlChanged(nameTextCtrl->GetValue(),AirfieldsViewModel::Columns::name);
    event.StopPropagation();
}

void ManageAirfieldsDialog::elevChanged(wxSpinEvent &event) {
    numberCtrlChanged(elevSpinCtrl->GetValue(),AirfieldsViewModel::Columns::elev);
    event.StopPropagation();
}

void ManageAirfieldsDialog::rwyChanged(wxSpinEvent &event) {
    numberCtrlChanged(rwySpinCtrl->GetValue(),AirfieldsViewModel::Columns::rwy);
    rwyOppositeLabel->SetLabelText(wxString{"/ "}+std::to_string(18+rwySpinCtrl->GetValue()));
    event.StopPropagation();
}

void ManageAirfieldsDialog::lengthChanged(wxSpinEvent &event) {
    numberCtrlChanged(lengthSpinCtrl->GetValue(),AirfieldsViewModel::Columns::rwyLength);
    event.StopPropagation();
}

void ManageAirfieldsDialog::surfaceChanged(wxCommandEvent &event) {
    textCtrlChanged(surfaceCombo->GetValue(),AirfieldsViewModel::Columns::surfaceName);
    event.StopPropagation();
}

void ManageAirfieldsDialog::toraChanged(wxSpinEvent &event) {
    numberCtrlChanged(toraSpinCtrl->GetValue(),AirfieldsViewModel::Columns::tora);
    event.StopPropagation();
}

void ManageAirfieldsDialog::ldaChanged(wxSpinEvent &event) {
    numberCtrlChanged(ldaSpinCtrl->GetValue(),AirfieldsViewModel::Columns::lda);
    event.StopPropagation();
}

void ManageAirfieldsDialog::revToraChanged(wxSpinEvent &event) {
    numberCtrlChanged(revToraSpinCtrl->GetValue(),AirfieldsViewModel::Columns::revTora);
    event.StopPropagation();
}

void ManageAirfieldsDialog::revLdaChanged(wxSpinEvent &event) {
    numberCtrlChanged(revLdaSpinCtrl->GetValue(),AirfieldsViewModel::Columns::revLda);
    event.StopPropagation();
}

void ManageAirfieldsDialog::radioToggeled(wxCommandEvent &event) {
    try {
        assert(uncontrolledRB->GetValue()^controlledRB->GetValue() && "radio RadioButtons must always have opposite values (F,T or T,F)");
        wxSpinEvent spinnert;
        wxCommandEvent plem;
        auto &model = dynamic_cast<AirfieldsViewModel&>( *(airfieldsViewCtrl->GetModel()));
        if(uncontrolledRB->GetValue()) {
            model.ChangeValue(AirfieldsViewModel::RadioType::Radio,selectedItem,AirfieldsViewModel::Columns::radioType);
            radioNameChanged(plem);
            radioChanged(spinnert);
        } else {
            model.ChangeValue(AirfieldsViewModel::RadioType::Atc,selectedItem,AirfieldsViewModel::Columns::radioType);
            approachChanged(spinnert);
            towerChanged(spinnert);
            groundChanged(spinnert);
        }
        xableRadioCtrls(controlledRB->GetValue());
    }  catch (std::exception const &ex) {
        throwOverFence(ex.what());
    }
}

void ManageAirfieldsDialog::radioNameChanged(wxCommandEvent &event) {
    textCtrlChanged(radioNameCombo->GetValue(),AirfieldsViewModel::Columns::radioName);
    event.StopPropagation();
}
void ManageAirfieldsDialog::radioChanged(wxSpinEvent &event) {
    numberCtrlChanged(radioFreqSpinCtrl->GetValue(),AirfieldsViewModel::Columns::radioFreq);
    event.StopPropagation();
}
void ManageAirfieldsDialog::approachChanged(wxSpinEvent &event) {
    numberCtrlChanged(approachFreqSpinCtrl->GetValue(),AirfieldsViewModel::Columns::apprFreq);
    event.StopPropagation();
}
void ManageAirfieldsDialog::towerChanged(wxSpinEvent &event) {
    numberCtrlChanged(towerFreqSpinCtrl->GetValue(),AirfieldsViewModel::Columns::towerFreq);
    event.StopPropagation();
}
void ManageAirfieldsDialog::groundChanged(wxSpinEvent &event) {
    numberCtrlChanged(groundFreqSpinCtrl->GetValue(),AirfieldsViewModel::Columns::gndFreq);
    event.StopPropagation();
}
void ManageAirfieldsDialog::fisChanged(wxSpinEvent &event) {
    numberCtrlChanged(fisFreqSpinCtrl->GetValue(),AirfieldsViewModel::Columns::fisFreq);
    event.StopPropagation();
}

void ManageAirfieldsDialog::airfieldSelected(wxDataViewEvent &event) {
    try {
        assert( event.GetModel() == airfieldsViewCtrl->GetModel() );
        auto const &model = dynamic_cast<AirfieldsViewModel&>( *(event.GetModel()));
        auto item = event.GetItem();
        if(item.IsOk()) {
            selectedItem = item;
            updateDetails(model.verifyAirfield(selectedItem));
            event.StopPropagation();
        }
    }  catch (std::exception const &ex) {
        throwOverFence(ex.what());
    }
}

void ManageAirfieldsDialog::textCtrlChanged(wxString const new_text, unsigned int const model_col) {
    try {
        auto &model = dynamic_cast<AirfieldsViewModel&>( *(airfieldsViewCtrl->GetModel()));
        model.ChangeValue(new_text,selectedItem,model_col);
    }  catch (std::exception const &ex) {
        throwOverFence(ex.what());
    }
}

void ManageAirfieldsDialog::numberCtrlChanged(long const new_number, unsigned int const model_col) {
    try {
        auto &model = dynamic_cast<AirfieldsViewModel&>( *(airfieldsViewCtrl->GetModel()));
        model.ChangeValue(new_number,selectedItem,model_col);
    }  catch (std::exception const &ex) {
        throwOverFence(ex.what());
    }
}

void ManageAirfieldsDialog::updateDetails(std::shared_ptr<Airfield const> field) {
    wxSpinEvent dummy{};
    icaoTextCtrl->ChangeValue(field->icaoCode());
    nameTextCtrl->ChangeValue(field->name());
    elevSpinCtrl->SetValue(plainFeetMSL<int>(field->elevation()));
    rwySpinCtrl->SetValue(field->rwyCode());
    rwyChanged(dummy);
    lengthSpinCtrl->SetValue(plainMetre<int>(field->rwyLength()));
    surfaceCombo->ChangeValue(Airfield::rwySurface(field->surface()));
    toraSpinCtrl->SetValue(plainMetre<int>(field->tora(false)));
    ldaSpinCtrl->SetValue(plainMetre<int>(field->lda(false)));
    revToraSpinCtrl->SetValue(plainMetre<int>(field->tora(true)));
    revLdaSpinCtrl->SetValue(plainMetre<int>(field->lda(true)));
    uncontrolledRB->SetValue(!(field->hasATC()));
    controlledRB->SetValue(field->hasATC());
    xableRadioCtrls(field->hasATC());
    if(field->hasATC()) {
        approachFreqSpinCtrl->SetValue(plainKHz<int>(field->atcRadio().approach_));
        towerFreqSpinCtrl->SetValue(plainKHz<int>(field->atcRadio().tower_));
        groundFreqSpinCtrl->SetValue(plainKHz<int>(field->atcRadio().ground_));
    } else {
        radioNameCombo->ChangeValue(InfoRadio::infoCallsign(field->infoRadio().callsign_));
        radioFreqSpinCtrl->SetValue(plainKHz<int>(field->infoRadio().frequency_));
    }
    fisFreqSpinCtrl->SetValue(plainKHz<int>(field->fisFreq()));
}

void ManageAirfieldsDialog::xableRadioCtrls(bool const has_atc) {
    radioNameCombo->Enable(!has_atc);
    radioFreqSpinCtrl->Enable(!has_atc);
    approachFreqSpinCtrl->Enable(has_atc);
    towerFreqSpinCtrl->Enable(has_atc);
    groundFreqSpinCtrl->Enable(has_atc);
}
