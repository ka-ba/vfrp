#include "AircraftsViewModel.h"
#include <algorithm>
#include <boost/log/trivial.hpp>
#include "EnabledConfig.h"
#include "plain-values.h"

wxRegEx const Aircraft::regEx{"[^a-zA-Z0-9]"};

Aircraft::Aircraft(wxConfigBase const &config, const wxString &entry_id)
    : entryID_(entry_id),
      callSign_(config.Read(entry_id+"/callsign","[UNDEFINED]")),
      model_(config.Read(entry_id+"/model","[UNDEFINED]")),
      climbIAS_(fromKnots(config.Read(entry_id+"/climbias",1))),
      cruisingIAS_(fromKnots(config.Read(entry_id+"/cruisingias",1))),
      climbConsumption_(fromlZh(EnabledConfig::Read(config,entry_id+"/climbconsumption",1.))), // TODO: find out why EnabledConfig
      cruisingConsumption_(fromlZh(EnabledConfig::Read(config,entry_id+"/cruisingconsumption",1.))) {
    assert(regEx.IsValid()&&"aircraft regex value");
}

Aircraft::Aircraft()
    : entryID_("MUST BE MADE UNIQUE"),
      callSign_("[UNDEFINED]"),
      model_("[UNDEFINED]"),
      climbIAS_(1*u_kt),
      cruisingIAS_(1*u_kt),
      climbConsumption_(9999*u_l_per_h),
      cruisingConsumption_(9999*u_l_per_h) {
    assert(regEx.IsValid()&&"aircraft regex value");

}

wxString const &Aircraft::callSign() const noexcept {
    return callSign_;
}

wxString const &Aircraft::model() const noexcept {
    return model_;
}

speed_t Aircraft::climbIAS() const noexcept {
    return climbIAS_;
}

consumption_t Aircraft::climbConsumption() const noexcept {
    return climbConsumption_;
}

speed_t Aircraft::cruisingIAS() const noexcept {
    return cruisingIAS_;
}

consumption_t Aircraft::cruisingConsumption() const noexcept {
    return cruisingConsumption_;
}

void Aircraft::callSign(wxString const &call_sign) {
    callSign_ = call_sign;
}

void Aircraft::model(wxString const &m_odel) {
    model_ = m_odel;
}

void Aircraft::climbIAS(speed_t const climb_speed) noexcept {
    climbIAS_ = climb_speed;
}

void Aircraft::climbConsumption(consumption_t const climb_ff) noexcept {
    climbConsumption_ = climb_ff;
}

void Aircraft::cruisingIAS(speed_t const cruising_speed) noexcept {
    cruisingIAS_ = cruising_speed;
}

void Aircraft::cruisingConsumption(consumption_t const cruising_ff) noexcept {
    cruisingConsumption_ = cruising_ff;
}

void Aircraft::save(wxConfigBase &config) const {
    config.Write(entryID_+"/callsign",callSign_);
    config.Write(entryID_+"/model",model_);
    config.Write(entryID_+"/climbias",plainKnots(climbIAS_)); // TODO: use templated???
    config.Write(entryID_+"/cruisingias",plainKnots(cruisingIAS_));
    config.Write(entryID_+"/climbconsumption",plainLZh(climbConsumption_));
    config.Write(entryID_+"/cruisingconsumption",plainLZh(cruisingConsumption_));
}

// duplicated code :(
void Aircraft::improveEntryId(std::set<wxString> &ids) {
    [[maybe_unused]] auto before = entryID_;
    entryID_ = callSign_;
    regEx.Replace(&entryID_,"");
    entryID_.UpperCase();
    while(ids.end() != ids.find(entryID_))
        entryID_ += "X";
    ids.insert(entryID_);
    BOOST_LOG_TRIVIAL(debug) << "improved entry id from " << before << " to " << entryID_;
}

AircraftsViewModel::AircraftsViewModel(wxConfigBase const &config)
    : hasChanges(false) {
    long cookie{};
    wxString entry_id;
    auto got_item = config.GetFirstGroup(entry_id,cookie);
    while(got_item) {
        crafts.emplace_back(config,entry_id);
        got_item = config.GetNextGroup(entry_id,cookie);
    }
    if(crafts.empty()) { // don't allow empty model to kick-start at frist use
        crafts.emplace_back();
        crafts.front().callSign("D-UMMY");
    }
}

void AircraftsViewModel::GetValue(wxVariant &variant, wxDataViewItem const &item, unsigned int const col) const {
    if(item.IsOk()) {
        auto const &craft = verifyAircraft(item);
        switch(col) {
        case 0:
            variant = craft.callSign();
            break;
        case 1:
            variant = craft.model();
            break;
        case 2:
            variant = plainKnots<long>(craft.climbIAS());
            break;
        case 3:
            // FIXME: why is this str?
            variant = std::to_string(plainLZh(craft.climbConsumption()));
            break;
        case 4:
            variant = plainKnots<long>(craft.cruisingIAS());
            break;
        case 5:
            variant = std::to_string(plainLZh(craft.cruisingConsumption()));
            break;
        default:
            variant = "[RANGE]";
        }
    } else {
    variant = "[INVALID]";
    }
}

bool AircraftsViewModel::SetValue(wxVariant const &variant, wxDataViewItem const &item, unsigned int col) {
    if(item.IsOk()) {
        auto &craft = verifyAircraft(item);
        switch(col) {
        case 0:
            craft.callSign(variant.GetString());
            break;
        case 1:
            craft.model(variant.GetString());
            break;
        case 2:
            craft.climbIAS(fromKnots(variant.GetLong()));
            break;
        case 3:
            craft.climbConsumption(fromlZh(variant.GetDouble()));
            BOOST_LOG_TRIVIAL(trace) << "set climb consuption to "<< craft.climbConsumption() << " (from " << variant.GetDouble() << " aka " << variant.GetString() << " )";
            break;
        case 4:
            craft.cruisingIAS(fromKnots(variant.GetLong()));
            break;
        case 5:
            craft.cruisingConsumption(fromlZh(variant.GetDouble()));
            break;
        default:
            return false;
        }
        hasChanges=true;
        return true;
    }
    return false;
}

bool AircraftsViewModel::IsContainer(wxDataViewItem const &item) const {
    return !item.IsOk();
}

unsigned int AircraftsViewModel::GetChildren(wxDataViewItem const &item, wxDataViewItemArray &children) const {
    children.clear();
    if(item.IsOk())
        return 0;
    // a subtle const error in wxWidgets API leads to const_cast here (1/3)
    for( Aircraft &craft : const_cast<std::deque<Aircraft>&>(crafts) )
        children.Add(wxDataViewItem{reinterpret_cast<void*>(&craft)});
    return crafts.size();
}

void AircraftsViewModel::newAircraft() {
    [[maybe_unused]] auto before = crafts.size();
    crafts.emplace_back();
    ItemAdded(wxDataViewItem{},wxDataViewItem{reinterpret_cast<void*>(&crafts.back())});
    hasChanges=true;
    BOOST_LOG_TRIVIAL(debug) << "aircraft added (before->after) : " << before << " -> " << crafts.size();
}

wxDataViewItem AircraftsViewModel::firstItem() const {
    assert(!crafts.empty());
    // a subtle const error in wxWidgets API leads to const_cast here (2/3)
    return wxDataViewItem{reinterpret_cast<void*>(&const_cast<Aircraft&>(crafts.front()))};
}

wxDataViewItem AircraftsViewModel::item(wxString const &callsign) const {
    auto const iter = std::find_if(crafts.cbegin(),crafts.cend(),[&callsign](auto const &craft){return craft.callSign()==callsign;});
    if(crafts.cend()!=iter)
        // a subtle const error in wxWidgets API leads to const_cast here (3/3)
        return wxDataViewItem{reinterpret_cast<void*>(&const_cast<Aircraft&>(*iter))};
    return {};
}

void AircraftsViewModel::save(wxConfigBase &config) {
    if(hasChanges) {
        std::set<wxString> ids;
        EnabledConfig::deleteAllGroups(config);
        for( auto &craft : crafts ) {
            craft.improveEntryId(ids);
            craft.save(config);
        }
        hasChanges=false;
        config.Flush();
        BOOST_LOG_TRIVIAL(info) << "saved aircrafts";
    } else {
        BOOST_LOG_TRIVIAL(debug) << "aircrafts not saved because nothing changed";
    }
}

Aircraft const &AircraftsViewModel::verifyAircraft(wxDataViewItem const &item) const {
    return verifyAircraftImpl(item);
}
Aircraft &AircraftsViewModel::verifyAircraft(wxDataViewItem const &item) {
    return verifyAircraftImpl(item);
}
Aircraft &AircraftsViewModel::verifyAircraftImpl(wxDataViewItem const &item) const {
    auto iter = std::find_if(crafts.cbegin(),crafts.cend(), [&item](auto const &ac){
        return reinterpret_cast<Aircraft*>(item.GetID()) == &ac;
    });
    if(crafts.cend() == iter)
        throw std::logic_error{"requested aircraft not in vector"};
    // const cast because function is accessed through access functions which handle const-nes
    return const_cast<Aircraft&>(*iter);
}
