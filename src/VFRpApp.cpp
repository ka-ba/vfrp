#include "VFRpApp.h"
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include "MainFrame.h"
#include <wx/msgdlg.h>

wxIMPLEMENT_APP(VFRpApp);

bool VFRpApp::OnInit()
{
    try {
//    auto main_frame = new MainFrame(wxPoint{50,50},wxSize{500,500});
#ifdef NDEBUG
        boost::log::core::get()->set_filter(
                    boost::log::trivial::severity >= boost::log::trivial::debug);
#endif
    SetAppName("vfrp");
    // don't use a smart pointer here;
    // object obviously has to outlive this function when returning true; should be deleted by WX in that case...
    topFrame = new TopFrame();
    SetTopWindow(topFrame);
    topFrame->Show();
    }  catch (std::exception const &e) {
        BOOST_LOG_TRIVIAL(fatal) << "exception in application initialisation: " << e.what();
        boost::log::core::get()->flush();
        if(topFrame)
            delete topFrame;
        topFrame=nullptr;
        return false;
    }
    return true;
}

bool VFRpApp::OnExceptionInMainLoop() {
    try { throw; }
    catch(std::exception &e) {
        wxMessageBox(wxString{e.what()},"C++ exception caught",wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTRE,nullptr);
    }
    return true;   // continue on. Return false to abort program
}
