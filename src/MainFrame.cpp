#include "MainFrame.h"
#include <fstream>
#include <boost/json.hpp>
#include <boost/log/trivial.hpp>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/stdpaths.h>
// #include <wx/sizer.h>
// #include <wx/textctrl.h>
#include "AircraftsViewModel.h"
#include "AirfieldsViewModel.h"
#include "EnabledConfig.h"
#include "FlightContextDialog.h"
#include "FlightPathPanel.h"
#include "FuelPanel.h"
#include "ManageAircraftDialog.h"
#include "ManageAirfieldsDialog.h"
#include "plain-values.h"
#include "util.h"
#include "vfrp-types.h"
constexpr char const *KEY_MAINFRAME_POS  = "GUI/mainframe/pos";
constexpr char const *KEY_MAINFRAME_SIZE = "GUI/mainframe/size";

TopFrame::TopFrame(wxWindow *parent) : vfrp::wxfb::MainFrame(parent) {
    try {
        logBasicInformation();
        BOOST_LOG_TRIVIAL(trace) << "TopFrame: " << this;
        BOOST_LOG_TRIVIAL(trace) << "notebook: " << m_notebook1 << "  - parent: " << m_notebook1->GetParent();
        BOOST_LOG_TRIVIAL(trace) << "frontPanel: " << frontPanel << "  - parent: " << frontPanel->GetParent();
        BOOST_LOG_TRIVIAL(trace) << "frontPanel: " << pathPanel << "  - parent: " << pathPanel->GetParent();
        EnabledConfig config{*(wxConfigBase::Get())};
        airfieldsViewModel = ManageAirfieldsDialog::airfielsdViewModel();
        fromViewCtrl->AssociateModel(airfieldsViewModel.get());
        toViewCtrl->AssociateModel(airfieldsViewModel.get());
        altViewCtrl->AssociateModel(airfieldsViewModel.get());
        // toViewCtrl->setsort???
        aircraftViewModel                             = ManageAircraftsDialog::aircraftsViewModel();
        AircraftsViewModel const *aircraft_view_model = aircraftViewModel.get();
        signViewCtrl->AssociateModel(aircraftViewModel.get());
        auto const first_craft = aircraftViewModel->firstItem();
        signViewCtrl->Select(first_craft);
        selectAircraft(first_craft);
        SetPosition(config.point(KEY_MAINFRAME_POS, wxDefaultPosition));
        SetSize(config.size(KEY_MAINFRAME_SIZE, wxDefaultSize));
        pathPanel->init(airfieldsViewModel->airfieldOrDummy("Westrauderfehn"),
                        airfieldsViewModel->airfieldOrDummy("Ostrauderfehn"),
                        aircraft_view_model->verifyAircraft(first_craft));
        dofButtonFgCol    = dofButton->GetForegroundColour();
        sunsetButtonFgCol = sunsetButton->GetForegroundColour();
        BOOST_LOG_TRIVIAL(trace) << "done constructing MainFrame";
        boost::log::core::get()->flush();
    } catch( std::exception const &e ) {
        BOOST_LOG_TRIVIAL(fatal) << "exception in MainFrame construction: " << e.what();
        boost::log::core::get()->flush();
    }
}

TopFrame::~TopFrame() noexcept = default;

void TopFrame::flightPathUpdated(FlightPath const &flight_path) {
    assert(selectedAircraft);
    auto safe_duration = fuelPanel->update(flight_path, *selectedAircraft);
    pathPanel->updateSafeDuration(safe_duration);  // FIXME: have this update upon fuel-only changes as well!
}

void TopFrame::onMove(wxMoveEvent &event) {
    EnabledConfig config{*(wxConfigBase::Get())};
    config.storePoint(KEY_MAINFRAME_POS, event.GetPosition());
    event.Skip();
}

void TopFrame::onSize(wxSizeEvent &event) {
    EnabledConfig config{*(wxConfigBase::Get())};
    config.storeSize(KEY_MAINFRAME_SIZE, event.GetSize());
    event.Skip();
}

void TopFrame::editContext(wxCommandEvent &) {
    try {
        FlightContextDialog flight_context_dialog(this, dofInfo_);
        auto const action = flight_context_dialog.ShowModal();
        if( wxID_OK == action ) dofInfo(flight_context_dialog.get());
        BOOST_LOG_TRIVIAL(trace) << "FlightContextDialog answers " << action;
    } catch( DialogException const &e ) {
        BOOST_LOG_TRIVIAL(error) << "FlightContextDialog failed: " << e.what();
        wxMessageBox(wxString{"Flugkontextdialog fehlgeschlagen:\n"} + e.what(),
                     "Fehler bei Erfassung des Flugkontextes", wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    } catch( ... ) {
        BOOST_LOG_TRIVIAL(error) << "FlightContextDialog failed (ultimate catch)";
        wxMessageBox(wxString{"Flugkontextdialog fehlgeschlagen"}, "Fehler bei Erfassung des Flugkontextes",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    }
}

// void TopFrame::dateChanged(wxDateEvent &event) {
//     using namespace std::chrono;
//     dofInfo_.dateOfFlight(system_clock::from_time_t(event.GetDate().FromUTC().GetTicks()));
//     datepickerlCtrl->SetForegroundColour(dofInfo_.past() ? *wxRED : dofButtonFgCol);
//     timepickerlCtrl->SetValue(wxDateTime{system_clock::to_time_t(dofInfo_.sunset())}.ToUTC());
// }
//
// void TopFrame::timeChanged(wxDateEvent &event) {
//     using namespace std::chrono;
//     dofInfo_.sunset(system_clock::from_time_t(event.GetDate().FromUTC().GetTicks()));
//     timepickerlCtrl->SetForegroundColour(dofInfo_.unset() ? *wxRED : sunsetButtonFgCol);
// }

void TopFrame::addAircraft(wxCommandEvent &) {
    try {
        ManageAircraftsDialog manage_aircraft_dialog(this, *aircraftViewModel);
        auto const action = manage_aircraft_dialog.ShowModal();
        if( wxID_OK == action ) manage_aircraft_dialog.save();
        BOOST_LOG_TRIVIAL(trace) << "ManageAircraftDialog answers " << action;
    } catch( DialogException const &e ) {
        BOOST_LOG_TRIVIAL(error) << "ManageAircraftDialog failed: " << e.what();
        wxMessageBox(wxString{"Flugzeugverwaltung fehlgeschlagen:\n"} + e.what(), "Fehler beim verwalten der Flugzeuge",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    } catch( ... ) {
        BOOST_LOG_TRIVIAL(error) << "ManageAircraftDialog failed (ultimate catch)";
        wxMessageBox(wxString{"Flugzeugverwaltung fehlgeschlagen"}, "Fehler beim verwalten der Flugzeuge",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    }
}

void TopFrame::manageAirfields(wxCommandEvent &) {
    try {
        ManageAirfieldsDialog manage_airfields_dialog(this, *airfieldsViewModel);
        auto const action = manage_airfields_dialog.ShowModal();
        manage_airfields_dialog.save();
        BOOST_LOG_TRIVIAL(trace) << "ManageAirfieldsDialog answers " << action;
    } catch( DialogException const &e ) {
        BOOST_LOG_TRIVIAL(error) << "ManageAirfieldsDialog failed: " << e.what();
        wxMessageBox(wxString{"Flugplatzverwaltung fehlgeschlagen:\n"} + e.what(),
                     "Fehler beim verwalten der Flugplätze", wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    } catch( ... ) {
        BOOST_LOG_TRIVIAL(error) << "ManageAirfieldsDialog failed (ultimate catch)";
        wxMessageBox(wxString{"Flugplatzverwaltung fehlgeschlagen"}, "Fehler beim verwalten der Flugplätze",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    }
}

void TopFrame::loadFlight(wxCommandEvent &) {
    try {
        BOOST_LOG_TRIVIAL(trace) << "load-flightpath item selected";
        // TODO: warn if currently dirty
        wxFileDialog load_dialog{this,
                                 "Flugweg laden",
                                 wxEmptyString,
                                 wxEmptyString,
                                 "VFRP Dateien (*.vfr)|*.vfr",
                                 wxFD_OPEN | wxFD_FILE_MUST_EXIST | wxFD_CHANGE_DIR};
        if( wxID_OK == load_dialog.ShowModal() ) {
            // TODO: remember dir in config (below as well)
            loadFlight(load_dialog.GetPath().ToStdString());
            setCurrentPathFlightpath(load_dialog.GetPath(), load_dialog.GetFilename());
            SetStatusText(wxString{"Flugweg "} + load_dialog.GetFilename() + " geladen");
        }
    } catch( std::exception const &e ) {
        BOOST_LOG_TRIVIAL(error) << "couldn't load flightpath: " << e.what();
        wxMessageBox(wxString{"Flugweg konnte nicht geladen werden:\n"} + e.what(), "Fehler beim laden",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    } catch( ... ) {
        BOOST_LOG_TRIVIAL(error) << "couldn't load flightpath (ultimate catch)";
        wxMessageBox(wxString{"Flugweg konnte nicht geladen werden"}, "Fehler beim laden",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    }
}

void TopFrame::storeFlight(wxCommandEvent &event) {
    BOOST_LOG_TRIVIAL(trace) << "store-flightpath item selected";
    if( currentPathFlightpath ) try {
            storeFlight(currentPathFlightpath.value().ToStdString());
            SetStatusText(wxString{"Flugweg gespeichert"});
        } catch( std::exception const &e ) {
            BOOST_LOG_TRIVIAL(error) << "couldn't strore flightpath: " << e.what();
            wxMessageBox(wxString{"Flugweg konnte nicht gespeichert werden:\n"} + e.what(), "Fehler beim speichern",
                         wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
        } catch( ... ) {
            BOOST_LOG_TRIVIAL(error) << "couldn't strore flightpath (ultimate catch)";
            wxMessageBox(wxString{"Flugweg konnte nicht gespeichert werden"}, "Fehler beim speichern",
                         wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
        }
    else
        storeFlightAs(event);
}

void TopFrame::storeFlightAs(wxCommandEvent &) {
    try {
        BOOST_LOG_TRIVIAL(trace) << "store-flightpath-as item selected";
        wxFileDialog store_dialog{
            this,          "Flugweg speichern als...",   wxEmptyString,
            wxEmptyString, "VFRP Dateien (*.vfr)|*.vfr", wxFD_SAVE | wxFD_OVERWRITE_PROMPT | wxFD_CHANGE_DIR};
        if( wxID_OK == store_dialog.ShowModal() ) {
            BOOST_LOG_TRIVIAL(trace) << "user appointed path " << store_dialog.GetPath();
            storeFlight(store_dialog.GetPath().ToStdString());
            setCurrentPathFlightpath(store_dialog.GetPath(), store_dialog.GetFilename());
            SetStatusText(wxString{"Flugweg als "} + store_dialog.GetFilename() + " gespeichert");
        }
    } catch( std::exception const &e ) {
        BOOST_LOG_TRIVIAL(error) << "couldn't strore-as flightpath: " << e.what();
        wxMessageBox(wxString{"Flugweg konnte nicht gespeichert werden:\n"} + e.what(), "Fehler beim speichern-als",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    } catch( ... ) {
        BOOST_LOG_TRIVIAL(error) << "couldn't strore-as flightpath (ultimate catch)";
        wxMessageBox(wxString{"Flugweg konnte nicht gespeichert werden"}, "Fehler beim speichern-als",
                     wxOK | wxICON_ERROR | wxSTAY_ON_TOP | wxCENTRE, this);
    }
}

void TopFrame::originSelected(wxDataViewEvent &event) {
    auto airfield = *reinterpret_cast<std::shared_ptr<Airfield> *>(
        event.GetItem().GetID());  // FIXME: why is this operation in MainFrame?
    pathPanel->changeOrigin(airfield);
    elevationDisplay();
}

void TopFrame::destinationSelected(wxDataViewEvent &event) {
    auto airfield = *reinterpret_cast<std::shared_ptr<Airfield> *>(
        event.GetItem().GetID());  // FIXME: why is this operation in MainFrame?
    pathPanel->changeDestination(airfield);
    elevationDisplay();
}

void TopFrame::alternateSelected(wxDataViewEvent &event) {
    auto airfield = *reinterpret_cast<std::shared_ptr<Airfield> *>(
        event.GetItem().GetID());  // FIXME: why is this operation in MainFrame?
    pathPanel->changeAlternate(airfield);
}

void TopFrame::aircraftSelected(wxDataViewEvent &event) {
    selectAircraft(event.GetItem());
    pathPanel->changeAircraft(*selectedAircraft);  // TODO: why's this missing in selectAircraft ?
}

void TopFrame::logBasicInformation() const {
    auto &wx_paths = wxStandardPaths::Get();
    BOOST_LOG_TRIVIAL(info) << "executable " << wx_paths.GetExecutablePath();
    BOOST_LOG_TRIVIAL(info) << "user config and data dir: " << wx_paths.GetUserConfigDir() << " , "
                            << wx_paths.GetUserDataDir();
    auto wx_config = wxConfigBase::Get();
    BOOST_LOG_TRIVIAL(info) << "config app name " << wx_config->GetAppName();
}

void TopFrame::dofInfo(FlightContext const dof) {
    //    using namespace std::chrono;
    dofInfo_ = dof;
    dofButton->SetLabel(dmy(dofInfo_.dateOfFlight_days()));
    dofButton->SetForegroundColour(dofInfo_.past() ? *wxRED : dofButtonFgCol);
    auto const sunset = dofInfo_.sunset_min();
    sunsetButton->SetLabel(sunset ? hhmm(sunset.value()) : "--:--");
    sunsetButton->SetForegroundColour(sunset ? sunsetButtonFgCol : *wxRED);
    auto const eobt = dofInfo_.eobt_min();
    eobtButton->SetLabel(std::string{"eobt "}.append(eobt ? hhmm(eobt.value()) : "--:--"));
    auto fl_display = [](auto atmo, auto label) {
        if( atmo ) {
            auto from_fl = lowestVfrFl(atmo.value());
            label->SetLabel(flText(from_fl.second) + " @\n" + std::to_string(plainFeetMSL(from_fl.first)) + " ft");
        } else
            label->SetLabel("-");
    };
    auto const origin = dofInfo_.origin();
    fromQnhButton->SetLabel(origin ? std::to_string(plainHPa(origin->qnh_)) : "----");
    fromTempButton->SetLabel(origin ? std::to_string(plainCelsius(origin->temperature_)) : "--");
    fl_display(origin, fromFlLabel);
    auto const dest = dofInfo_.destination();
    toQnhButton->SetLabel(dest ? std::to_string(plainHPa(dest->qnh_)) : "----");
    toTempButton->SetLabel(dest ? std::to_string(plainCelsius(dest->temperature_)) : "--");
    fl_display(dest, toFlLabel);
    elevationDisplay();
}
void TopFrame::elevationDisplay() {
    auto elevation_display = [](auto field, auto atmo, auto l_elev, auto l_pres, auto l_dens) {
        if( field && atmo ) {
            l_elev->SetLabel("elev\n" + std::to_string(plainFeetMSL(field->elevation())) + " ft MSL");
            auto pres = convert_altitude(field->elevation(), atmo->qnh_);
            l_pres->SetLabel("pres\n" + std::to_string(plainFeet1013(pres)) + " ft");
            auto dens = convert_altitude(pres, atmo->temperature_);
            l_dens->SetLabel("dens\n" + std::to_string(plainFeetICAO(dens)) + " ft");
        } else {
            l_elev->SetLabel("-");
            l_pres->SetLabel("-");
            l_dens->SetLabel("-");
        }
    };
    elevation_display(pathPanel->origin(), dofInfo_.origin(), fromElevLabel, fromPressLabel, fromDensLabel);
    elevation_display(pathPanel->destination(), dofInfo_.destination(), toElevLabel, toPressLabel, toDensLabel);
}

void TopFrame::selectAircraft(wxDataViewItem const &item) {
    AircraftsViewModel const *model = aircraftViewModel.get();
    auto const &craft               = model->verifyAircraft(item);
    BOOST_LOG_TRIVIAL(debug) << "selected aircraft " << craft.callSign() << " , a " << craft.model();
    selectedAircraft = &craft;
}

void TopFrame::selectAircraft(wxString const &callsign) {
    auto item = aircraftViewModel.get()->item(callsign);
    if( item ) {
        selectAircraft(item);
        signViewCtrl->Select(item);
    } else
        BOOST_LOG_TRIVIAL(warning) << "cannot selected aircraft by callsign " << callsign << ", not changing selection";
}

void TopFrame::selectFieldCtrl(wxDataViewCtrl *ctrl, wxString const &icao) {
    auto item = airfieldsViewModel.get()->item(icao);
    if( item )
        ctrl->Select(item);
    else
        BOOST_LOG_TRIVIAL(warning) << "cannot selected airfield by icao code " << icao << ", not changing selection";
}

void TopFrame::setCurrentPathFlightpath(std::optional<wxString> const &path, wxString const &filename) {
    assert(filename.empty() != path.has_value());
    currentPathFlightpath = path;
    SetStatusText(filename, 1);
}

void TopFrame::loadFlight(std::filesystem::path const &path) {
    auto const jvalue = load_json(path);
    if( jvalue.as_object().if_contains("fuel") ) {
        BOOST_LOG_TRIVIAL(info) << "loading file: " << path;
        dofInfo(jvalue.as_object().if_contains("dof") ? boost::json::value_to<FlightContext>(jvalue.at("dof"))
                                                      : FlightContext{});
        selectAircraft(boost::json::value_to<std::string>(jvalue.at("craft")));
        pathPanel->fromJson(jvalue.at("path"), *selectedAircraft);
        selectFieldCtrl(fromViewCtrl, pathPanel->path().origin()->icaoCode());
        if( pathPanel->path().leg().checkpointIsField() )
            selectFieldCtrl(toViewCtrl, pathPanel->path().leg().checkpointField()->icaoCode());
        if( pathPanel->path().alternate().checkpointIsField() )
            selectFieldCtrl(altViewCtrl, pathPanel->path().alternate().checkpointField()->icaoCode());
        auto const safe_duration = fuelPanel->fromJson(jvalue.at("fuel"), pathPanel->path(), *selectedAircraft);
        pathPanel->updateSafeDuration(safe_duration);  // FIXME: have this update upon fuel-only changes as well!
    } else {                                           // legacy
        BOOST_LOG_TRIVIAL(warning) << "loading legacy file: " << path;
        pathPanel->loadFlightpath(path.string());
    }
}

void TopFrame::storeFlight(std::filesystem::path const &path) {
    std::ofstream out{path};
    out << boost::json::object{
        {  "dof",                                                               dofInfo_},
        {"craft", selectedAircraft ? selectedAircraft->callSign().ToStdString() : "NONE"},
        { "path",                                         pathPanel->jsoniseFlightpath()},
        { "fuel",                                               fuelPanel->jsoniseFuel()},
    };
}