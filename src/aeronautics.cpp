#include "aeronautics.h"
#include "trigonometry.h"

angle_t calcWCA(angle_t const tc, wind_t const wind, speed_t tas) {
    using mp_units::isq::sin;
    using mp_units::isq::asin;
    // law of sines with assumption that WCA must be <90° (that is: you don't fly echo class in 100kt wind)
    // c.f. https://www.omnicalculator.com/physics/wind-correction-angle
    auto const wind_angle = wind.angle - tc; // current equal to : tc - invert(wind.angle); ???
    return mp_units::value_cast<angle_t::rep>(asin(sin(wind_angle)*wind.speed/tas));
}

speed_t calcGS(angle_t const tc, angle_t wca, speed_t const tas, wind_t const wind) {
    using mp_units::isq::cos;
    // calculation equivalent to law of cosines
    // c.f. https://www.omnicalculator.com/physics/ground-speed
    auto const wind_angle = tc - invert(wind.angle);
    return mp_units::value_cast<speed_t::rep>(tas*cos(wca) + wind.speed*cos(wind_angle)); // floor intended
}
