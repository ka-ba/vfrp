#include "vfrp-types-json.h"
#include "plain-values.h"
// using namespace mp_units;

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, wind_t const &wind) {
    value = {wind.angle, wind.speed};
}
wind_t tag_invoke(boost::json::value_to_tag<wind_t> const &, boost::json::value const &value) {
    return wind_t{
        boost::json::value_to<angle_t>(value.as_array().at(0)),
        boost::json::value_to<speed_t>(value.as_array().at(1)),
    };
}

namespace mp_units {
void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, speed_t const &speed) {
    value.emplace_int64() = plainKnots(speed);
    assert(value.is_int64() && "from speed_t");
}
speed_t tag_invoke(boost::json::value_to_tag<speed_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to speed_t");
    return fromKnots(value.as_int64());
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, duration_t const &time) {
    value.emplace_int64() = plainMins(time);
    assert(value.is_int64() && "from duration_t");
}
duration_t tag_invoke(boost::json::value_to_tag<duration_t> const &, boost::json::value const &value) {
    auto const plain = (value.is_object() && value.as_object().contains("min"))
                           ? value.at("min").as_int64()  // support medium-legacy
                           : value.as_int64();
    return fromMins(plain);
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, distance_t const &distance) {
    value.emplace_int64() = plainNM(distance);
    assert(value.is_int64() && "from distance_t");
}

distance_t tag_invoke(boost::json::value_to_tag<distance_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to distance_t");
    return fromNM(value.as_int64());
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, amount_t const &amount) {
    value.emplace_int64() = plainLitre(amount);
    assert(value.is_int64() && "from amount_t");
}

amount_t tag_invoke(boost::json::value_to_tag<amount_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to amount_t");
    return fromLitre(value.as_int64());
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, true_altitude_t const &altitude) {
    value.emplace_int64() = plainFeetMSL(altitude);
    assert(value.is_int64() && "from true_altitude_t");
}

true_altitude_t tag_invoke(boost::json::value_to_tag<true_altitude_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to true_altitude_t");
    return fromFeetMSL(value.as_int64());
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, angle_t const &angle) {
    // value.as_int64() = static_cast<long>(std::round(angle.numerical_value_in(si::degree)));
    value.emplace_double() = angle.numerical_value_in(mp_units::si::radian);  // TODO: remove this legacy in due time
    assert(value.is_double() && "from angle_t");
}

angle_t tag_invoke(boost::json::value_to_tag<angle_t> const &, boost::json::value const &value) {
    assert((value.is_int64() || value.is_double()) && "to angle_t");
    return value.is_int64() ? angle_t{degree_base_t{value.as_int64()} * si::degree}
                            : angle_t{radian_base_t{value.as_double()} * si::radian};  // legacy - leave longer
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, pressure_t const &pressure) {
    value.emplace_int64() = plainHPa(pressure);
    assert(value.is_int64() && "from pressure_t");
}

pressure_t tag_invoke(boost::json::value_to_tag<pressure_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to pressure_t");
    return fromHPa(value.as_int64());
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, temperature_t const &temperature) {
    value.emplace_int64() = plainCelsius(temperature);
    assert(value.is_int64() && "from temperature_t");
}

temperature_t tag_invoke(boost::json::value_to_tag<temperature_t> const &, boost::json::value const &value) {
    assert(value.is_int64() && "to temperature_t");
    return fromCelsius(value.as_int64());
}
}  // namespace mp_units

namespace std::chrono {
// void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, minutes const &minu) {
//     value = {{"min",minu.count()}};
// }

// minutes tag_invoke(boost::json::value_to_tag<minutes> const &, boost::json::value const &value) {
//     using namespace std::chrono_literals;
//     return 1min * static_cast<minutes::rep>(value.at("min").as_int64());
// }
}
