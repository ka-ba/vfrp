#include "trigonometry.h"
#include <cassert>
#include <mp-units/math.h>

using namespace mp_units;

angle_t invert(angle_t const rhs) {
    return rhs > HALFCIRCLE
            ? rhs - HALFCIRCLE
            : rhs + HALFCIRCLE;
}

angle_t rel_angle(angle_t const lhs,angle_t const rhs) {
    auto const diff = lhs > rhs
            ? lhs-rhs
            : rhs-lhs;
    return diff > HALFCIRCLE
            ? angle_t{0.* si::radian} - diff
            : diff;
}

wind_t invert(wind_t const rhs) {
    return {invert(rhs.angle),rhs.speed};
}

wind_t vector_sum(wind_t const lhs, wind_t const rhs) {
    auto const a =  mp_units::isq::sin(lhs.angle) * lhs.speed
            +mp_units::isq::sin(rhs.angle) * rhs.speed;
    auto const b = mp_units::isq::cos(lhs.angle) * lhs.speed
            +mp_units::isq::cos(rhs.angle) * rhs.speed;
    static_assert (std::is_same_v<decltype(a),decltype(b)>, "a-b");
    static_assert (std::is_same_v<decltype(a)::rep,double>, "ar-d");
    auto const c = mp_units::/*isq::*/round<u_kt>((mp_units::sqrt(mp_units::pow<2>(a)+mp_units::pow<2>(b))));
    return {value_cast<angle_t::rep>(mp_units::isq::asin(a/c)),mp_units::value_cast<speed_t::rep>(c)};
}