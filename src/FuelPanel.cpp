#include "FuelPanel.h"
#include <memory>
#include <boost/json.hpp>
#include <wx/gbsizer.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include "AircraftsViewModel.h"
#include "FlightPath.h"
#include "plain-values.h"
#include "util.h"
#include "wx-shortcuts.h"

FuelPanel::FuelPanel(wxWindow *parent, wxWindowID const winid, wxPoint const &pos, wxSize const &size, long const style,
                     wxString const &name)
    : wxPanel(parent, winid, pos, size, style, name) {
    initGUI();
}

duration_t FuelPanel::update(FlightPath const &path, Aircraft const &craft) {
    fuelModel_.updateConsumption(craft, *this);
    fuelModel_.cruiseDuration(path.duration(), *this);
    fuelModel_.taxiAddAmount(fromLitre(taxiAddFuelSpin->GetValue()), *this);
    fuelModel_.climbAddDuration(path.climbDuration(), *this);
    fuelModel_.circuitAddDuration(fromMins(circuitAddDurSpin->GetValue()), *this);
    fuelModel_.alternateDuration(path.alternateDuration(), *this);
    fuelModel_.reserveDuration(fromMins(reserveSpin->GetValue()), *this);
    fuelModel_.extraAmount(fromLitre(extraFuelSpin->GetValue()), *this);
    return fuelModel_.safe();
}

boost::json::object FuelPanel::jsoniseFuel() const {
    return fuelModel_.jsonise();
}

duration_t FuelPanel::fromJson(boost::json::value const &jvalue, FlightPath const &path, Aircraft const &aircraft) {
    fuelModel_ = FuelModel{jvalue, aircraft, *this};
    return update(path, aircraft);
}

wxStaticText *FuelPanel::addLabel(wxGridBagSizer *sizer, wxString const &text, wxGBPosition const &pos,
                                  wxGBSpan const &span, /*int sizer_flags,*/ int text_flags) {
    auto label = new wxStaticText(this, wxID_ANY, text, wxDefaultPosition, wxDefaultSize, text_flags);
    label->Wrap(-1);
    sizer->Add(label, pos, span, wxALIGN_CENTER_VERTICAL | wxEXPAND, 0 /*5?*/);
    return label;
}

wxSpinCtrl *FuelPanel::addSpin(wxGridBagSizer *sizer, wxWindowID const wid, wxRange const range, int const init,
                               wxObjectEventFunction const handler, wxString const &example, wxGBPosition const &pos) {
    auto spin = new wxSpinCtrl(this, wid, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT | wxSP_ARROW_KEYS,
                               range.GetMin(), range.GetMax(), init);
    sizeByExample(*spin, example);
    if( handler ) spin->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, handler, NULL, this);
    sizer->Add(spin, pos, wxGBSpan(1, 1), wxALIGN_CENTER_VERTICAL | wxEXPAND, 0);
    return spin;
}

void FuelPanel::initGUI() {
    auto fuel_sizer = std::make_unique<wxGridBagSizer>(1, 1);
    fuel_sizer->SetFlexibleDirection(wxBOTH);
    fuel_sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

    addLabel(fuel_sizer.get(), wxT("Flugzeit"), {0, 2}, {1, 1}, wxALIGN_CENTER_HORIZONTAL);
    addLabel(fuel_sizer.get(), wxT("Kraftstoff"), {0, 3}, {1, 1}, wxALIGN_CENTER_HORIZONTAL);

    addLabel(fuel_sizer.get(), wxT("Reiseflug (Platz-Platz)"), {1, 0}, {1, 2}, wxALIGN_RIGHT);
    cruiseDurationLabel = addLabel(fuel_sizer.get(), wxT("cruise dur"), {1, 2}, {1, 1}, wxALIGN_RIGHT);
    cruiseFuelLabel     = addLabel(fuel_sizer.get(), wxT("cruise fuel"), {1, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Steigflug (wenn separat)"), {2, 0}, {1, 2}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Zuschläge"), {3, 0}, {3, 1}, wxALIGN_RIGHT);
    addLabel(fuel_sizer.get(), wxT("Anlassen,Rollen"), {3, 1}, {1, 1}, wxALIGN_RIGHT);
    taxiAddFuelSpin = addSpin(fuel_sizer.get(), wxID_ANY, {1, 99}, 3,
                              wxCommandEventHandler(FuelPanel::changedTaxiAddFuel), "99", {3, 3});  // FIXME: handler

    addLabel(fuel_sizer.get(), wxT("Steigflug"), {4, 1}, {1, 1}, wxALIGN_RIGHT);
    climbAddDurLabel  = addLabel(fuel_sizer.get(), wxT("climb dur"), {4, 2}, {1, 1}, wxALIGN_RIGHT);
    climbAddFuelLabel = addLabel(fuel_sizer.get(), wxT("climb fuel"), {4, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("An-/Abflug"), {5, 1}, {1, 1}, wxALIGN_RIGHT);
    circuitAddDurSpin   = addSpin(fuel_sizer.get(), wxID_ANY, {10, 60}, 10,
                                  wxCommandEventHandler(FuelPanel::changedCircuitAddDur), "55", {5, 2});
    circuitAddFuelLabel = addLabel(fuel_sizer.get(), wxT("circuit fuel"), {5, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Ausweichflugplatz"), {6, 0}, {1, 2}, wxALIGN_RIGHT);
    alternateDurLabel  = addLabel(fuel_sizer.get(), wxT("alternate dur"), {6, 2}, {1, 1}, wxALIGN_RIGHT);
    alternateFuelLabel = addLabel(fuel_sizer.get(), wxT("alternate fuel"), {6, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Reserve"), {7, 0}, {1, 2}, wxALIGN_RIGHT);
    reserveSpin      = addSpin(fuel_sizer.get(), wxID_ANY, {30, 120}, 30,
                               wxCommandEventHandler(FuelPanel::changedReserveDur), "122", {7, 2});
    reserveFuelLabel = addLabel(fuel_sizer.get(), wxT("reserve fuel"), {7, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Mindest-Kraftstoffbedarf"), {8, 0}, {1, 2}, wxALIGN_RIGHT);
    minimalFuelLabel = addLabel(fuel_sizer.get(), wxT("minimal fuel"), {8, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("Extra-Kraftstoff"), {9, 0}, {1, 2}, wxALIGN_RIGHT);
    extraDurSpin  = addSpin(fuel_sizer.get(), wxID_ANY, {0, 300}, 0, wxCommandEventHandler(FuelPanel::changedExtraDur),
                            "299", {9, 2});
    extraFuelSpin = addSpin(fuel_sizer.get(), wxID_ANY, {0, 999}, 0, wxCommandEventHandler(FuelPanel::changedExtraFuel),
                            "999", {9, 3});

    addLabel(fuel_sizer.get(), wxT("Kraftstoff-Vorrat"), {10, 0}, {1, 2}, wxALIGN_RIGHT);
    stockDurLabel  = addLabel(fuel_sizer.get(), wxT("stock dur"), {10, 2}, {1, 1}, wxALIGN_RIGHT);
    stockFuelLabel = addLabel(fuel_sizer.get(), wxT("stock fuel"), {10, 3}, {1, 1}, wxALIGN_RIGHT);

    addLabel(fuel_sizer.get(), wxT("sichere Flugzeit"), {11, 0}, {1, 2}, wxALIGN_RIGHT);
    safeDurLabel = addLabel(fuel_sizer.get(), wxT("safe dur"), {11, 2}, {1, 1}, wxALIGN_RIGHT);

    consumptionLabel_ =
        addLabel(fuel_sizer.get(), wxT("Daten über Verbrauch: x"), {12, 0}, {1, 4}, wxALIGN_RIGHT | wxST_NO_AUTORESIZE);

    auto fuel_sizer_raw = fuel_sizer.release();
    SetSizer(fuel_sizer_raw);
    Layout();
    fuel_sizer_raw->Fit(this);
}

void FuelPanel::cruiseDisplay(duration_t const minutes, amount_t const amount) {
    cruiseDurationLabel->SetLabel(wxT("") + hhmm(minutes));
    cruiseFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::taxiAddDisplay(amount_t const amount) {
    taxiAddFuelSpin->SetValue(plainLitre(amount));
}

void FuelPanel::climbAddDisplay(duration_t const minutes, amount_t const amount) {
    climbAddDurLabel->SetLabel(wxT("") + hhmm(minutes));
    climbAddFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::circuitAddDisplay(duration_t const minutes, amount_t const amount) {
    circuitAddDurSpin->SetValue(plainMins(minutes));
    circuitAddFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::alternateDisplay(duration_t const minutes, amount_t const amount) {
    alternateDurLabel->SetLabel(wxT("") + hhmm(minutes));
    alternateFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::reserveDisplay(duration_t const minutes, amount_t const amount) {
    reserveSpin->SetValue(plainMins(minutes));
    reserveFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::minimalDisplay(amount_t const amount) {
    minimalFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::extraDisplay(duration_t const minutes, amount_t const amount) {
    extraDurSpin->SetValue(plainMins(minutes));
    extraFuelSpin->SetValue(plainLitre(amount));
}

void FuelPanel::stockDisplay(duration_t const minutes, amount_t const amount) {
    stockDurLabel->SetLabel(wxT("") + hhmm(minutes));
    stockFuelLabel->SetLabel(wxT("") + std::to_string(plainLitre(amount)));
}

void FuelPanel::safeDisplay(duration_t const minutes) {
    safeDurLabel->SetLabel(wxT("") + hhmm(minutes));
}
// TODO with g++-13: can we replace nice_str by std::format?
// #include <format>
// #include <mp-units/format.h>
void FuelPanel::consumptionLabel(consumption_t const cruise_c, consumption_t const climb_c) {
    consumptionLabel_->SetLabel(wxT("Daten über Verbrauch: ") + nice_str(plainLZh(cruise_c)) + " / "
                                + nice_str(plainLZh(climb_c)) + " l/h");
}

void FuelPanel::changedTaxiAddFuel(wxCommandEvent &event) {
    fuelModel_.taxiAddAmount(fromLitre(event.GetInt()), *this);
    event.StopPropagation();
}

void FuelPanel::changedCircuitAddDur(wxCommandEvent &event) {
    fuelModel_.circuitAddDuration(fromMins(event.GetInt()), *this);
    event.StopPropagation();
}

void FuelPanel::changedReserveDur(wxCommandEvent &event) {
    fuelModel_.reserveDuration(fromMins(event.GetInt()), *this);
    event.StopPropagation();
}

void FuelPanel::changedExtraDur(wxCommandEvent &event) {
    fuelModel_.extraDuration(fromMins(event.GetInt()), *this);
    event.StopPropagation();
}

void FuelPanel::changedExtraFuel(wxCommandEvent &event) {
    fuelModel_.extraAmount(fromLitre(event.GetInt()), *this);
    event.StopPropagation();
}
