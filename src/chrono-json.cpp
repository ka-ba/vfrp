#include "chrono-json.h"

namespace boost::json
{
    using namespace std;

    void tag_invoke(value_from_tag const&, value &value, chrono::sys_days const &days) {
        value.emplace_int64() = days.time_since_epoch().count();
        assert(value.is_int64() && "from sys_days");
    }
    chrono::sys_days tag_invoke(value_to_tag<chrono::sys_days> const&, value const &value) {
        assert(value.is_int64() && "to sys_days");
        return chrono::sys_days{chrono::sys_days::duration{value.as_int64()}};
    }

    void tag_invoke(value_from_tag const&, value &value, chrono::minutes const &minu) {
        value.emplace_int64() = minu.count();
        assert(value.is_int64() && "from minutes");
    }
    chrono::minutes tag_invoke(value_to_tag<chrono::minutes> const&, value const &value) {
        assert(value.is_int64() && "to minutes");
        return chrono::minutes {value.as_int64()};
    }
}