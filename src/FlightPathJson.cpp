#include "FlightPathJson.h"
#include "FlightPath.h"
#include "vfrp-types-json.h"

JsonLeg::JsonLeg(Leg const &leg)
    : checkpoint_(leg.checkpointName()), notes_(leg.notes_), isField_(leg.checkpointIsField()), secureAltitude_(leg.secureAltitude_),
      altitude_(leg.altitude_), wind_(leg.wind_), tC_(leg.tC_), var_(leg.var_), distance_(leg.distance_) {}

JsonLeg::JsonLeg(std::string const checkpoint, bool const is_field, true_altitude_t const secure,
                 true_altitude_t const altitude, wind_t const wind, angle_t const tC, angle_t const var,
                 distance_t const distance)
    : checkpoint_(checkpoint), isField_(is_field), secureAltitude_(secure), altitude_(altitude), wind_(wind), tC_(tC),
      var_(var), distance_(distance) {}

JsonLeg::operator Leg() const {
    return Leg{checkpoint_, isField_, secureAltitude_, altitude_, wind_, tC_, var_, distance_,notes_};
}

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, Leg const &leg) {
    JsonLeg jleg{leg};
    value = {
        {"checkpoint",     jleg.checkpoint_},
        {   "isfield",        jleg.isField_},
        {    "secure", jleg.secureAltitude_},
        {  "altitude",       jleg.altitude_},
        {      "wind",           jleg.wind_},
        {        "TC",             jleg.tC_},
        {       "VAR",            jleg.var_},
        {  "distance",       jleg.distance_},
        {  "notes",       jleg.notes_},
    };
}

Leg tag_invoke(boost::json::value_to_tag<Leg> const &, boost::json::value const &value) {
    JsonLeg jleg{
        boost::json::value_to<std::string>(value.at("checkpoint")),
        false,  // see below
        boost::json::value_to<true_altitude_t>(value.at("secure")),
        boost::json::value_to<true_altitude_t>(value.at("altitude")),
        boost::json::value_to<wind_t>(value.at("wind")),
        boost::json::value_to<angle_t>(value.at("TC")),
        boost::json::value_to<angle_t>(value.at("VAR")),
        boost::json::value_to<distance_t>(value.at("distance")),
    };
    if( value.as_object().if_contains("elevation") ) jleg.isField_ = true;  // support legacy format for a while
    if( value.as_object().if_contains("isfield") ) jleg.isField_ = boost::json::value_to<bool>(value.at("isfield"));
    if( value.as_object().if_contains("notes") ) jleg.notes_ = boost::json::value_to<std::string>(value.at("notes"));
    return jleg;
}