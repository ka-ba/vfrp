#include "util.h"
#include <charconv>
#include <fstream>
#include <sstream>
#include <string_view>
#include <boost/json.hpp>
#include <boost/log/trivial.hpp>
#include "plain-values.h"

// https://stackoverflow.com/a/60046608 - converted to units
std::string hhmm(duration_t minutes) {
    std::stringstream result;
    auto hours = minutes.force_in(u_h);
    if( hours > 0 * u_h ) {
        if( hours < 10 * u_h ) result << "0";
        result << std::to_string(hours.numerical_value_in(u_h));
        result << ':';
        minutes -= hours;
        if( minutes < 10 * u_min ) result << "0";
    }
    result << std::to_string(minutes.numerical_value_in(u_min));
    return result.str();
}

std::string hhmm(std::chrono::minutes const mins) {
    std::chrono::hh_mm_ss<std::chrono::minutes> hms{mins};
    std::stringstream result;
    result << std::setfill('0') << std::setw(2) << hms.hours().count();
    result << ":";
    result << std::setfill('0') << std::setw(2) << hms.minutes().count();
    return result.str();
}

std::string dmy(std::chrono::sys_days const days) {
    std::chrono::year_month_day ymd{days};
    std::stringstream result;
    result << ymd.day().operator unsigned int() << "." << ymd.month().operator unsigned int() << "."
           << ymd.year().operator int();
    return result.str();
}

std::string nice_str(double const num) {
    std::string str{"1234567890"};
    // why did they add an awkward interface like to_chars to modern c++ ?
    if( auto const [ptr, ec] = std::to_chars(str.data(), str.data() + str.size(), num); ec == std::errc() )
        return std::string{std::string_view(str.data(), ptr - str.data())};
    else {
        BOOST_LOG_TRIVIAL(warning) << "unable to display " << num << " nicely, because too many digits";
        return std::to_string(num);
    }
}

std::string flText(pressure_altitude_t const altitude) {
    std::stringstream text;
    text << "FL" << std::setw(3) << std::setfill('0') << plainFL(altitude);
    return text.str();
}

boost::json::value load_json(std::filesystem::path const &path) {
    std::ifstream in{path};
    boost::json::stream_parser parser;
    std::string line;
    while( std::getline(in, line) )
        parser.write(line);
    parser.finish();
    return parser.release();
}

std::ostream &operator<<(std::ostream &out,
                         true_altitude_t amsl) {  // declared in vfrp-types.h
    out << amsl.quantity_from(mean_sea_level) << " AMSL";
    return out;
}
std::ostream &operator<<(std::ostream &out,
                         pressure_altitude_t palt) {  // declared in vfrp-types.h
    out << palt.quantity_from(pressure_level_1013) << " above 1013 hPa";
    return out;
}
std::ostream &operator<<(std::ostream &out,
                         density_altitude_t dalt) {  // declared in vfrp-types.h
    out << dalt.quantity_from(icao_std_atmo) << " density altitude";
    return out;
}