#include "FlightPath.h"
#include <fstream>
#include <stdexcept>
#include <utility>
#include <boost/json.hpp>
#include <boost/log/trivial.hpp>
#include <mp-units/math.h>
#include "AircraftsViewModel.h"
#include "AirfieldsViewModel.h"
#include "FlightPathJson.h"
#include "aeronautics.h"
#include "plain-values.h"
#include "trigonometry.h"
#include "util.h"
#include "vfrp-types-json.h"
using namespace mp_units;

FlightPath::FlightPath() : origin_(std::make_shared<Airfield>("[INVALID]")) {}

FlightPath::FlightPath(std::shared_ptr<Airfield const> origin, std::shared_ptr<Airfield const> destination)
    : origin_(std::move(origin)) {
    path_.emplace_back(std::move(destination));
}

FlightPath::FlightPath(std::filesystem::path const &path, Aircraft const &aircraft)
    : FlightPath(load_json(path), aircraft) {}

FlightPath::FlightPath(boost::json::value const &jvalue, Aircraft const &aircraft) {
    origin_ =
        AirfieldsViewModel::instanceConst().airfieldOrDummy(boost::json::value_to<std::string>(jvalue.at("origin")));
    path_ = boost::json::value_to<std::vector<Leg>>(jvalue.at("legs"));
    if( jvalue.as_object().if_contains("alternate") ) alternate_ = boost::json::value_to<Leg>(jvalue.at("alternate"));
    recalc(aircraft);
}

boost::json::object FlightPath::jsonise() const {
    return boost::json::object{
        {         "origin",  origin_->icaoCode()},
        {"originelevation", origin_->elevation()}, // FIXME: deprecated: not necessary
        {           "legs",                path_},
        {      "alternate",           alternate_},
    };
}

std::shared_ptr<Airfield const> FlightPath::origin() const {
    return origin_;
}

void FlightPath::origin(std::shared_ptr<Airfield const> o_rigin) {
    origin_ = std::move(o_rigin);
}

size_t FlightPath::size() const noexcept {
    return path_.size();
}

Leg const &FlightPath::leg(std::optional<size_t> index) const {
    if( ! index ) index = size() - 1;
    if( ALTINDEX == index ) return alternate();
    if( index >= size() ) throw std::invalid_argument("no leg at this index");
    return path_.at(index.value());
}

Leg &FlightPath::leg(std::optional<size_t> index) {
    if( ! index ) index = size() - 1;
    if( ALTINDEX == index ) return alternate();
    if( index >= size() ) throw std::invalid_argument("no leg at this index");
    return path_.at(index.value());
}

void FlightPath::deleteLeg(size_t const index) {
    if( index >= size() ) throw std::invalid_argument("no leg at this index");
    auto iter = path_.begin();
    std::advance(iter, index);
    path_.erase(iter);
}

bool FlightPath::isLast(size_t const index) const {
    return index == size() - 1;
}

void FlightPath::recalc(Aircraft const &aircraft, std::optional<size_t> index) {
    if( path_.empty() ) return;
    if( (! index) || (ALTINDEX == index) ) index = size() - 1;
    if( index >= size() ) throw std::invalid_argument("can't recalc index too large");
    // add up distance and duration from back to front, while recalculating individually
    for( size_t naked = index.value(); naked < size(); --naked ) {
        auto const distance_sum = (naked + 1) < size() ? leg(naked + 1).distanceSum_ : distance_t::zero();
        auto const duration_sum = (naked + 1) < size() ? leg(naked + 1).durationSum_ : duration_t::zero();
        auto const alt_before   = naked > 0 ? leg(naked - 1).altitude_ : origin_->elevation();
        auto const alt_after    = (naked + 1) < size()             ? leg(naked + 1).altitude_
                                  : leg(naked).checkpointIsField() ? leg(naked).checkpointField()->elevation()
                                                                   : true_altitude_t::zero();
        leg(naked).recalc(distance_sum, duration_sum, alt_before, alt_after, aircraft);
    }
    alternate_.recalc(distance_t::zero(), duration_t::zero(), alternate_.altitude_, alternate_.altitude_, aircraft);
}

duration_t FlightPath::duration() const {
    return leg(0).durationSum_;
}

duration_t FlightPath::climbDuration() const {
    return std::transform_reduce(path_.cbegin(), path_.cend(), duration_t::zero(), std::plus{},
                                 [](auto const &leg) { return leg.preClimb_; });
}

duration_t FlightPath::alternateDuration() const {
    return alternate_.legDuration_;
}

void FlightPath::insert(Leg const &l_eg, std::optional<size_t> index) {
    if( ! index ) index = size();
    if( index >= size() ) throw std::invalid_argument("can't insert at index too large");
    auto iter = path_.begin();
    std::advance(iter, index.value());
    path_.insert(iter, l_eg);
}

Leg const &FlightPath::alternate() const {
    return alternate_;
}

Leg &FlightPath::alternate() {
    return alternate_;
}

void FlightPath::alternate(Leg a_lternate) {
    alternate_ = std::move(a_lternate);
}

void FlightPath::alternate(std::shared_ptr<Airfield const> a_lternate) {
    alternate_.checkpoint_ = std::move(a_lternate);
}

Leg::Leg(CP_OR_FIELD checkpoint) : checkpoint_(std::move(checkpoint)) {}

Leg::Leg(std::variant<std::string, std::shared_ptr<Airfield const>> checkpoint, true_altitude_t secure_altitude,
         true_altitude_t altitude, wind_t wind, angle_t tc, angle_t var, distance_t distance)
    : checkpoint_(std::move(checkpoint)), secureAltitude_(secure_altitude), altitude_(altitude), wind_(wind), tC_(tc),
      var_(var), distance_(distance) {}

Leg::Leg(std::string const checkpoint, bool const checkpoint_is_field, true_altitude_t const secure,
         true_altitude_t const altitude, wind_t const wind, angle_t const tc, angle_t const var,
         distance_t const distance, std::string notes)
    : checkpoint_(checkpoint_is_field ? CP_OR_FIELD{AirfieldsViewModel::instanceConst().airfieldOrDummy(checkpoint)}
                                      : CP_OR_FIELD{checkpoint}),
      secureAltitude_(secure), altitude_(altitude), notes_(std::move(notes)), wind_(wind), tC_(tc), var_(var), distance_(distance) {}

std::string Leg::checkpointName() const noexcept {
    if( auto str_ptr = std::get_if<std::string>(&checkpoint_) )
        return *str_ptr;
    else if( auto fld_ptr = std::get_if<std::shared_ptr<Airfield const>>(&checkpoint_) )
        return (*fld_ptr)->icaoCode().ToStdString();
    else
        return "INTERNAL ERROR ON std::variant";
}

bool Leg::checkpointIsField() const noexcept {
    return std::holds_alternative<std::shared_ptr<Airfield const>>(checkpoint_);
}

std::shared_ptr<Airfield const> Leg::checkpointField() const noexcept {
    return std::get<std::shared_ptr<Airfield const>>(checkpoint_);
}

void Leg::recalc(distance_t const distance_sum, duration_t const duration_sum, true_altitude_t const alt_before,
                 true_altitude_t const alt_after, Aircraft const &aircraft) {
    if( alt_before >= altitude_ ) {  // don't (pre)climb
        recalc(alt_before, alt_after, aircraft, SubLegType::CruiseDescend);
    } else {  // do (pre)climb, that is: calculate 2 sub-legs
        Leg climb_subleg{*this};
        climb_subleg.recalc(alt_before, altitude_, aircraft, SubLegType::Climb);
        Leg cruise_subleg{*this};
        cruise_subleg.distance_ -= climb_subleg.distance_;
        BOOST_LOG_TRIVIAL(trace) << "split leg into sub-legs with " << climb_subleg.distance_ << " (climb) and "
                                 << cruise_subleg.distance_ << " (cruise)";
        cruise_subleg.recalc(altitude_, alt_after, aircraft, SubLegType::CruiseDescend);
        *this = addUp(climb_subleg, cruise_subleg);
    }
    distanceSum_ = distance_sum + distance_;
    durationSum_ = duration_sum + legDuration_;
}

wind_t Leg::gVector(angle_t const heading, speed_t const tas, wind_t const wind) {
    return vector_sum({heading, tas}, invert(wind));
}

void Leg::recalc(true_altitude_t const alt_before, true_altitude_t const alt_after, Aircraft const &aircraft,
                 SubLegType const type) {
    // FIXME:: add pre-climb mins and post-descend mins somewhere in this function ...
    auto const leg_tas = type == SubLegType::Climb ? aircraft.climbIAS() : aircraft.cruisingIAS();
    tAS_               = std::to_string(plainKnots(leg_tas));
    mC_                = calcMC(tC_, var_);
    wCA_               = calcWCA(tC_, wind_, leg_tas);
    mH_                = calcMH(mC_, wCA_);
    gS_                = calcGS(tC_, wCA_, leg_tas, wind_);
    preClimb_ = postDescend_ = duration_t::zero();
    switch( type ) {
        case SubLegType::Climb : {  // changing dist on climb sub-legs ok, because leg is temporary only
            assert(alt_before < altitude_);
            // FIXME: make here hard-coded 500ft/min an aircraft attribute
            auto const climb_duration =
                ceil<u_min>(value_cast<double>(altitude_ - alt_before) / speed_vert_t{500 * u_ft_per_min});
            static_assert(std::is_same_v<double, typename decltype(climb_duration)::rep>);
            distance_    = min(distance_, value_cast<distance_t::rep>(floor<u_NM>(gS_ * climb_duration)));
            legDuration_ = value_cast<duration_t::rep>(climb_duration);
            BOOST_LOG_TRIVIAL(trace) << "climb from " << plainFeetMSL(alt_before) << " to " << plainFeetMSL(altitude_)
                                     << " in " << climb_duration << " at GS " << gS_ << " which hopefully covers "
                                     << distance_;
        } break;
        case SubLegType::CruiseDescend :
            if( alt_after < altitude_ ) {
                postDescend_ =
                    value_cast<duration_t::rep>(ceil<u_min>((altitude_ - alt_after) / (500. * u_ft_per_min)));
                BOOST_LOG_TRIVIAL(trace) << "descend from " << plainFeetMSL(altitude_) << " to "
                                         << plainFeetMSL(alt_after) << " in " << postDescend_;
            }
            // break;
            legDuration_ = gS_ == speed_t::zero()
                               ? duration_t{9999 * u_min}
                               : value_cast<duration_t::rep>(ceil<u_min>(value_cast<double>(distance_) / gS_));
    }
    BOOST_LOG_TRIVIAL(trace) << "leg duraition is " << legDuration_ << " for " << distance_ << " with GS " << gS_;
}

Leg Leg::addUp(Leg const &lhs, Leg const &rhs) {
    // FIXME: work out angles correctly
    Leg result{rhs.checkpoint_,
               std::max(lhs.secureAltitude_, rhs.secureAltitude_),
               rhs.altitude_,
               rhs.wind_,
               rhs.tC_,
               rhs.var_,
               lhs.distance_ + rhs.distance_};
    result.tAS_         = lhs.tAS_ == rhs.tAS_ ? rhs.tAS_ : lhs.tAS_ + "/" + rhs.tAS_;
    result.wCA_         = rhs.wCA_;
    result.mH_          = rhs.mH_;
    result.mC_          = rhs.mC_;
    result.legDuration_ = lhs.legDuration_ + rhs.legDuration_;
    result.preClimb_    = lhs.legDuration_;
    result.postDescend_ = rhs.postDescend_;
    result.gS_ = value_cast<speed_t::rep>(round<u_kt>(value_cast<double>(result.distance_) / result.legDuration_));
    return result;
}