#include "wx-shortcuts.h"

void sizeByExample(wxSpinCtrl &spin, const wxString &example)
{
    auto size = spin.GetSizeFromTextSize(spin.GetTextExtent(example));
    spin.SetSize(size);
    spin.SetMaxSize(size);
}
