#include "DofInfo.h"
#include <boost/log/trivial.hpp>
#include "chrono-json.h"
#include "vfrp-types-json.h"
using namespace std::chrono;
using namespace boost;

// FlightContext::FlightContext(std::chrono::sys_days const dof, std::chrono::minutes const sun_set)
//     : dateOfFlight_(dof), sunset_(sun_set) {}

FlightContext::FlightContext(std::chrono::system_clock::time_point const dof,
                             std::optional<std::chrono::system_clock::time_point> const sun_set)
    : dateOfFlight_(floor<days>(dof)), sunset_(xMinutes(sun_set)) {}

FlightContext::FlightContext(std::chrono::system_clock::time_point dof,
                             std::optional<std::chrono::system_clock::time_point> e_obt,
                             std::optional<std::chrono::system_clock::time_point> sun_set,
                             std::optional<FieldAtmosphere> o_rigin, std::optional<FieldAtmosphere> dest)
    : dateOfFlight_(floor<days>(dof)), eobt_(xMinutes(e_obt)), sunset_(xMinutes(sun_set)), origin_(o_rigin),
      destination_(dest) {}

FlightContext::FlightContext(std::chrono::sys_days dof, std::optional<std::chrono::minutes> e_obt,
                             std::optional<std::chrono::minutes> sun_set, std::optional<FieldAtmosphere> o_rigin,
                             std::optional<FieldAtmosphere> dest)
    : dateOfFlight_(dof), eobt_(e_obt), sunset_(sun_set), origin_(o_rigin), destination_(dest) {}

void FlightContext::dateOfFlight(system_clock::time_point const date) {
    dateOfFlight_ = floor<days>(date);
    sunset_       = {};
    eobt_         = {};
    origin_       = {};
    destination_  = {};
    BOOST_LOG_TRIVIAL(trace) << "DOF set to " << static_cast<unsigned>(dateOfFlight_ymd().day()) << "."
                             << static_cast<unsigned>(dateOfFlight_ymd().month()) << "."
                             << static_cast<int>(dateOfFlight_ymd().year()) << "."
                             << "; sunset reset";
}

system_clock::time_point FlightContext::dateOfFlight() const {
    return dateOfFlight_;
}

year_month_day FlightContext::dateOfFlight_ymd() const {
    return dateOfFlight_;
}

sys_days FlightContext::dateOfFlight_days() const {
    return dateOfFlight_;
}

bool FlightContext::past() const {
    return dateOfFlight_ < floor<days>(system_clock::now());
}

void FlightContext::sunset(std::optional<system_clock::time_point> const time) {
    sunset_         = xMinutes(time);
    auto const hhmm = hours_mins(sunset_min());
    if( hhmm )
        BOOST_LOG_TRIVIAL(trace) << "sunset set to " << hhmm.value().hours() << ":" << hhmm.value().minutes();
    else
        BOOST_LOG_TRIVIAL(trace) << "sunset reset";
}

std::optional<system_clock::time_point> FlightContext::sunset() const {
    if( sunset_ ) return dateOfFlight_ + sunset_.value();
    return std::nullopt;
}

// hh_mm_ss<minutes> FlightContext::sunset_hms() const {
//     return hh_mm_ss<minutes>{sunset_};
// }

std::optional<minutes> FlightContext::sunset_min() const {
    return sunset_;
}

void FlightContext::eobt(std::optional<system_clock::time_point> const time) {
    eobt_           = xMinutes(time);
    auto const hhmm = hours_mins(eobt_min());
    if( hhmm )
        BOOST_LOG_TRIVIAL(trace) << "eobt set to " << hhmm.value().hours() << ":" << hhmm.value().minutes();
    else
        BOOST_LOG_TRIVIAL(trace) << "eobt reset";
}

std::optional<system_clock::time_point> FlightContext::eobt() const {
    if( eobt_ ) return dateOfFlight_ + eobt_.value();
    return std::nullopt;
}

std::optional<minutes> FlightContext::eobt_min() const {
    return eobt_;
}

void FlightContext::origin(std::optional<FieldAtmosphere> atmo) {
    origin_ = std::move(atmo);
}

std::optional<FieldAtmosphere> FlightContext::origin() const {
    return origin_;
}

void FlightContext::destination(std::optional<FieldAtmosphere> atmo) {
    destination_ = std::move(atmo);
}

std::optional<FieldAtmosphere> FlightContext::destination() const {
    return destination_;
}

// hh_mm_ss<std::chrono::minutes> hours_mins(std::chrono::minutes const mins) {
//   return hh_mm_ss<minutes>{mins};
// }
std::optional<std::chrono::hh_mm_ss<std::chrono::minutes>> hours_mins(std::optional<std::chrono::minutes> const mins) {
    if( mins ) return hh_mm_ss<minutes>{mins.value()};
    return std::nullopt;
}

std::optional<std::chrono::minutes> xMinutes(std::optional<std::chrono::system_clock::time_point> const point) {
    return point ? std::optional{xMinutes(point.value())} : std::nullopt;
}
std::chrono::minutes xMinutes(std::chrono::system_clock::time_point const point) {
    return duration_cast<minutes>(point - floor<days>(point));
}

void tag_invoke(json::value_from_tag const &, json::value &value, FieldAtmosphere const &atmo) {
    value = {
        { "QNH",         atmo.qnh_},
        {"temp", atmo.temperature_},
    };
}

FieldAtmosphere tag_invoke(json::value_to_tag<FieldAtmosphere> const &, json::value const &value) {
    return {json::value_to<pressure_t>(value.at("QNH")), json::value_to<temperature_t>(value.at("temp"))};
}

void tag_invoke(json::value_from_tag const &, json::value &value, FlightContext const &cntxt) {
    auto &obj  = value.emplace_object();
    obj["dof"] = json::value_from(cntxt.dateOfFlight_days());
    if( cntxt.eobt_min() ) obj["eobt"] = json::value_from(cntxt.eobt_min().value());
    if( cntxt.sunset_min() ) obj["sunset"] = json::value_from(cntxt.sunset_min().value());
    if( cntxt.origin() ) obj["origin"] = json::value_from(cntxt.origin().value());
    if( cntxt.destination() ) obj["dest"] = json::value_from(cntxt.destination().value());
}

FlightContext tag_invoke(json::value_to_tag<FlightContext> const &, json::value const &value) {
    auto &obj = value.as_object();
    return {
        json::value_to<sys_days>(value.at("dof")),
        obj.if_contains("eobt") ? std::optional{json::value_to<minutes>(value.at("eobt"))} : std::nullopt,
        obj.if_contains("sunset") ? std::optional{json::value_to<minutes>(value.at("sunset"))} : std::nullopt,
        obj.if_contains("origin") ? std::optional{json::value_to<FieldAtmosphere>(value.at("origin"))} : std::nullopt,
        obj.if_contains("dest") ? std::optional{json::value_to<FieldAtmosphere>(value.at("dest"))} : std::nullopt,
    };
}
std::pair<true_altitude_t, pressure_altitude_t> lowestVfrFl(FieldAtmosphere const atmo) {
    for( pressure_altitude_t palt = pressure_level_1013 + 1500u * u_ft;; palt += 1000 * u_ft ) {
        true_altitude_t talt = convert_altitude(palt, atmo.qnh_);
        if( talt > mean_sea_level + 5000 * u_ft ) return {talt, palt};
    }
}