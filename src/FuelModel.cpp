#include "FuelModel.h"
#include <algorithm>
#include <boost/json.hpp>
#include "AircraftsViewModel.h"
#include "FuelPanel.h"
#include "vfrp-types-json.h"
#include <mp-units/math.h>

using mp_units::ceil;
using mp_units::round;
using mp_units::value_cast;

FuelModel::FuelModel() noexcept
    : cruise_{1*u_min,cruiseConsumption}, climbAdd_{1*u_min,climbConsumption-cruiseConsumption}, circuitAdd_{10*u_min,cruiseConsumption}, alternate_{1*u_min,cruiseConsumption},
      reserve_{30*u_min,cruiseConsumption}, extra_{0*u_min,cruiseConsumption}, stock_{0*u_min,cruiseConsumption} {
    //    addUp();
}

FuelModel::FuelModel(boost::json::value const &jvalue, Aircraft const &aircraft, FuelPanel &panel)
    : FuelModel() {
    consumption(aircraft.cruisingConsumption(),aircraft.climbConsumption(),panel);
    taxiAddAmount(boost::json::value_to<amount_t>(jvalue.at("taxi")),panel);
    circuitAddDuration(boost::json::value_to<duration_t>(jvalue.at("circuit")), panel);
    reserveDuration(boost::json::value_to<duration_t>(jvalue.at("reserve")), panel);
    extraAmount(boost::json::value_to<amount_t>(jvalue.at("extra")),panel);
}

void FuelModel::updateConsumption(Aircraft const &craft, FuelPanel &panel) {
    consumption(craft.cruisingConsumption(),craft.climbConsumption(),panel);
    cruiseDuration(cruise_.duration(),panel);
    climbAddDuration(climbAdd_.duration(),panel);
    circuitAddDuration(circuitAdd_.duration(),panel);
    alternateDuration(alternate_.duration(),panel);
    reserveDuration(reserve_.duration(),panel);
    extraAmount(extra_.amount(),panel);
}

void FuelModel::cruiseDuration(duration_t const duration, FuelPanel &panel) {
    cruise_ =  DurationAmount{duration,cruiseConsumption};
    panel.cruiseDisplay(cruise_.duration(),cruise_.amount());
    addUp(panel);
}

void FuelModel::taxiAddAmount(amount_t const taxi_add, FuelPanel &panel) { // FIXME: no need to re-display? or add to saved file and update()?
    taxiAdd_ = taxi_add;
    panel.taxiAddDisplay(taxiAdd_);
    addUp(panel);
}

void FuelModel::climbAddDuration(duration_t const duration, FuelPanel &panel) {
    climbAdd_ =  DurationAmount{duration,climbConsumption-cruiseConsumption};
    panel.climbAddDisplay(climbAdd_.duration(),climbAdd_.amount());
    addUp(panel);
}

void FuelModel::circuitAddDuration(duration_t const duration, FuelPanel &panel) {
    circuitAdd_ =  DurationAmount{duration,cruiseConsumption};
    panel.circuitAddDisplay(circuitAdd_.duration(),circuitAdd_.amount());
    addUp(panel);
}

void FuelModel::alternateDuration(duration_t const duration, FuelPanel &panel) {
    alternate_ =  DurationAmount{duration,cruiseConsumption};
    panel.alternateDisplay(alternate_.duration(),alternate_.amount());
    addUp(panel);
}

void FuelModel::reserveDuration(duration_t const duration, FuelPanel &panel) {
    reserve_ =  DurationAmount{duration,cruiseConsumption};
    panel.reserveDisplay(reserve_.duration(),reserve_.amount());
    addUp(panel);
}

void FuelModel::extraDuration(duration_t const duration, FuelPanel &panel) {
    extra_ =  DurationAmount{duration,cruiseConsumption};
    panel.extraDisplay(extra_.duration(),extra_.amount());
    addUp(panel);
}

void FuelModel::extraAmount(amount_t const amount, FuelPanel &panel) {
    extra_ =  DurationAmount{amount,cruiseConsumption};
    panel.extraDisplay(extra_.duration(),extra_.amount());
    addUp(panel);
}

duration_t FuelModel::safe() const noexcept {
    return safe_;
}

boost::json::object FuelModel::jsonise() const {
    return boost::json::object{
        {"taxi",taxiAdd_},
        {"circuit",circuitAdd_.duration()},
        {"reserve",reserve_.duration()},
        {"extra",extra_.amount()},
    };
}

void FuelModel::addUp(FuelPanel &panel) {
    minimal_ = cruise_.amount() + taxiAdd_ + climbAdd_.amount() + circuitAdd_.amount() + alternate_.amount() + reserve_.amount();
    panel.minimalDisplay(minimal_);
    stock_ = DurationAmount{minimal_+extra_.amount(),cruiseConsumption};
    panel.stockDisplay(stock_.duration(),stock_.amount());
    safe_ = stock_.duration() > 30*u_min
            ? duration_t{stock_.duration() - 30 * u_min}
            : duration_t::zero();
    panel.safeDisplay(safe_);
}

void FuelModel::consumption(consumption_t const cruise_c, consumption_t const climb_c, FuelPanel &panel) {
    cruiseConsumption = cruise_c;
    climbConsumption = climb_c;
    panel.consumptionLabel(cruiseConsumption,climbConsumption);
}

duration_t DurationAmount::duration() const noexcept {
    return duration_;
}

amount_t DurationAmount::amount() const noexcept {
    return amount_;
}

DurationAmount::DurationAmount(duration_t const new_duration, consumption_t const consumption)
    : duration_(new_duration),
      amount_(value_cast<amount_t::rep>(ceil<u_l>(consumption*duration_))) {}

DurationAmount::DurationAmount(amount_t const new_amount, consumption_t const consumption)
    : duration_(value_cast<duration_t ::rep>(round<u_min>((1. / consumption) * new_amount))),
      amount_(new_amount) {}
