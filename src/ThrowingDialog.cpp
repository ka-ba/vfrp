#include "ThrowingDialog.h"

ThrowingDialog::ThrowingDialog(wxWindow *parent, wxWindowID const id,wxString const &title,wxPoint const &pos,
                               wxSize const &size,long style,wxString const &name)
    : wxDialog(parent,id,title,pos,size,style,name) {}

int ThrowingDialog::ShowModal() {
    auto result = wxDialog::ShowModal();
    if(errorMsg) {
        assert(wxID_STOP==result);
        throw DialogException(errorMsg);
    }
    return result;
}

void ThrowingDialog::throwOverFence(char const *error) {
    errorMsg = error;
    EndModal(wxID_STOP);
}

DialogException::DialogException(char const *msg) noexcept
    : message(msg) {}

char const *DialogException::what() const noexcept {
    return message;
}
