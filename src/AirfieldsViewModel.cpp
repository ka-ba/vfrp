#include "AirfieldsViewModel.h"
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <boost/log/trivial.hpp>
#include "EnabledConfig.h"
#include "plain-values.h"

void InfoRadio::save(wxConfigBase &config) const {
    config.Write("infocall", infoCallsign(callsign_));
    config.Write("infofreq", plainKHz(frequency_));
}

std::optional<InfoRadio> InfoRadio::read(wxConfigBase const &config, wxString const &entry_id) {
    if( config.HasEntry(entry_id + "/infocall") )
        return InfoRadio{infoCallsign(config.Read(entry_id + "/infocall")),
                         freq_t{fromKHz(config.Read(entry_id + "/infofreq", plainKHz(freq_t::min())))}};
    return std::nullopt;
}

wxString InfoRadio::infoCallsign(Callsign const call) {
    switch( call ) {
        case Callsign::Radio :
            return "RADIO";
        case Callsign::Information :
            return "INFORMATION";
        case Callsign::Info :
            return "INFO";
        case Callsign::Illegal :
            [[fallthrough]];
        default :
            return "ILLEGAL";
    }
}

InfoRadio::Callsign InfoRadio::infoCallsign(wxString const &call) {
    if( "RADIO" == call ) return Callsign::Radio;
    if( "INFORMATION" == call ) return Callsign::Information;
    if( "INFO" == call ) return Callsign::Info;
    return Callsign::Illegal;
}

void AtcRadio::save(wxConfigBase &config) const {
    config.Write("atcapproach", plainKHz(approach_));
    config.Write("atctower", plainKHz(tower_));
    config.Write("atcground", plainKHz(ground_));
}

std::optional<AtcRadio> AtcRadio::read(wxConfigBase const &config, wxString const &entry_id) {
    if( config.HasEntry(entry_id + "/atctower") )
        return AtcRadio{freq_t{fromKHz(config.Read(entry_id + "/atcapproach", plainKHz(freq_t::min())))},
                        freq_t{fromKHz(config.Read(entry_id + "/atctower", plainKHz(freq_t::min())))},
                        freq_t{fromKHz(config.Read(entry_id + "/atcground", plainKHz(freq_t::min())))}};
    return std::nullopt;
}

wxRegEx const Airfield::regEx{"[^a-zA-Z0-9]"};

wxString Airfield::rwySurface(RwySurface const surf) {
    switch( surf ) {
        case RwySurface::Gras :
            return "GRAS";
        case RwySurface::Asph :
            return "ASPH";
        case RwySurface::Conc :
            return "CONC";
        case RwySurface::Illegal :
            [[fallthrough]];
        default :
            return "ILLEGAL";
    }
}

Airfield::RwySurface Airfield::rwySurface(wxString const &surf) {
    if( "GRAS" == surf ) return RwySurface::Gras;
    if( "ASPH" == surf ) return RwySurface::Asph;
    if( "CONC" == surf ) return RwySurface::Conc;
    return RwySurface::Illegal;
}

Airfield::Airfield(wxConfigBase const &config, wxString const &entry_id)
    : entryID_(entry_id), name_(config.Read(entry_id + "/name", "[UNDEFINED]")),
      icaoCode_(config.Read(entry_id + "/icao", "[UNDEFINED]")), rwyCode_(config.Read(entry_id + "/rwy", 1)),
      elevation_(fromFeetMSL(config.Read(entry_id + "/elev", 1))),
      rwyLength_(fromMetre(config.Read(entry_id + "/length", 1))), tora_(fromMetre(config.Read(entry_id + "/tora", 1))),
      reverseTora_(fromMetre(config.Read(entry_id + "/revtora", 1))),
      lda_(fromMetre(config.Read(entry_id + "/lda", 1))), reverseLda_(fromMetre(config.Read(entry_id + "/revlda", 1))),
      surface_(toRwySurface(config.Read(entry_id + "/surface", 255))), radio_(readRadio(config, entry_id)),
      fis_(fromKHz(config.Read(entry_id + "/fisfreq", plainKHz(freq_t::min())))) {
    assert(regEx.IsValid() && "airfield regex value");
}

Airfield::Airfield(wxString const &icao_code) : icaoCode_(icao_code) {}

Airfield::Radio Airfield::readRadio(wxConfigBase const &config, wxString const &entry_id) {
    auto atc = AtcRadio::read(config, entry_id);
    if( atc ) return *(atc);
    auto info = InfoRadio::read(config, entry_id);
    if( info ) return *(info);
    return InfoRadio{};
}

void Airfield::save(wxConfigBase &config) const {
    auto marker = EnabledConfig{config}.setPath(entryID_);
    config.Write("name", name_);
    config.Write("icao", icaoCode_);
    config.Write("rwy", rwyCode_);
    config.Write("elev", plainFeetMSL(elevation_));
    config.Write("length", plainMetre(rwyLength_));
    config.Write("tora", plainMetre(tora_));
    config.Write("revtora", plainMetre(reverseTora_));
    config.Write("lda", plainMetre(lda_));
    config.Write("revlda", plainMetre(reverseLda_));
    config.Write("surface", fromRwySurface(surface_));
    if( hasATC() )
        std::get<AtcRadio>(radio_).save(config);
    else
        std::get<InfoRadio>(radio_).save(config);
    config.Write("fisfreq", plainKHz(fis_));
}

// duplicated code :(
void Airfield::improveEntryId(std::set<wxString> &ids) {
    [[maybe_unused]] auto before = entryID_;
    entryID_                     = icaoCode_;
    regEx.Replace(&entryID_, "");
    entryID_.UpperCase();
    while( ids.end() != ids.find(entryID_) )
        entryID_ += "X";
    ids.insert(entryID_);
    BOOST_LOG_TRIVIAL(debug) << "improved entry id from " << before << " to " << entryID_;
}

wxString const &Airfield::name() const noexcept {
    return name_;
}

void Airfield::name(wxString const &n_ame) {
    name_ = n_ame;
}

wxString const &Airfield::icaoCode() const noexcept {
    return icaoCode_;
}

void Airfield::icaoCode(wxString const &icao_code) {
    icaoCode_ = icao_code;
}

uint8_t Airfield::rwyCode() const noexcept {
    return rwyCode_;
}

void Airfield::rwyCode(uint8_t rwy_code) noexcept {
    rwyCode_ = rwy_code;
}

wxString Airfield::rwyName() const {
    return rwyName(false) + "/" + rwyName(true);
}

wxString Airfield::rwyName(bool const reverse) const {
    std::ostringstream stream;
    stream << std::setfill('0') << std::setw(2) << rwyCode_ + 18 * reverse;
    return stream.str();
}

elevation_t Airfield::elevation() const noexcept {
    return elevation_;
}

void Airfield::elevation(elevation_t const alt) noexcept {
    elevation_ = alt;
}

length_t Airfield::rwyLength() const noexcept {
    return rwyLength_;
}

void Airfield::rwyLength(length_t const len) noexcept {
    rwyLength_ = len;
}

length_t Airfield::tora(bool reverse) const noexcept {
    return reverse ? reverseTora_ : tora_;
}

void Airfield::tora(length_t const t_ora, bool const reverse) noexcept {
    (reverse ? reverseTora_ : tora_) = t_ora;
}

length_t Airfield::lda(bool reverse) const noexcept {
    return reverse ? reverseLda_ : lda_;
}

void Airfield::lda(length_t const l_da, bool const reverse) noexcept {
    (reverse ? reverseLda_ : lda_) = l_da;
}

Airfield::RwySurface Airfield::surface() const noexcept {
    return surface_;
}

uint8_t Airfield::fromRwySurface(RwySurface const s_urface) {
    return static_cast<uint8_t>(s_urface);
}

Airfield::RwySurface Airfield::toRwySurface(uint8_t const s_urface) {
    if( s_urface > fromRwySurface(Airfield::RwySurface::Conc) )
        throw std::invalid_argument(std::string{"can't toRwySurface value "}.append(std::to_string(s_urface)));
    return static_cast<Airfield::RwySurface>(s_urface);
}

wxString Airfield::surfaceName() const {
    return rwySurface(surface_);
}

bool Airfield::hasATC() const {
    return std::holds_alternative<AtcRadio>(radio_);
}

AtcRadio &Airfield::atcRadio() {
    return std::get<AtcRadio>(radio_);
}

AtcRadio const &Airfield::atcRadio() const {
    return std::get<AtcRadio>(radio_);
}

InfoRadio &Airfield::infoRadio() {
    return std::get<InfoRadio>(radio_);
}

InfoRadio const &Airfield::infoRadio() const {
    return std::get<InfoRadio>(radio_);
}

freq_t Airfield::fisFreq() const noexcept {
    return fis_;
}

void Airfield::fisFreq(freq_t const fis_freq) noexcept {
    fis_ = fis_freq;
}

void Airfield::radio(Radio const &radio) noexcept {
    radio_ = radio;
}

void Airfield::surfaceName(wxString const surface_name) {
    surface_ = rwySurface(surface_name);
}

AirfieldsViewModel::AirfieldsViewModel(wxConfigBase const &config) : hasChanges(false) {
    IncRef();  // protected against hostile destruction (?) FIXME: this good or bad? Seems to avoid double free on prg
               // exit
    fields = loadAirfields(config);
}

AirfieldsViewModel &AirfieldsViewModel::instance(wxConfigBase const &config) {
    static AirfieldsViewModel airfields_view_model{config};
    return airfields_view_model;
}

AirfieldsViewModel const &AirfieldsViewModel::instanceConst() {
    return instance(wxFileConfig{});  // dummy config will be ignored on subsequent calls
}

unsigned int AirfieldsViewModel::GetColumnCount() const {
    return Columns::colCount;
}

wxString AirfieldsViewModel::GetColumnType([[maybe_unused]] unsigned int col) const {
    return wxVariant{"bla"}.GetType();
}

void AirfieldsViewModel::GetValue(wxVariant &variant, wxDataViewItem const &item, unsigned int const col) const {
    if( item.IsOk() ) {
        auto field = verifyAirfield(item);
        switch( col ) {
            case name :
                variant = field->name();
                break;
            case icao :
                variant = field->icaoCode();
                break;
            case rwyName :
                variant = field->rwyName();
                break;
            case elev :
                variant = plainFeetMSL<long>(field->elevation());
                break;
            case rwyLength :
                variant = plainMetre<long>(field->rwyLength());
                break;
            case surfaceName :
                variant = field->surfaceName();
                break;
            case combinedName :
                variant = field->icaoCode() + " - " + field->name();
                break;
            case rwy :
                variant = long{field->rwyCode()};
                break;
            case tora :
                variant = plainMetre<long>(field->tora(false));
                break;
            case revTora :
                variant = plainMetre<long>(field->tora(true));
                break;
            case lda :
                variant = plainMetre<long>(field->lda(false));
                break;
            case revLda :
                variant = plainMetre<long>(field->lda(true));
                break;
            default :
                variant = "[RANGE]";
        }
    } else {
        variant = "[INVALID]";
    }
}

bool AirfieldsViewModel::SetValue(wxVariant const &variant, wxDataViewItem const &item, unsigned int const col) {
    if( item.IsOk() ) {
        auto field = verifyAirfield(item);
        switch( col ) {
            case name :
                field->name(variant);
                break;
            case icao :
                field->icaoCode(variant);
                break;
            case elev :
                field->elevation(fromFeetMSL(variant.GetLong()));
                break;
            case rwyLength :
                field->rwyLength(fromMetre(variant.GetLong()));
                break;
            case surfaceName :
                field->surfaceName(variant);
                break;
            case rwy :
                field->rwyCode(variant.GetLong());
                break;
            case tora :
                field->tora(fromMetre(variant.GetLong()), false);
                break;
            case revTora :
                field->tora(fromMetre(variant.GetLong()), true);
                break;
            case lda :
                field->lda(fromMetre(variant.GetLong()), false);
                break;
            case revLda :
                field->lda(fromMetre(variant.GetLong()), true);
                break;
            case radioType : {
                switch( variant.GetInteger() ) {
                    case RadioType::Radio :
                        field->radio(InfoRadio{});
                        break;
                    case RadioType::Atc :
                        field->radio(AtcRadio{});
                        break;
                }
                break;
            }
            case radioName :
                field->infoRadio().callsign_ = InfoRadio::infoCallsign(variant);
                break;
            case radioFreq :
                field->infoRadio().frequency_ = fromKHz(variant.GetLong());
                break;
            case apprFreq :
                field->atcRadio().approach_ = fromKHz(variant.GetLong());
                break;
            case towerFreq :
                field->atcRadio().tower_ = fromKHz(variant.GetLong());
                break;
            case gndFreq :
                field->atcRadio().ground_ = fromKHz(variant.GetLong());
                break;
            case fisFreq :
                field->fisFreq(fromKHz(variant.GetLong()));
                break;
            default :
                return false;
        }
        hasChanges = true;
        return true;
    }
    return false;
}

wxDataViewItem AirfieldsViewModel::GetParent(wxDataViewItem const &) const {
    return wxDataViewItem{};
}

bool AirfieldsViewModel::IsContainer(wxDataViewItem const &item) const {
    return ! item.IsOk();
}

unsigned int AirfieldsViewModel::GetChildren(wxDataViewItem const &item, wxDataViewItemArray &children) const {
    children.clear();
    if( item.IsOk() ) return 0;
    // a subtle const error in wxWidgets API leads to const_cast here (1/2)
    for( std::shared_ptr<Airfield> &field : const_cast<std::deque<std::shared_ptr<Airfield>> &>(fields) )
        children.Add(wxDataViewItem{reinterpret_cast<void *>(&field)});
    return fields.size();
}

wxDataViewItem AirfieldsViewModel::item(wxString const &icao_code) const {
    auto const iter = std::find_if(fields.cbegin(), fields.cend(),
                                   [&icao_code](auto const &field) { return field->icaoCode() == icao_code; });
    if( fields.cend() != iter )
        // a subtle const error in wxWidgets API leads to const_cast here (2/2)
        return wxDataViewItem{reinterpret_cast<void *>(&const_cast<std::shared_ptr<Airfield> &>(*iter))};
    return {};
}

std::deque<std::shared_ptr<Airfield>> AirfieldsViewModel::loadAirfields(wxConfigBase const &config) {
    std::deque<std::shared_ptr<Airfield>> af;
    long cookie;
    wxString entry_id;
    auto more_items = config.GetFirstGroup(entry_id, cookie);
    while( more_items ) {
        BOOST_LOG_TRIVIAL(trace) << "loading airfield " << entry_id;
        af.push_back(std::make_shared<Airfield>(config, entry_id));
        more_items = config.GetNextGroup(entry_id, cookie);
    }
    return af;
}

void AirfieldsViewModel::save(wxConfigBase &config) {
    if( hasChanges ) {
        std::set<wxString> ids;
        EnabledConfig::deleteAllGroups(config);
        for( auto &field : fields ) {
            field->improveEntryId(ids);
            field->save(config);
        }
        hasChanges = false;
        config.Flush();
        BOOST_LOG_TRIVIAL(info) << "saved airfields";
    } else {
        BOOST_LOG_TRIVIAL(debug) << "airfields not saved because nothing changed";
    }
}

std::shared_ptr<Airfield> AirfieldsViewModel::verifyAirfield(wxDataViewItem const &item) {
    return verifyAirfieldImpl(item);
}
std::shared_ptr<Airfield const> AirfieldsViewModel::verifyAirfield(wxDataViewItem const &item) const {
    return verifyAirfieldImpl(item);
}
std::shared_ptr<Airfield> AirfieldsViewModel::verifyAirfieldImpl(wxDataViewItem const &item) const {
    auto iter = std::find_if(fields.cbegin(), fields.cend(), [&item](auto const &af) {
        return reinterpret_cast<std::shared_ptr<Airfield> *>(item.GetID())
               == &af;  // looks a bit strange after move to shared_ptr
    });
    if( fields.cend() == iter ) throw std::logic_error{"requested airfield not in vector"};
    // const cast because function is accessed through access functions which handle const-nes
    return const_cast<std::shared_ptr<Airfield> &>(*iter);
}

std::shared_ptr<Airfield const> AirfieldsViewModel::airfieldOrDummy(wxString const &icao_code) const {
    auto const exact = std::find_if(fields.cbegin(), fields.cend(),
                                    [&icao_code](auto const &field) { return field->icaoCode() == icao_code; });
    if( fields.cend() == exact ) {
        auto const icao_lower  = icao_code.Lower();
        auto const insensitive = std::find_if(fields.cbegin(), fields.cend(), [&icao_code](auto const &field) {
            return field->icaoCode().Lower() == icao_code;
        });
        if( fields.cend() == insensitive ) return std::make_shared<Airfield>(icao_code);
        return *insensitive;
    }
    return *exact;
}