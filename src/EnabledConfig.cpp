#include "EnabledConfig.h"
#include <vector>

namespace vfrphelper {
template<class C,typename T>
std::optional<C> read_pair(wxConfigBase const &config,wxString const &key,wxString const &keyadd0,wxString const &keyadd1) {
    T value0{},value1{};
    if(!config.Read(key+keyadd0,&value0))
        return std::nullopt;
    if(!config.Read(key+keyadd1,&value1))
        return std::nullopt;
    return C{value0,value1};
}
}  // namespace vfrphelper

EnabledConfig::EnabledConfig(wxConfigBase &config_to_use)
    :config_(config_to_use) {}

wxConfigBase &EnabledConfig::configBase() {
    return config_;
}

wxConfigBase const &EnabledConfig::configBase() const {
    return config_;
}

wxPoint EnabledConfig::point(wxString const &key, wxPoint preset) const {
    return point(key).value_or(preset);
}

std::optional<wxPoint> EnabledConfig::point(wxString const &key) const {
    return vfrphelper::read_pair<wxPoint,int>(config_,key,"/x","/y");
}

void EnabledConfig::storePoint(wxString const &key, wxPoint const point)
{
    config_.Write(key+"/x",point.x);
    config_.Write(key+"/y",point.y);
}

wxSize EnabledConfig::size(wxString const &key, wxSize preset) const {
    return size(key).value_or(preset);
}

std::optional<wxSize> EnabledConfig::size(wxString const &key) const {
    return vfrphelper::read_pair<wxSize,int>(config_,key,"/w","/h");
}

void EnabledConfig::storeSize(wxString const &key,wxSize const size)
{
    config_.Write(key+"/w",size.x);
    config_.Write(key+"/h",size.y);
}

PathMarker EnabledConfig::setPath(wxString const &new_path) {
    return PathMarker{config_,new_path};
}

void EnabledConfig::deleteAllGroups(wxConfigBase &config) {
    [[maybe_unused]] auto path_before = config.GetPath();
    std::vector<wxString> groups(config.GetNumberOfGroups());
    assert(config.GetNumberOfGroups()==groups.size());
    long cookie{};
    wxString entry;
    auto groups_iter = groups.begin();
    auto got_item = config.GetFirstGroup(entry,cookie);
    while(got_item) {
        *groups_iter++ = entry;
        got_item = config.GetNextGroup(entry,cookie);
    }
    assert(groups.end()==groups_iter);
    for(auto const &group : groups)
        config.DeleteGroup(group);
    assert(config.GetPath()==path_before);
}

double EnabledConfig::Read(wxConfigBase const &config, const wxString &key, double def_val)
{
    double value{def_val};
    if( config.Read(key,&value,def_val) )
        return value;
    else
        return def_val;
}

PathMarker::PathMarker(wxConfigBase &config, wxString const &new_path)
    : config_(config), markedPath_(config_.GetPath()) {
    config_.SetPath(new_path);
}

PathMarker::~PathMarker() {
    config_.SetPath(markedPath_);
}
