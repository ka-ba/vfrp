#include "FlightPathPanel.h"
#include <memory>
#include <boost/json.hpp>
#include <boost/log/trivial.hpp>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include "AircraftsViewModel.h"
#include "AirfieldsViewModel.h"
#include "MainFrame.h"
#include "plain-values.h"
#include "trigonometry.h"
#include "util.h"
#include "wx-shortcuts.h"
using namespace mp_units;

FlightPathPanel::FlightPathPanel(wxWindow *parent, wxWindowID const winid, wxPoint const &pos, wxSize const &size,
                                 long const style, wxString const &name)
    : wxPanel(parent, winid, pos, size, style, name) {
    initGUI();
    assert(! selectedAircraft);
}

void FlightPathPanel::init(std::shared_ptr<Airfield const> origin, std::shared_ptr<Airfield const> destination,
                           /*wxObjectDataPtr<AirfieldsViewModel> const airfields_view_model,*/ Aircraft const &craft) {
    topParent = ancestorWindow<TopFrame>(this /*GetParent()*/);
    if( ! topParent ) throw std::invalid_argument("FlightPathPanel needs MainFrame as ancestor");
    selectedAircraft = &craft;
    flightPath_      = FlightPath{origin, destination};
    assert(selectedAircraft);
    layoutFlightpath();
}

void FlightPathPanel::changeOrigin(std::shared_ptr<Airfield> const airfield) {
    flightPath_.origin(airfield);
    layoutFlightpath();
}

std::shared_ptr<Airfield const> FlightPathPanel::origin() const {
    return flightPath_.origin();
}

void FlightPathPanel::changeDestination(std::shared_ptr<Airfield> const airfield) {
    flightPath_.leg().checkpoint_ = airfield;
    layoutFlightpath();
}

std::shared_ptr<Airfield const> FlightPathPanel::destination() const {
    return flightPath_.leg().checkpointField();
}

void FlightPathPanel::changeAlternate(std::shared_ptr<Airfield> const airfield) {
    flightPath_.alternate(airfield);
    layoutFlightpath();
}

std::shared_ptr<Airfield const> FlightPathPanel::alternate() const {
    return flightPath_.alternate().checkpointField();
}

void FlightPathPanel::changeAircraft(Aircraft const &craft) {
    selectedAircraft = &craft;
    flightPath_.recalc(*selectedAircraft);
    layoutFlightpath();
    topParent->flightPathUpdated(flightPath_);
}

void FlightPathPanel::updateSafeDuration(duration_t const minutes) {
    if( safeDurLabel ) safeDurLabel->SetLabel(wxT("") + hhmm(minutes));
}

void FlightPathPanel::loadFlightpath(wxString const &path) {
    assert(selectedAircraft);
    flightPath_ = FlightPath{path.ToStdString(), *selectedAircraft};
    layoutFlightpath();
    topParent->flightPathUpdated(flightPath_);
}

void FlightPathPanel::fromJson(boost::json::value const &jvalue, Aircraft const &aircraft) {
    selectedAircraft = &aircraft;
    assert(selectedAircraft);
    flightPath_ = FlightPath{jvalue, *selectedAircraft};
    layoutFlightpath();
}

boost::json::object FlightPathPanel::jsoniseFlightpath() const {
    return flightPath_.jsonise();
}

FlightPath const &FlightPathPanel::path() const noexcept {
    return flightPath_;
}

void FlightPathPanel::initGUI() {
    auto path_sizer = std::make_unique<wxGridBagSizer>(1, 1);
    path_sizer->SetFlexibleDirection(wxBOTH);
    path_sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_ALL /*wxFLEX_GROWMODE_SPECIFIED*/);
    SetSizer(path_sizer.release());
}

void FlightPathPanel::addLabel(wxGridBagSizer *sizer, wxString const &text, int const wrap, int const col) {
    addText(sizer, text, wrap, {1, col}, wxALIGN_CENTRE_HORIZONTAL | wxALIGN_BOTTOM, 1, wxALIGN_CENTER_HORIZONTAL);
}

wxStaticText *FlightPathPanel::addText(wxGridBagSizer *sizer, wxString const &text, int const wrap,
                                       wxGBPosition const &pos, int const sizer_flags, int const rowspan,
                                       int const text_flags) {
    auto label = new wxStaticText(this, wxID_ANY, text, wxDefaultPosition, wxDefaultSize, text_flags);
    label->Wrap(wrap);
    sizer->Add(label, pos, wxGBSpan(rowspan, 1), sizer_flags | wxALL, 0);
    return label;
}

void FlightPathPanel::addSpin(wxGridBagSizer *sizer, wxWindowID const wid, wxRange const range, int const init,
                              wxObjectEventFunction const handler, wxString const &example, wxGBPosition const &pos) {
    auto spin = new wxSpinCtrl(this, wid, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT | wxSP_ARROW_KEYS,
                               range.GetMin(), range.GetMax(), init);
    sizeByExample(*spin, example);
    if( handler ) spin->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, handler, NULL, this);
    sizer->Add(spin, pos, wxGBSpan(4, 1), wxALIGN_CENTER_VERTICAL | wxALL, 0);
}

constexpr int fpcol_CP = 2, fpcol_SEC = 4, fpcol_ALT = 5, fpcol_TAS = 6, fpcol_W_DIR = 7, fpcol_W_V = 8, fpcol_TC = 9,
              fpcol_WCA = 10, fpcol_VAR = 11, fpcol_MH = 12, fpcol_MC = 13, fpcol_DIST = 14, fpcol_DISTSUM = 15,
              fpcol_GS = 16, fpcol_CLMB = 17, fpcol_DUR = 18, fpcol_DSCND = 19, fpcol_DURSUM = 20, fpcol_NOTES =21;

wxColour fieldColour(std::shared_ptr<Airfield const> field) {
    if( 3 > field.use_count() ) return wxColour{*wxRED};
    return wxColour{*wxBLACK};
}

void FlightPathPanel::layoutFlightpath() {
    auto path_sizer = dynamic_cast<wxGridBagSizer *>(GetSizer());
    assert(path_sizer);
    path_sizer->Clear(true);
    // TODO: clear ButtonIndex as well, release ids
    assert(selectedAircraft);
    flightPath_.recalc(*selectedAircraft);

    addLabel(path_sizer, wxT("Checkpunkte"), 0, fpcol_CP);
    addLabel(path_sizer, wxT("sichere Höhe"), 50, fpcol_SEC);
    addLabel(path_sizer, wxT("Höhe / FL"), -1, fpcol_ALT);
    addLabel(path_sizer, wxT("TAS"), 0, fpcol_TAS);
    addLabel(path_sizer, wxT("W dir"), -1, fpcol_W_DIR);
    addLabel(path_sizer, wxT("W v"), -1, fpcol_W_V);
    addLabel(path_sizer, wxT("TC"), 0, fpcol_TC);
    addLabel(path_sizer, wxT("WCA"), 0, fpcol_WCA);
    addLabel(path_sizer, wxT("VAR"), 0, fpcol_VAR);
    addLabel(path_sizer, wxT("MH"), 0, fpcol_MH);
    addLabel(path_sizer, wxT("MC"), 0, fpcol_MC);
    addLabel(path_sizer, wxT("Entfernung Abschnitt"), 50, fpcol_DIST);
    addLabel(path_sizer, wxT("Entfernung Summe"), 50, fpcol_DISTSUM);
    addLabel(path_sizer, wxT("GS"), 0, fpcol_GS);
    addLabel(path_sizer, wxT("climb min"), 30, fpcol_CLMB);
    addLabel(path_sizer, wxT("total min"), 30, fpcol_DUR);
    addLabel(path_sizer, wxT("desc min"), 30, fpcol_DSCND);
    addLabel(path_sizer, wxT("min Summe"), 30, fpcol_DURSUM);
    addLabel(path_sizer, wxT("Bemerkung"), 0, fpcol_NOTES);
    addText(path_sizer, wxT("sichere Flugzeit:"), 0, {0, fpcol_DSCND}, wxALIGN_CENTER_VERTICAL | wxEXPAND, 1,
            wxALIGN_RIGHT);  // FIXME: do col_span
    safeDurLabel =
        addText(path_sizer, wxT("x:xx"), 0, {0, fpcol_DURSUM}, wxALIGN_CENTER_VERTICAL | wxEXPAND, 1, wxALIGN_RIGHT);

    auto origin = addText(path_sizer, flightPath_.origin()->icaoCode(), 0, {2, fpcol_CP});
    origin->SetForegroundColour(fieldColour(flightPath_.origin()));
    addText(path_sizer, std::to_string(plainFeetMSL(flightPath_.origin()->elevation())), 0, {2, fpcol_ALT},
            wxALIGN_CENTER, 2);

    auto const fp_size = flightPath_.size();
    for( size_t i = 0; i < fp_size; ++i )
        layoutLeg(i, flightPath_.isLast(i), false, flightPath_.leg(i), path_sizer);

    layoutLeg(fp_size + 1, true, true, flightPath_.alternate(), path_sizer);

    Layout();
    path_sizer->Fit(this);
}

wxWindowID FlightPathPanel::wxwid4Index(size_t const index) {
    auto id         = wxWindow::NewControlId();
    buttonIndex[id] = index;
    return id;
}

int FlightPathPanel::legRow(size_t const index) {
    return static_cast<int>(4 + (index * 4));
}

void FlightPathPanel::layoutLeg(size_t const index, bool const last, bool const alternate, Leg const &leg,
                                wxGridBagSizer *gb_sizer) {
    int const leg_row     = legRow(index);  // 4+(index*4); // wx likes ints
    size_t const id_index = alternate ? FlightPath::ALTINDEX : index;
    if( ! alternate ) {
        auto insert_leg_button = std::make_unique<wxButton>(this, wxwid4Index(id_index), wxT("+"), wxDefaultPosition,
                                                            wxDefaultSize, wxBU_EXACTFIT /*|wxBU_NOTEXT*/);
        insert_leg_button->SetToolTip(wxT("Leg einfügen"));
        insert_leg_button->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(FlightPathPanel::insertLeg),
                                   NULL, this);
        gb_sizer->Add(insert_leg_button.release(), wxGBPosition(leg_row, 0), wxGBSpan(4, 1), wxALIGN_CENTER | wxALL, 0);
    }
    if( ! last ) {
        auto del_leg_button = std::make_unique<wxButton>(this, wxwid4Index(id_index), wxT("-"), wxDefaultPosition,
                                                         wxDefaultSize, wxBU_EXACTFIT /*|wxBU_NOTEXT*/);
        del_leg_button->SetToolTip(wxT("Leg löschen"));
        del_leg_button->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(FlightPathPanel::deleteLeg), NULL,
                                this);
        gb_sizer->Add(del_leg_button.release(), wxGBPosition(leg_row + 2, 1), wxGBSpan(4, 1), wxALIGN_CENTER | wxALL,
                      0);
    }
    auto c_checkpoint = [this, id_index, leg_row, last, &leg, &gb_sizer]() -> wxWindow * {
        if( last ) {
            std::unique_ptr<wxStaticText> checkpoint{
                new wxStaticText(this, wxID_ANY, leg.checkpointName(), wxDefaultPosition, wxDefaultSize, 0)};
            if( leg.checkpointIsField() ) {
                auto c_delev =
                    new wxStaticText(this, wxID_ANY, std::to_string(plainFeetMSL(leg.checkpointField()->elevation())),
                                     wxDefaultPosition, wxDefaultSize, 0);
                gb_sizer->Add(c_delev, wxGBPosition(leg_row + 4, fpcol_ALT), wxGBSpan(2, 1), wxALIGN_CENTER | wxALL, 0);
                checkpoint->SetForegroundColour(fieldColour(leg.checkpointField()));
            } else
                checkpoint->SetForegroundColour(wxColour{*wxBLUE});
            return checkpoint.release();
        } else {
            auto text_ctrl = new wxTextCtrl(this, wxwid4Index(id_index), leg.checkpointName(), wxDefaultPosition,
                                            wxDefaultSize, wxTE_DONTWRAP | wxTE_LEFT | wxTE_NO_VSCROLL);
            text_ctrl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(FlightPathPanel::changeCheckpoint),
                               NULL, this);
            return text_ctrl;
        }
    }();
    gb_sizer->Add(c_checkpoint, wxGBPosition(leg_row + 2, fpcol_CP), wxGBSpan(4, 1), wxALIGN_CENTER_VERTICAL | wxALL,
                  0);

    addSpin(gb_sizer, wxwid4Index(id_index), {0, 10000}, plainFeetMSL(leg.secureAltitude_),
            wxCommandEventHandler(FlightPathPanel::changedSecALT), "10000", {leg_row, fpcol_SEC});
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 10000}, plainFeetMSL(leg.altitude_),
            wxCommandEventHandler(FlightPathPanel::changedALT), "10000", {leg_row, fpcol_ALT});
    addText(gb_sizer, leg.tAS_, 0, {leg_row, fpcol_TAS}, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 360}, plainDeg_0_359(leg.wind_.angle),
            wxCommandEventHandler(FlightPathPanel::changedWdir), "666", {leg_row, fpcol_W_DIR});
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 99}, plainKnots(leg.wind_.speed),
            wxCommandEventHandler(FlightPathPanel::changedWv), "66", {leg_row, fpcol_W_V});
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 360}, plainDeg_0_359(leg.tC_),
            wxCommandEventHandler(FlightPathPanel::changedTC), "666", {leg_row, fpcol_TC});
    addText(gb_sizer, wxString{std::to_string(plainDeg_0_359(leg.wCA_))}, 0, {leg_row, fpcol_WCA},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 360}, plainDeg_0_359(leg.var_),
            wxCommandEventHandler(FlightPathPanel::changedVAR), "666", {leg_row, fpcol_VAR});
    addText(gb_sizer, wxString{std::to_string(plainDeg_0_359(leg.mH_))}, 0, {leg_row, fpcol_MH},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(gb_sizer, wxString{std::to_string(plainDeg_0_359(leg.mC_))}, 0, {leg_row, fpcol_MC},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addSpin(gb_sizer, wxwid4Index(id_index), {0, 999}, plainNM(leg.distance_),
            wxCommandEventHandler(FlightPathPanel::changedDist), "666", {leg_row, fpcol_DIST});
    addText(gb_sizer, wxString{std::to_string(plainNM(leg.distanceSum_))}, 0, {leg_row, fpcol_DISTSUM},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(gb_sizer, wxString{std::to_string(plainKnots(leg.gS_))}, 0, {leg_row, fpcol_GS},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(gb_sizer,
            wxString{(plainMins(leg.preClimb_) ? std::string{"^"} + std::to_string(plainMins(leg.preClimb_)) : "")}, 0,
            {leg_row, fpcol_CLMB}, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(gb_sizer, wxString{std::to_string(plainMins(leg.legDuration_))}, 0, {leg_row, fpcol_DUR},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(
        gb_sizer,
        wxString{(plainMins(leg.postDescend_) ? std::string{"v"} + std::to_string(plainMins(leg.postDescend_)) : "")},
        0, {leg_row, fpcol_DSCND}, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    addText(gb_sizer, wxString{std::to_string(plainMins(leg.durationSum_))}, 0, {leg_row, fpcol_DURSUM},
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    auto notes_ctrl = new wxTextCtrl(this, wxwid4Index(id_index), leg.notes_, wxDefaultPosition,
                                    wxDefaultSize, wxTE_DONTWRAP | wxTE_LEFT | wxTE_NO_VSCROLL);
    notes_ctrl->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(FlightPathPanel::changeNotes),
                       NULL, this);
gb_sizer->Add(notes_ctrl, wxGBPosition(leg_row, fpcol_NOTES), wxGBSpan{4,1}, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT,
              0);
}

void FlightPathPanel::updateLeg(size_t const index, Leg const &leg, wxGridBagSizer *gb_sizer) {
    // FIXME: some are missing
    auto const leg_row = legRow(index);
    assert(gb_sizer && "null gb_sizer in updateLeg");
    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_WCA}) && "no item at fpcol_WCA");
    auto c_wca =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_WCA))->GetWindow());
    assert(c_wca);
    c_wca->SetLabel(wxString{std::to_string(plainDeg_0_359(leg.wCA_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_MH}) && "no item at fpcol_MH");
    auto c_mh =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_MH))->GetWindow());
    assert(c_mh);
    c_mh->SetLabel(wxString{std::to_string(plainDeg_0_359(leg.mH_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_MC}) && "no item at fpcol_MC");
    auto c_mc =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_MC))->GetWindow());
    assert(c_mc);
    c_mc->SetLabel(wxString{std::to_string(plainDeg_0_359(leg.mC_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_DISTSUM}) && "no item at fpcol_DISTSUM");
    auto c_dist_sum =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_DISTSUM))->GetWindow());
    assert(c_dist_sum);
    c_dist_sum->SetLabel(wxString{std::to_string(plainNM(leg.distanceSum_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_GS}) && "no item at fpcol_GS");
    auto c_gs =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_GS))->GetWindow());
    assert(c_gs);
    c_gs->SetLabel(wxString{std::to_string(plainKnots(leg.gS_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_DUR}) && "no item at fpcol_DUR");
    auto c_duration =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_DUR))->GetWindow());
    assert(c_duration);
    c_duration->SetLabel(wxString{std::to_string(plainMins(leg.legDuration_))});

    assert(gb_sizer->FindItemAtPosition({leg_row, fpcol_DURSUM}) && "no item at fpcol_DURSUM");
    auto c_dur_sum =
        dynamic_cast<wxStaticText *>(gb_sizer->FindItemAtPosition(wxGBPosition(leg_row, fpcol_DURSUM))->GetWindow());
    assert(c_dur_sum);
    c_dur_sum->SetLabel(wxString{std::to_string(plainMins(leg.durationSum_))});
    gb_sizer->Layout();
}

#include <iostream>
void FlightPathPanel::insertLeg(wxCommandEvent &event) {
    BOOST_LOG_TRIVIAL(trace) << "insert leg " << event.GetId() << " -> " << buttonIndex.at(event.GetId()) << "\n";
    [[maybe_unused]] auto before = flightPath_.size();
    flightPath_.insert(Leg{}, buttonIndex.at(event.GetId()));
    assert(before == flightPath_.size() - 1);
    layoutFlightpath();
    event.StopPropagation();
}

void FlightPathPanel::deleteLeg(wxCommandEvent &event) {
    BOOST_LOG_TRIVIAL(trace) << "delete leg " << event.GetId() << " -> " << buttonIndex.at(event.GetId()) << "\n";
    [[maybe_unused]] auto before = flightPath_.size();
    flightPath_.deleteLeg(buttonIndex.at(event.GetId()));
    assert(before == flightPath_.size() + 1);
    layoutFlightpath();
    event.StopPropagation();
}

void FlightPathPanel::changeCheckpoint(wxCommandEvent &event) {
    BOOST_LOG_TRIVIAL(trace) << "change checkpoint " << event.GetString() << " | " << event.GetId() << " -> "
                             << buttonIndex.at(event.GetId()) << "\n";
    flightPath_.leg(buttonIndex.at(event.GetId())).checkpoint_ = event.GetString().ToStdString();
    event.StopPropagation();
}

void FlightPathPanel::changedLegInput(wxCommandEvent &event, int const input) {
    try {
        auto const leg_row = buttonIndex.at(event.GetId());
        BOOST_LOG_TRIVIAL(trace) << "changed leg input, type " << input << ", in row " << leg_row;
        auto &leg   = flightPath_.leg(leg_row);
        bool update = true;
        switch( input ) {
            // case fpcol_CP:
            case fpcol_SEC :
                leg.secureAltitude_ = event.GetInt() * r_ft_talt + mean_sea_level;
                update              = false;
                break;
            case fpcol_ALT :
                leg.altitude_ = event.GetInt() * r_ft_talt + mean_sea_level;
                break;
                //    case fpcol_TAS:
                //        leg.tAS_ = event.GetInt();
                //        break;
            case fpcol_W_DIR :
                leg.wind_.angle = angle_t{degree_base_t{event.GetInt()} * si::degree};
                break;
            case fpcol_W_V :
                leg.wind_.speed = event.GetInt() * u_kt;
                break;
            case fpcol_TC :
                leg.tC_ = angle_t{degree_base_t{event.GetInt()} * si::degree};
                break;
            case fpcol_VAR :
                leg.var_ = angle_t{degree_base_t{event.GetInt()} * si::degree};
                break;
            // case fpcol_MC:
            case fpcol_DIST :
                leg.distance_ = event.GetInt() * u_NM;
                break;
            case fpcol_NOTES :
                leg.notes_ = event.GetString();
                update = false;
                break;
        }
        if( update ) {
            assert(selectedAircraft);
            flightPath_.recalc(*selectedAircraft, leg_row);
            for( size_t i = 0; i <= std::min(leg_row, flightPath_.size() - 1); ++i )
                updateLeg(i, flightPath_.leg(i), dynamic_cast<wxGridBagSizer *>(GetSizer()));
            if( FlightPath::ALTINDEX == leg_row )
                updateLeg(flightPath_.size() + 1, leg, dynamic_cast<wxGridBagSizer *>(GetSizer()));
        }
    } catch( std::exception const &e ) { BOOST_LOG_TRIVIAL(error) << "EXCEPTION: " << e.what() /* << "\n" << e*/; }
}

void FlightPathPanel::changedSecALT(wxCommandEvent &event) {
    changedLegInput(event, fpcol_SEC);
    event.StopPropagation();
}

void FlightPathPanel::changedALT(wxCommandEvent &event) {
    changedLegInput(event, fpcol_ALT);
    event.StopPropagation();
}

void FlightPathPanel::changedWdir(wxCommandEvent &event) {
    changedLegInput(event, fpcol_W_DIR);
    event.StopPropagation();
}

void FlightPathPanel::changedWv(wxCommandEvent &event) {
    changedLegInput(event, fpcol_W_V);
    event.StopPropagation();
}

void FlightPathPanel::changedTC(wxCommandEvent &event) {
    changedLegInput(event, fpcol_TC);
    event.StopPropagation();
}

void FlightPathPanel::changedVAR(wxCommandEvent &event) {
    changedLegInput(event, fpcol_VAR);
    event.StopPropagation();
}

void FlightPathPanel::changedDist(wxCommandEvent &event) {
    changedLegInput(event, fpcol_DIST);
    event.StopPropagation();
}

void FlightPathPanel::changeNotes(wxCommandEvent &event) {
    changedLegInput(event, fpcol_NOTES);
    event.StopPropagation();
}