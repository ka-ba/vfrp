// BOOST_TEST_MODULE defined in trigonometry.cpp
#include "FlightPath.h"
#include <boost/test/unit_test.hpp>
#include "AircraftsViewModel.h"
#include "plain-values.h"

constexpr angle_t angle(std::integral auto deg) {
    return angle_t{fromDeg(deg)};
}
struct fp_fix {
    // example from EDFB-EDFE 10.1.23
    fp_fix() {
        craft.callSign("D-ESEL");
        craft.model("Aquila");
        craft.cruisingIAS(100 * u_kt);
        craft.cruisingConsumption(20 * u_l_per_h);
        craft.climbIAS(70 * u_kt);
        craft.climbConsumption(30 * u_l_per_h);
    }
    Leg leg1{
        "A45 A66",  false,    fromFeetMSL(2000), fromFeetMSL(3000), wind_t{angle(270), 20 * u_kt},
        angle(160), angle(2), 11 * u_NM, "Leg 1"
    };
    Leg leg2{
        "A45 A3",   false,    fromFeetMSL(2200), fromFeetMSL(2000), wind_t{angle(270), 25 * u_kt},
        angle(174), angle(2), 12 * u_NM, "Leg 2"
    };
    // treat EDFE as normal checkpoint here to avoid dependency on Field
    Leg leg3{
        "EDFE",     false,    fromFeetMSL(1900), fromFeetMSL(1300), wind_t{angle(270), 25 * u_kt},
        angle(267), angle(2), 15 * u_NM, "Leg 3"
    };
    Aircraft craft;
};
BOOST_TEST_DONT_PRINT_LOG_VALUE(true_altitude_t)
std::ostream &boost_test_print_type(std::ostream &out, true_altitude_t const &amsl) {
    out << "BUNPRINTABLE";
    return out;
}

BOOST_FIXTURE_TEST_SUITE(LegTests, fp_fix)
BOOST_AUTO_TEST_CASE(create_en_route_leg) {
    // Leg en_route{"checkpoint",false,fromFeetMSL(4000),fromFeetMSL(3000),
    //     wind_t{angle(180),15*u_kt},angle(270),angle(-3),20*u_NM};
    std::cout << leg1.altitude_ << "\n";
    BOOST_TEST("A45 A66" == leg1.checkpointName());
    BOOST_TEST(false == leg1.checkpointIsField());
    BOOST_TEST(fromFeetMSL(2000) == leg1.secureAltitude_);
    BOOST_TEST(fromFeetMSL(3000) == leg1.altitude_);
    BOOST_TEST(fromDeg(270) == deg_t{leg1.wind_.angle});
    BOOST_TEST((20 * u_kt) == leg1.wind_.speed);
    BOOST_TEST(fromDeg(160) == deg_t{leg1.tC_});
    BOOST_TEST(fromDeg(2) == deg_t{leg1.var_});
    BOOST_TEST((11 * u_NM) == leg1.distance_);
}
BOOST_AUTO_TEST_CASE(leg_calculations_without_climb_descend) {
    // reference valules corrected compared to p&p planning
    leg2.recalc(15 * u_NM, 12 * u_min, leg2.altitude_, leg2.altitude_, craft);
    BOOST_TEST("100" == leg2.tAS_);
    BOOST_TEST(fromDeg(14) == deg_t{leg2.wCA_});
    BOOST_TEST(fromDeg(186) == deg_t{leg2.mH_});
    BOOST_TEST(fromDeg(172) == deg_t{leg2.mC_});
    BOOST_TEST((27 * u_NM) == leg2.distanceSum_);
    BOOST_TEST((99 * u_kt) == leg2.gS_);
    BOOST_TEST((8 * u_min) == leg2.legDuration_);
    BOOST_TEST((20 * u_min) == leg2.durationSum_);
    BOOST_TEST((0 * u_min) == leg2.preClimb_);
    BOOST_TEST((0 * u_min) == leg2.postDescend_);
}
BOOST_AUTO_TEST_CASE(leg_calculations_with_pre_climb) {
    // reference valules corrected compared to p&p planning
    leg1.recalc(27 * u_NM, 19 * u_min, fromFeetMSL(398), leg1.altitude_, craft);
    BOOST_TEST("70/100" == leg1.tAS_);
    BOOST_TEST(fromDeg(11) == deg_t{leg1.wCA_});
    BOOST_TEST(fromDeg(169) == deg_t{leg1.mH_});
    BOOST_TEST(fromDeg(158) == deg_t{leg1.mC_});
    BOOST_TEST((38 * u_NM) == leg1.distanceSum_);
    BOOST_TEST((73 * u_kt) == leg1.gS_);
    BOOST_TEST((9 * u_min) == leg1.legDuration_);
    BOOST_TEST((28 * u_min) == leg1.durationSum_);
    BOOST_TEST((6 * u_min) == leg1.preClimb_);
    BOOST_TEST((0 * u_min) == leg1.postDescend_);
}
BOOST_AUTO_TEST_CASE(leg_calculations_with_post_descend) {
    // reference valules corrected compared to p&p planning
    leg3.recalc(0 * u_NM, 0 * u_min, fromFeetMSL(2000), fromFeetMSL(385), craft);
    BOOST_TEST("100" == leg3.tAS_);
    BOOST_TEST(fromDeg(1) == deg_t{leg3.wCA_});
    BOOST_TEST(fromDeg(266) == deg_t{leg3.mH_});
    BOOST_TEST(fromDeg(265) == deg_t{leg3.mC_});
    BOOST_TEST((15 * u_NM) == leg3.distanceSum_);
    BOOST_TEST((75 * u_kt) == leg3.gS_);
    BOOST_TEST((12 * u_min) == leg3.legDuration_);
    BOOST_TEST((12 * u_min) == leg3.durationSum_);
    BOOST_TEST((0 * u_min) == leg3.preClimb_);
    BOOST_TEST((2 * u_min) == leg3.postDescend_);
}
BOOST_AUTO_TEST_SUITE_END()  // Leg

BOOST_AUTO_TEST_SUITE(FlightPathTests)
BOOST_AUTO_TEST_SUITE_END()  // FlightPath