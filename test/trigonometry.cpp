#define BOOST_TEST_MODULE VFRP test module
#include <boost/test/unit_test.hpp>
#include "trigonometry.h"
#include "plain-values.h"

constexpr angle_t QUARTERCIRCLE{HALFCIRCLE/2.}, MQUARTERCIRCLE = angle_t{0* mp_units::si::radian}-QUARTERCIRCLE, MHALFCIRCLE = angle_t::zero()-HALFCIRCLE;
constexpr angle_t dot1{.1* mp_units::si::radian}, dot2{.2* mp_units::si::radian}, dot3{.3* mp_units::si::radian}, mdot1{-.1* mp_units::si::radian};

BOOST_AUTO_TEST_SUITE(trigonometry)
BOOST_AUTO_TEST_CASE(positiveremainder, * boost::unit_test::tolerance(1./10'000'000.)) {
    BOOST_TEST( posremain(14.,10.) == 4. );
    BOOST_TEST( posremain(-14.,10.) == 6. );
    BOOST_TEST( posremain(14.,-10.) == 4. );
    // doesn't behave nicely: BOOST_TEST( posremain(-14.,-10.) == 6. );
}
    BOOST_AUTO_TEST_CASE(deg_rad_baseclasses, * boost::unit_test::tolerance(1./10'000'000.)) {
        BOOST_TEST( std::numbers::pi == (double)radian_base_t{std::numbers::pi} );
        BOOST_TEST( 0. == (double)radian_base_t{2.*std::numbers::pi} );
        BOOST_TEST( std::numbers::pi == (double)radian_base_t{-std::numbers::pi} );
        BOOST_TEST( 180. == (double)degree_base_t{180.} );
        BOOST_TEST( 270. == (double)degree_base_t{270.} );
        BOOST_TEST( 0. == (double)degree_base_t{360.} );
        BOOST_TEST( 90. == (double)degree_base_t{450.} );
        BOOST_TEST( 180. == (double)degree_base_t{-180.} );
        BOOST_TEST( 90. == (double)degree_base_t{-270.} );
    }
BOOST_AUTO_TEST_CASE(deg_rad, * boost::unit_test::tolerance(1./100'000.)) {
        BOOST_TEST_INFO(HALFCIRCLE);
    BOOST_TEST( degree(HALFCIRCLE) == 180.*mp_units::si::degree );
    BOOST_TEST( degree(HALFCIRCLE*3.) == 180.*mp_units::si::degree );
    BOOST_TEST( degree(MHALFCIRCLE) == 180.*mp_units::si::degree );
    //BOOST_TEST( rad(180) == HALFCIRCLE );
    //BOOST_TEST( rad(540) == HALFCIRCLE );
}
BOOST_AUTO_TEST_CASE(relative_angle, * boost::unit_test::tolerance(1./100'000.)) {
    BOOST_TEST( plainRad_0_2PI(rel_angle(dot2,dot3)) == plainRad_0_2PI(dot1) );
    // fails: BOOST_TEST( rel_angle(dot2,dot3) == dot1 );
    BOOST_TEST( plainRad_0_2PI(rel_angle(dot3,dot2)) == plainRad_0_2PI(dot1) );
    BOOST_TEST( plainRad_0_2PI(rel_angle(dot2,mdot1)) == plainRad_0_2PI(dot3) );
    BOOST_TEST( rel_angle(MQUARTERCIRCLE,QUARTERCIRCLE) == HALFCIRCLE );
    BOOST_TEST( rel_angle(MQUARTERCIRCLE,QUARTERCIRCLE-dot1) == HALFCIRCLE-dot1 );
    BOOST_TEST( rel_angle(MQUARTERCIRCLE,QUARTERCIRCLE+dot1) == HALFCIRCLE-dot1 );
}
BOOST_AUTO_TEST_SUITE_END() // trigonometry
