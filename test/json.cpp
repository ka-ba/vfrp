// BOOST_TEST_MODULE defined in trigonometry.cpp
#include <boost/test/unit_test.hpp>
#include "vfrp-types-json.h"

BOOST_TEST_DONT_PRINT_LOG_VALUE(true_altitude_t)  // operator<< not found inside boost !?
BOOST_TEST_DONT_PRINT_LOG_VALUE(wind_t)
BOOST_AUTO_TEST_SUITE(json)
BOOST_AUTO_TEST_CASE(json_roundtrip_basic_types) {
    auto const kt100 = 100 * u_kt;
    boost::json::value const ktjson{
        {"speed", kt100}
    };
    BOOST_TEST(ktjson.at("speed").is_int64());
    auto const ktx = boost::json::value_to<speed_t>(ktjson.at("speed"));
    BOOST_TEST(ktx == kt100);

    auto const min11 = 11 * u_min;
    boost::json::value const minjson{
        {"min", min11}
    };
    auto const minx = boost::json::value_to<duration_t>(minjson.at("min"));
    BOOST_TEST(minx == min11);
    auto const minx_legacy = boost::json::value_to<duration_t>(minjson);
    BOOST_TEST(minx_legacy == min11);  // test medium-legacy

    auto const nm17 = 17 * u_NM;
    boost::json::value const nmjson{
        {"distance", nm17}
    };
    auto const nmx = boost::json::value_to<distance_t>(nmjson.at("distance"));
    BOOST_TEST(nmx == nm17);

    auto const l321 = 321 * u_l;
    boost::json::value const ljson{
        {"amount", l321}
    };
    auto const lx = boost::json::value_to<amount_t>(ljson.at("amount"));
    BOOST_TEST(lx == l321);

    auto const ft1412 = -1412 * r_ft_talt + mean_sea_level;
    // works: std::cout << "altitude: " << ft1412 << "\n" << std::flush;
    boost::json::value const ftjson{
        {"altitude", ft1412}
    };
    auto const ftx = boost::json::value_to<true_altitude_t>(ftjson.at("altitude"));
    BOOST_TEST(ftx == ft1412);

    auto const d120 =  // 120 * u_deg;
        angle_t{degree_base_t{120} * mp_units::si::degree};
    boost::json::value const djson{
        {"deg", d120}
    };
    auto const dmx = boost::json::value_to<angle_t>(djson.at("deg"));
    BOOST_TEST(dmx == d120, "JSON: " << djson.at("deg").as_double());
}

BOOST_AUTO_TEST_CASE(json_roundtrip_compound_types) {
    wind_t const w35925{// 359 * u_deg
                        angle_t{degree_base_t{359} * mp_units::si::degree}, 25 * u_kt};
    boost::json::value const wijson{
        {"wind", w35925}
    };
    auto const wix = boost::json::value_to<wind_t>(wijson.at("wind"));
    BOOST_TEST(wix == w35925);
}
BOOST_AUTO_TEST_SUITE_END()  // json