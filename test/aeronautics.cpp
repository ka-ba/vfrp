// BOOST_TEST_MODULE defined in trigonometry.cpp
#include "aeronautics.h"
#include <boost/test/unit_test.hpp>
#include "plain-values.h"
#include "trigonometry.h"

constexpr angle_t angle(std::integral auto deg) {
    return angle_t{fromDeg(deg)};
}

// namespace std {
// std::ostream &operator<<(std::ostream &out, pressure_altitude_t palt) {
//     out << palt.quantity_from(pressure_level_1013) << " above 1013 plane";
//     return out;
// }
// std::ostream &operator<<(std::ostream &out, density_altitude_t dalt) {
//     out << dalt.quantity_from(icao_std_atmo) << " ICAO std atmo";
//     return out;
// }
// }  // namespace std
BOOST_TEST_DONT_PRINT_LOG_VALUE(true_altitude_t)  // operator<< not found inside boost !?
// BOOST_TEST_DONT_PRINT_LOG_VALUE(pressure_altitude_t )  // operator<< not found inside boost !?
// BOOST_TEST_DONT_PRINT_LOG_VALUE(density_altitude_t )  // operator<< not found inside boost !?
BOOST_AUTO_TEST_SUITE(aeronautics)
BOOST_AUTO_TEST_CASE(calc_MC) {
    BOOST_TEST_MESSAGE("all tests executed at compile time");
    static_assert(fromDeg(-1) == degree(calcMC(angle(2), angle(3))));
    static_assert(fromDeg(5) == degree(calcMC(angle(2), angle(-3))));
    static_assert(fromDeg(-5) == degree(calcMC(angle(-2), angle(3))));
    static_assert(fromDeg(1) == degree(calcMC(angle(-2), angle(-3))));
    static_assert(fromDeg(1) == degree(calcMC(angle(4), angle(3))));
}

BOOST_AUTO_TEST_CASE(calc_WCA) {
    BOOST_TEST(fromDeg(-3) == degree(calcWCA(angle(64), {angle(340), 5 * u_kt}, 85 * u_kt)));
    BOOST_TEST(fromDeg(6) == degree(calcWCA(angle(235), {angle(0), 10 * u_kt}, 85 * u_kt)));
    BOOST_TEST(fromDeg(-1) == degree(calcWCA(angle(187), {angle(180), 10 * u_kt}, 85 * u_kt)));
    BOOST_TEST(fromDeg(11) == degree(calcWCA(angle(134), {angle(190), 20 * u_kt}, 85 * u_kt)));
}

BOOST_AUTO_TEST_CASE(calc_MH) {
    BOOST_TEST_MESSAGE("all tests executed at compile time");
    static_assert(fromDeg(338) == degree(calcMH(angle(343), angle(-5))));
    static_assert(fromDeg(96) == degree(calcMH(angle(87), angle(9))));
    static_assert(fromDeg(336) == degree(calcMH(angle(350), angle(-14))));
    static_assert(fromDeg(177) == degree(calcMH(angle(183), angle(-6))));
}

BOOST_AUTO_TEST_CASE(calc_GS) {
    BOOST_TEST((99 * u_kt) == calcGS(angle(6), angle(3), 85 * u_kt, {angle(170), 15 * u_kt}));
    BOOST_TEST((72 * u_kt) == calcGS(angle(303), angle(4), 70 * u_kt, {angle(60), 5 * u_kt}));
    BOOST_TEST((91 * u_kt) == calcGS(angle(180), angle(11), 100 * u_kt, {angle(250), 20 * u_kt}));
    BOOST_TEST((113 * u_kt) == calcGS(angle(55), angle(-8), 100 * u_kt, {angle(280), 20 * u_kt}));
}

BOOST_AUTO_TEST_CASE(calc_pressure_and_density_altitudes) {
    auto pres0 = convert_altitude(mean_sea_level + 600 * u_ft, 1020 * u_hPa);
    BOOST_TEST((pressure_altitude_t{pressure_level_1013 + 390 * u_ft}) == pres0);
    BOOST_TEST((density_altitude_t{icao_std_atmo + 1083 * u_ft}) == convert_altitude(pres0, 20 * u_cel));
}
BOOST_AUTO_TEST_SUITE_END()  // aeronautics