#pragma once
#include <memory>
#include "noname.h"
class AirfieldsViewModel;
class Airfield;

class ManageAirfieldsDialog final : public vfrp::wxfb::ManageAirfieldsDialog
{
public:
    explicit ManageAirfieldsDialog(wxWindow *parent,AirfieldsViewModel&);
    ~ManageAirfieldsDialog() noexcept override =default;
    void save();
    [[nodiscard]] static wxObjectDataPtr<AirfieldsViewModel> airfielsdViewModel();
protected:
    void icaoChanged(wxCommandEvent &event) final;
    void nameChanged( wxCommandEvent& event ) final;
    void elevChanged(wxSpinEvent &event) final;
    void rwyChanged(wxSpinEvent &event) final;
    void lengthChanged(wxSpinEvent &event) final;
    void surfaceChanged(wxCommandEvent &event) final;
    void toraChanged(wxSpinEvent &event) final;
    void ldaChanged(wxSpinEvent &event) final;
    void revToraChanged(wxSpinEvent &event) final;
    void revLdaChanged(wxSpinEvent &event) final;
    void radioToggeled(wxCommandEvent &event) final;
    void radioNameChanged(wxCommandEvent &event) final;
    void radioChanged(wxSpinEvent &event) final;
    void approachChanged(wxSpinEvent &event) final;
    void towerChanged(wxSpinEvent &event) final;
    void groundChanged(wxSpinEvent &event) final;
    void fisChanged(wxSpinEvent &event) final;
    void airfieldSelected(wxDataViewEvent &event) final;
private:
    void textCtrlChanged(wxString,unsigned int model_col);
    void numberCtrlChanged(long,unsigned int model_col);
    void updateDetails(std::shared_ptr<Airfield const>);
    void xableRadioCtrls(bool has_atc);
    wxDataViewItem selectedItem;
};
