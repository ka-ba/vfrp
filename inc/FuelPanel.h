#pragma once
#include <wx/panel.h>
#include <wx/range.h>
#include "FuelModel.h"
#include "vfrp-types.h"

class wxStaticText;
class wxSpinCtrl;
class wxGridBagSizer;
class wxGBPosition;
class wxGBSpan;
class Aircraft;
class FlightPath;

class FuelPanel : public wxPanel
{
public:
    explicit FuelPanel(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            wxPoint const &pos = wxDefaultPosition,
            wxSize const &size = wxDefaultSize,
            long style = wxTAB_TRAVERSAL | wxNO_BORDER,
            wxString const &name = wxPanelNameStr);
    [[nodiscard]] duration_t update(FlightPath const&, Aircraft const&);
    [[nodiscard]] boost::json::object jsoniseFuel() const;
    [[nodiscard]] duration_t fromJson(boost::json::value const&, FlightPath const&, Aircraft const&);
private:
    friend class FuelModel;
    void initGUI();
    void cruiseDisplay(duration_t, amount_t);
    void taxiAddDisplay(amount_t);
    void climbAddDisplay(duration_t, amount_t);
    void circuitAddDisplay(duration_t, amount_t);
    void alternateDisplay(duration_t, amount_t);
    void reserveDisplay(duration_t, amount_t);
    void minimalDisplay(amount_t);
    void extraDisplay(duration_t, amount_t);
    void stockDisplay(duration_t, amount_t);
    void safeDisplay(duration_t);
    void consumptionLabel(consumption_t,consumption_t);
    void changedTaxiAddFuel( wxCommandEvent& event );
    void changedCircuitAddDur( wxCommandEvent& event );
    void changedReserveDur( wxCommandEvent& event );
    void changedExtraDur( wxCommandEvent& event );
    void changedExtraFuel( wxCommandEvent& event );
    wxStaticText *addLabel(wxGridBagSizer*,wxString const &text,wxGBPosition const &pos,wxGBSpan const &span,/*int sizer_flags=0,*/int text_flags=0);
    wxSpinCtrl *addSpin(wxGridBagSizer*, wxWindowID, wxRange const, int init, wxObjectEventFunction, wxString const &example, wxGBPosition const &);
    wxStaticText *cruiseDurationLabel{}, *cruiseFuelLabel{}, *climbAddDurLabel{}, *climbAddFuelLabel{}, *circuitAddFuelLabel{},
    *alternateDurLabel{}, *alternateFuelLabel{}, *reserveFuelLabel{}, *minimalFuelLabel{}, *stockDurLabel{}, *stockFuelLabel{}, *safeDurLabel{}, *consumptionLabel_{};
    wxSpinCtrl *taxiAddFuelSpin{}, *circuitAddDurSpin{}, *reserveSpin{}, *extraDurSpin{}, *extraFuelSpin{};
    FuelModel fuelModel_;
};
