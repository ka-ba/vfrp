#pragma once
#include <chrono>
#include <filesystem>
#include <string>
#include "vfrp-types.h"
namespace boost::json {
class value;
}

[[nodiscard]] std::string hhmm(duration_t);
[[nodiscard]] std::string hhmm(std::chrono::minutes);
[[nodiscard]] std::string dmy(std::chrono::sys_days);

[[nodiscard]] std::string nice_str(double);
[[nodiscard]] std::string flText(pressure_altitude_t const altitude);

[[nodiscard]] boost::json::value load_json(std::filesystem::path const &);