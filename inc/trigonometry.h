#pragma once
#include <cmath>
#include <cstdint>
#include <type_traits>
#include "vfrp-types.h"

inline namespace constants {
    constexpr angle_t HALFCIRCLE{std::numbers::pi * mp_units::si::radian};
}

constexpr deg_t degree(angle_t radi) {
    return deg_t{radi};
}

angle_t invert(angle_t);
angle_t rel_angle(angle_t lhs,angle_t rhs);

wind_t invert(wind_t);
wind_t vector_sum(wind_t lhs, wind_t rhs);

template<typename FP,typename std::enable_if_t<std::is_floating_point_v<FP>,bool> =true>
FP posremain(FP x, FP divider) {
    FP r = std::remainder(x,divider);
    return r>=FP{0}
    ? r
    : divider+r;
}
