#pragma once
// #include <wx/object.h>
#include "noname.h"
class FlightContext;

class FlightContextDialog final : public vfrp::wxfb::FlightContextDialog {
  public:
    explicit FlightContextDialog(wxWindow *parent, FlightContext const &);
    ~FlightContextDialog() noexcept override /* =default*/;
    //    void newAircraft( wxCommandEvent& event ) final;
    FlightContext get() const;
  protected:
    void toggleDetail(wxCommandEvent &) final;
  private:
    void showHideDetail(bool show);
};
