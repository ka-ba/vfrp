#pragma once
#include <wx/spinctrl.h>
#include <wx/window.h>

void sizeByExample(wxSpinCtrl&,wxString const&);

template<class T>
T *ancestorWindow(wxWindow *window) {
    while(window) {
        if(auto typed = dynamic_cast<T*>(window))
            return typed;
        window = window->GetParent();
    }
    return nullptr;
}
