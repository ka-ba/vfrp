#pragma once
#include <functional>
#include <map>
#include <memory>
#include <wx/defs.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/range.h>
#include "FlightPath.h"
class wxStaticText;
class TopFrame;
class Aircraft;
class Airfield;

class FlightPathPanel : public wxPanel {
  public:
    explicit FlightPathPanel(wxWindow *parent, wxWindowID winid = wxID_ANY, wxPoint const &pos = wxDefaultPosition,
                             wxSize const &size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER,
                             wxString const &name = wxPanelNameStr);
    void init(std::shared_ptr<Airfield const> origin, std::shared_ptr<Airfield const> destination, Aircraft const &);
    void changeOrigin(std::shared_ptr<Airfield>);
    std::shared_ptr<Airfield const> origin() const;
    void changeDestination(std::shared_ptr<Airfield>);
    std::shared_ptr<Airfield const> destination() const;
    void changeAlternate(std::shared_ptr<Airfield>);
    std::shared_ptr<Airfield const> alternate() const;
    void changeAircraft(Aircraft const &);
    void updateSafeDuration(duration_t);
    void loadFlightpath(wxString const &path);
    void fromJson(boost::json::value const &, Aircraft const &);
    //    void storeFlightpath(wxString const &path);
    boost::json::object jsoniseFlightpath() const;
    FlightPath const &path() const noexcept;
  private:
    void initGUI();
    void addLabel(wxGridBagSizer *, wxString const &text, int wrap, int col);
    wxStaticText *addText(wxGridBagSizer *, wxString const &text, int wrap, wxGBPosition const &pos,
                          int sizer_flags = 0, int rowspan = 4, int text_flags = 0);
    void addSpin(wxGridBagSizer *, wxWindowID, wxRange const, int init, wxObjectEventFunction, wxString const &example,
                 wxGBPosition const &);
    void layoutFlightpath();
    [[nodiscard]] wxWindowID wxwid4Index(size_t index);
    [[nodiscard]] int legRow(size_t index);
    void layoutLeg(size_t index, bool last, bool alternate, Leg const &leg, wxGridBagSizer *gb_sizer);
    void updateLeg(size_t index, Leg const &leg, wxGridBagSizer *gb_sizer);
    void insertLeg(wxCommandEvent &event);
    void deleteLeg(wxCommandEvent &event);
    void changeCheckpoint(wxCommandEvent &event);
    void changedLegInput(wxCommandEvent &event,
                         int input);  // FIXME: dispose of int (maybe in favour of class around scoped enum?)
    void changedSecALT(wxCommandEvent &event);
    void changedALT(wxCommandEvent &event);
    void changedWdir(wxCommandEvent &event);
    void changedWv(wxCommandEvent &event);
    void changedTC(wxCommandEvent &event);
    void changedVAR(wxCommandEvent &event);
    void changedDist(wxCommandEvent &event);
    void changeNotes(wxCommandEvent &event);
    TopFrame *topParent;
    wxStaticText *safeDurLabel{};
    FlightPath flightPath_;
    std::map<wxWindowID, size_t> buttonIndex;
    Aircraft const *selectedAircraft{};
};