#pragma once
#include <wx/object.h>
#include "noname.h"
class AircraftsViewModel;

class ManageAircraftsDialog final : public vfrp::wxfb::ManageAircraftsDialog
{
public:
    explicit ManageAircraftsDialog(wxWindow *parent, AircraftsViewModel&);
    ~ManageAircraftsDialog() noexcept override/* =default*/;
    void newAircraft( wxCommandEvent& event ) final;
    void save();
    [[nodiscard]] static wxObjectDataPtr<AircraftsViewModel> aircraftsViewModel();
};
