#pragma once
#include <stdexcept>
#include <wx/dialog.h>

/** dialog that can throw out of showModal if necessary */
class ThrowingDialog : public wxDialog
{
public:
    explicit ThrowingDialog(wxWindow *parent, wxWindowID id, wxString const &title, wxPoint const &pos = wxDefaultPosition,
                            wxSize const &size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, wxString const &name = wxDialogNameStr );
    virtual ~ThrowingDialog() =default;
    int ShowModal() override;
protected:
    /** set a non-nullptr error msg (use text literals!) and the caller of ShowModal will receive an exception with this text */
    void throwOverFence(char const*);
private:
    char const *errorMsg{};
};

class DialogException : public std::exception
{
public:
    explicit DialogException(char const*) noexcept;
    char const *what() const noexcept override;
private:
    char const *message;
};
