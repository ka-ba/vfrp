#pragma once

#include <deque>
#include <memory>
#include <optional>
#include <set>
#include <variant>
#include <wx/config.h>
#include <wx/dataview.h>
#include <wx/regex.h>
#include "vfrp-types.h"

struct InfoRadio {
    enum class Callsign { Radio, Information, Info, Illegal };
    void save(wxConfigBase &config) const;
    static std::optional<InfoRadio> read(wxConfigBase const &config, wxString const &entry_id);
    static wxString infoCallsign(Callsign);
    static Callsign infoCallsign(wxString const &);
    Callsign callsign_{};
    freq_t frequency_{};
};

struct AtcRadio {
    void save(wxConfigBase &config) const;
    static std::optional<AtcRadio> read(wxConfigBase const &config, wxString const &entry_id);
    freq_t approach_{}, tower_{}, ground_{};
};

class Airfield {
  public:
    enum class RwySurface { Gras, Asph, Conc, Illegal };
    using Radio = std::variant<InfoRadio, AtcRadio>;
    static wxString rwySurface(RwySurface);
    static RwySurface rwySurface(wxString const &);
    Airfield(wxConfigBase const &, wxString const &entry_id);
    /** construct dummy entry with only ICAO code */
    Airfield(wxString const &icao_code);
    Airfield &operator=(Airfield &&) = delete;  // no walzing matilda
    void save(wxConfigBase &) const;
    [[nodiscard]] wxString const &name() const noexcept;
    [[nodiscard]] wxString const &icaoCode() const noexcept;
    [[nodiscard]] uint8_t rwyCode() const noexcept;
    [[nodiscard]] wxString rwyName() const;
    [[nodiscard]] wxString rwyName(bool reverse) const;
    [[nodiscard]] elevation_t elevation() const noexcept;
    [[nodiscard]] length_t rwyLength() const noexcept;
    [[nodiscard]] length_t tora(bool reverse) const noexcept;
    [[nodiscard]] length_t lda(bool reverse) const noexcept;
    [[nodiscard]] RwySurface surface() const noexcept;
    [[nodiscard]] wxString surfaceName() const;
    [[nodiscard]] bool hasATC() const;
    [[nodiscard]] AtcRadio &atcRadio();
    [[nodiscard]] AtcRadio const &atcRadio() const;
    [[nodiscard]] InfoRadio &infoRadio();
    [[nodiscard]] InfoRadio const &infoRadio() const;
    [[nodiscard]] freq_t fisFreq() const noexcept;
    void name(wxString const &);
    void icaoCode(wxString const &);
    void rwyCode(uint8_t) noexcept;
    void elevation(elevation_t) noexcept;
    void rwyLength(length_t) noexcept;
    void tora(length_t, bool reverse) noexcept;
    void lda(length_t, bool reverse) noexcept;
    void surface(RwySurface) noexcept;
    void surfaceName(wxString);
    void radio(Radio const &) noexcept;
    void fisFreq(freq_t) noexcept;
  private:
    friend class AirfieldsViewModel;
    static uint8_t fromRwySurface(RwySurface);
    static RwySurface toRwySurface(uint8_t);
    void improveEntryId(std::set<wxString> &ids);
    Radio readRadio(wxConfigBase const &, wxString const &entry_id);
    wxString entryID_;
    wxString name_;
    wxString icaoCode_;
    uint8_t rwyCode_{};
    elevation_t elevation_{};
    length_t rwyLength_{};
    length_t tora_{}, reverseTora_{};
    length_t lda_{}, reverseLda_{};
    RwySurface surface_;
    Radio radio_;
    freq_t fis_{};
    static wxRegEx const regEx;
};

class AirfieldsViewModel final : public wxDataViewModel {
  public:
    enum Columns : unsigned int {
        name = 0,
        icao,
        rwyName,
        elev,
        rwyLength,
        surfaceName,
        combinedName,
        rwy,
        tora,
        revTora,
        lda,
        revLda,
        radioType,
        radioName,
        radioFreq,
        apprFreq,
        towerFreq,
        gndFreq,
        fisFreq,
        colCount
    };                                    // deliberately non-scoped
    enum RadioType : int { Radio, Atc };  // deliberately non-scoped
    /** the first call must go here with valid config! */
    [[nodiscard]] static AirfieldsViewModel &instance(wxConfigBase const &config);  // singleton
    /** must not be called before instance() was called! */
    [[nodiscard]] static AirfieldsViewModel const &instanceConst();
    AirfieldsViewModel &operator=(AirfieldsViewModel &&) = delete;  // desdemova
    [[nodiscard]] wxDataViewItem item(wxString const &) const;
    [[nodiscard]] unsigned int GetColumnCount() const final;
    [[nodiscard]] wxString GetColumnType(unsigned int col) const final;
    void GetValue(wxVariant &variant, wxDataViewItem const &item, unsigned int col) const final;
    [[nodiscard]] bool SetValue(wxVariant const &variant, wxDataViewItem const &item, unsigned int col) final;
    [[nodiscard]] wxDataViewItem GetParent(wxDataViewItem const &item) const final;
    [[nodiscard]] bool IsContainer(wxDataViewItem const &item) const final;
    [[nodiscard]] unsigned int GetChildren(wxDataViewItem const &item, wxDataViewItemArray &children) const final;
    [[nodiscard]] static std::deque<std::shared_ptr<Airfield>> loadAirfields(wxConfigBase const &config);
    void save(wxConfigBase &config);
    [[nodiscard]] std::shared_ptr<Airfield const> verifyAirfield(wxDataViewItem const &item) const;
    [[nodiscard]] std::shared_ptr<Airfield const> airfieldOrDummy(wxString const &icao_code) const;
  private:
    explicit AirfieldsViewModel(wxConfigBase const &config);
    [[nodiscard]] std::shared_ptr<Airfield> verifyAirfield(wxDataViewItem const &item);
    [[nodiscard]] std::shared_ptr<Airfield> verifyAirfieldImpl(wxDataViewItem const &item) const;
    // void addAircraft(std::unique_ptr<Aircraft> &&aircraft);
    std::deque<std::shared_ptr<Airfield>> fields;  // derive a vector that doesn't grow?
    bool hasChanges{};
};
