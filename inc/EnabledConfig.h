#pragma once
#include <optional>
#include <wx/config.h>
#include <wx/gdicmn.h>
constexpr char const *CONFKEY_AIRCRAFTDATA = "data/aircraft";
constexpr char const *CONFKEY_AIRFIELDSDATA = "data/airfields";

class PathMarker
{
    wxConfigBase &config_;
    wxString markedPath_;
public:
    explicit PathMarker(wxConfigBase &config,wxString const &new_path);
    PathMarker(PathMarker const&) =delete; // don't copy
    PathMarker &operator=(PathMarker const&) =delete; // don't copy
    // TODO: move might be allowable...
    ~PathMarker();
};

class EnabledConfig
{
public:
    explicit EnabledConfig(wxConfigBase &config);
    [[nodiscard]] wxConfigBase &configBase();
    [[nodiscard]] wxConfigBase const &configBase() const;
    [[nodiscard]] wxPoint point(wxString const &key,wxPoint preset) const;
    [[nodiscard]] std::optional<wxPoint> point(wxString const &key) const;
    void storePoint(wxString const &key,wxPoint point);
    [[nodiscard]] wxSize size(wxString const &key,wxSize preset) const;
    [[nodiscard]] std::optional<wxSize> size(wxString const &key) const;
    void storeSize(wxString const &key,wxSize size);
    [[nodiscard]] PathMarker setPath(wxString const &new_path);
    static void deleteAllGroups(wxConfigBase &config);
    static double Read(wxConfigBase const &config,wxString const &key,double def_val);
    private:
    wxConfigBase &config_; // do not own it
};
