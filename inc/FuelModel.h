#pragma once
#include "vfrp-types.h"
namespace boost::json {
class object;
class value;
}
class Aircraft;
class FuelModel;
class FuelPanel;
class DurationAmount
{
public:
    [[nodiscard]] duration_t duration() const noexcept;
    [[nodiscard]] amount_t amount() const noexcept;
private:
    friend class FuelModel;
    DurationAmount(duration_t, consumption_t);
    DurationAmount(amount_t,consumption_t);
    duration_t duration_;
    amount_t amount_{};
};

class FuelModel
{
public:
    FuelModel() noexcept;
    FuelModel(boost::json::value const&, Aircraft const&, FuelPanel&);
    void updateConsumption(Aircraft const&, FuelPanel&);
    void cruiseDuration(duration_t, FuelPanel&);
    void taxiAddAmount(amount_t, FuelPanel&);
    void climbAddDuration(duration_t, FuelPanel&);
    void circuitAddDuration(duration_t, FuelPanel&);
    void alternateDuration(duration_t, FuelPanel&);
    void reserveDuration(duration_t, FuelPanel&);
    void extraDuration(duration_t, FuelPanel&);
    void extraAmount(amount_t, FuelPanel&);
    [[nodiscard]] duration_t safe() const noexcept;
    [[nodiscard]] boost::json::object jsonise() const;
private:
    void addUp(FuelPanel&);
    void consumption(consumption_t,consumption_t, FuelPanel&);
    consumption_t cruiseConsumption{20*u_l_per_h}, climbConsumption{30*u_l_per_h};
    DurationAmount cruise_, climbAdd_, circuitAdd_, alternate_, reserve_, extra_, stock_;
    amount_t taxiAdd_{1*u_l}, minimal_{};
    duration_t safe_{};
};
