#pragma once
#include <filesystem>
#include <memory>
#include <noname.h>
#include <optional>
#include <wx/object.h>
#include "DofInfo.h"
class AircraftsViewModel;
class Aircraft;
class AirfieldsViewModel;
class EnabledConfig;
class FlightPath;
class ManageAircraftsDialog;
class ManageAirfieldsDialog;

class TopFrame final : public vfrp::wxfb::MainFrame {
  public:
    explicit TopFrame(wxWindow *parent = nullptr);
    ~TopFrame() noexcept override;
    void flightPathUpdated(FlightPath const &);
  protected:
    void onMove(wxMoveEvent &) final;
    void onSize(wxSizeEvent &) final;
    void editContext(wxCommandEvent &) final;
    //    void dateChanged( wxDateEvent&) final;
    //    void timeChanged( wxDateEvent&) final;
    void addAircraft(wxCommandEvent &) final;
    void manageAirfields(wxCommandEvent &) final;
    void loadFlight(wxCommandEvent &) final;
    void storeFlight(wxCommandEvent &) final;
    void storeFlightAs(wxCommandEvent &) final;
    void originSelected(wxDataViewEvent &) final;
    void destinationSelected(wxDataViewEvent &) final;
    void alternateSelected(wxDataViewEvent &) final;
    void aircraftSelected(wxDataViewEvent &) final;
  private:
    void logBasicInformation() const;
    void dofInfo(FlightContext);
    void elevationDisplay();
    void selectAircraft(wxDataViewItem const &);
    void selectAircraft(wxString const &callsign);
    void selectFieldCtrl(wxDataViewCtrl *, wxString const &);
    void setCurrentPathFlightpath(std::optional<wxString> const &path, wxString const &filename);
    void loadFlight(std::filesystem::path const &);
    void storeFlight(std::filesystem::path const &);
    wxColour dofButtonFgCol, sunsetButtonFgCol;
    std::optional<wxString> currentPathFlightpath;
    wxObjectDataPtr<AircraftsViewModel> aircraftViewModel;
    wxObjectDataPtr<AirfieldsViewModel> airfieldsViewModel;
    Aircraft const *selectedAircraft{};
    FlightContext dofInfo_;
};