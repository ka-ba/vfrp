#pragma once

#include <deque>
#include <set>
#include <wx/config.h>
#include <wx/dataview.h>
#include <wx/regex.h>
#include "vfrp-types.h"

class AircraftsViewModel;
class Aircraft {
public:
    Aircraft(wxConfigBase const &config,wxString const &entry_id);
    Aircraft();
    [[nodiscard]] wxString const &callSign() const noexcept;
    [[nodiscard]] wxString const &model() const noexcept;
    [[nodiscard]] speed_t climbIAS() const noexcept;
    [[nodiscard]] speed_t cruisingIAS() const noexcept;
    [[nodiscard]] consumption_t climbConsumption() const noexcept;
    [[nodiscard]] consumption_t cruisingConsumption() const noexcept;
    void callSign(wxString const &call_sign);
    void model(wxString const &m_odel);
    void climbIAS(speed_t climb_ias) noexcept;
    void cruisingIAS(speed_t cruising_ias) noexcept;
    void climbConsumption(consumption_t climb_consumption) noexcept;
    void cruisingConsumption(consumption_t cruising_consumption) noexcept;
    void save(wxConfigBase &config) const;
private:
    friend AircraftsViewModel;
    void improveEntryId(std::set<wxString> &ids);
    wxString entryID_;
    wxString callSign_;
    wxString model_;
    // TODO: do we need more speed/consumption profiles?
    speed_t climbIAS_, cruisingIAS_;
    consumption_t climbConsumption_, cruisingConsumption_;
    // enumtype speedUnit
    static wxRegEx const regEx;
};

class AircraftsViewModel final : public wxDataViewModel
{
public:
    explicit AircraftsViewModel(wxConfigBase const &config);
    AircraftsViewModel &operator=(AircraftsViewModel&&) =delete; // desdemova
    [[nodiscard]] unsigned int GetColumnCount() const final {return 2;}
    [[nodiscard]] wxString GetColumnType(unsigned int) const final {return wxVariant{"bla"}.GetType();}
    void GetValue(wxVariant &variant, wxDataViewItem const &item, unsigned int col) const final;
    bool SetValue(wxVariant const &variant, wxDataViewItem const &item, unsigned int col) final;
    [[nodiscard]] wxDataViewItem GetParent(wxDataViewItem const &) const final {return wxDataViewItem{};}
    [[nodiscard]] bool IsContainer(const wxDataViewItem &item) const final;
    [[nodiscard]] unsigned int GetChildren(wxDataViewItem const &item, wxDataViewItemArray &children) const final;
    void newAircraft();
    [[nodiscard]] wxDataViewItem firstItem() const;
    [[nodiscard]] wxDataViewItem item(wxString const &callsign) const;
    void save(wxConfigBase &config);
    [[nodiscard]] Aircraft const &verifyAircraft(wxDataViewItem const &item) const;
private:
    [[nodiscard]] Aircraft &verifyAircraft(wxDataViewItem const &item);
    [[nodiscard]] Aircraft &verifyAircraftImpl(wxDataViewItem const &item) const;
    std::deque<Aircraft> crafts;
    bool hasChanges{};
};
