#pragma once
#include <chrono>
#include <boost/json/value_from.hpp>
#include <boost/json/value_to.hpp>

namespace boost::json
{
    void tag_invoke(value_from_tag const&, value &value, std::chrono::sys_days const &wind);
    std::chrono::sys_days tag_invoke(value_to_tag<std::chrono::sys_days> const&, value const &value);

    void tag_invoke(value_from_tag const&, value &value, std::chrono::minutes const &wind);
    std::chrono::minutes tag_invoke(value_to_tag<std::chrono::minutes> const&, value const &value);
}