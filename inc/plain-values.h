#pragma once
#include <iomanip>
#include "vfrp-types.h"

[[nodiscard]] constexpr speed_t::rep plainKnots(mp_units::QuantityOf<mp_units::isq::speed> auto const speed) {
    return speed.numerical_value_in(u_kt);
}
template<typename R> [[nodiscard]] constexpr R plainKnots(mp_units::QuantityOf<mp_units::isq::speed> auto const speed) {
    return static_cast<R>(plainKnots(speed));
}
[[nodiscard]] constexpr speed_t fromKnots(std::integral auto const kt) {
    return static_cast<speed_t::rep>(kt) * u_kt;
}

[[nodiscard]] constexpr duration_t::rep plainMins(mp_units::QuantityOf<mp_units::isq::time> auto const time) {
    return time.numerical_value_in(u_min);
}
[[nodiscard]] constexpr duration_t fromMins(std::integral auto const mins) {
    return static_cast<duration_t::rep>(mins) * u_min;
}

[[nodiscard]] constexpr distance_t::rep plainNM(mp_units::QuantityOf<mp_units::isq::distance> auto const distance) {
    return distance.numerical_value_in(u_NM);
}
[[nodiscard]] constexpr distance_t fromNM(std::integral auto const nm) {
    return static_cast<distance_t::rep>(nm) * u_NM;
}

[[nodiscard]] constexpr length_t::rep plainMetre(mp_units::QuantityOf<mp_units::isq::length> auto const length) {
    return length.numerical_value_in(u_m);
}
template<typename R>
[[nodiscard]] constexpr R plainMetre(mp_units::QuantityOf<mp_units::isq::length> auto const length) {
    return static_cast<R>(plainMetre(length));
}
[[nodiscard]] constexpr length_t fromMetre(std::integral auto const metre) {
    return static_cast<length_t::rep>(metre) * u_m;
}

[[nodiscard]] constexpr amount_t::rep plainLitre(mp_units::QuantityOf<mp_units::isq::volume> auto const amount) {
    return amount.numerical_value_in(u_l);
}
[[nodiscard]] constexpr amount_t fromLitre(std::integral auto const litre) {
    return static_cast<amount_t::rep>(litre) * u_l;
}

[[nodiscard]] constexpr consumption_t::rep plainLZh(mp_units::QuantityOf<consumption_spec> auto const consumption) {
    return consumption.numerical_value_in(u_l_per_h);
}
[[nodiscard]] constexpr consumption_t fromlZh(std::floating_point auto const l_per_h) {
    return static_cast<consumption_t::rep>(l_per_h) * u_l_per_h;
}

[[nodiscard]] constexpr true_altitude_t::rep plainFeetMSL(true_altitude_t const altitude) {
    return altitude.quantity_from(mean_sea_level).numerical_value_in(mp_units::international::foot);
}
template<typename R> [[nodiscard]] constexpr R plainFeetMSL(true_altitude_t const altitude) {
    return static_cast<R>(plainFeetMSL(altitude));
}
[[nodiscard]] constexpr true_altitude_t fromFeetMSL(std::integral auto const feet_msl) {
    return mean_sea_level + static_cast<true_altitude_t::rep>(feet_msl) * r_ft_talt;
}
[[nodiscard]] constexpr pressure_altitude_t ::rep plainFeet1013(pressure_altitude_t const paltitude) {
    return paltitude.quantity_from(pressure_level_1013).numerical_value_in(mp_units::international::foot);
}
[[nodiscard]] constexpr pressure_altitude_t ::rep plainFL(pressure_altitude_t const altitude) {
    constexpr mp_units::Unit auto hft = mp_units::si::hecto<mp_units::international::foot>;
    return mp_units::round<hft>(altitude.quantity_from(pressure_level_1013)).numerical_value_in(hft);
}
[[nodiscard]] constexpr density_altitude_t::rep plainFeetICAO(density_altitude_t const daltitude) {
    return daltitude.quantity_from(icao_std_atmo).numerical_value_in(mp_units::international::foot);
}

[[nodiscard]] constexpr angle_t fromRad(std::floating_point auto const rad) {
    return angle_t{static_cast<angle_t::rep::value_type>(rad) * u_rad};
}

[[nodiscard]] constexpr uint16_t plainDeg_0_359(mp_units::QuantityOf<mp_units::isq::angular_measure> auto const angle) {
    return (mp_units::round<u_deg>(deg_t{angle})).numerical_value_in(u_deg).value;
}
[[nodiscard]] constexpr deg_t fromDeg(std::integral auto const deg) {
    return deg_t{static_cast<deg_t::rep::value_type>(deg) * u_deg};
}

[[nodiscard]] constexpr double plainRad_0_2PI(mp_units::QuantityOf<mp_units::isq::angular_measure> auto const angle) {
    return (angle_t{angle}).numerical_value_in(mp_units::si::radian).value;
}

[[nodiscard]] constexpr freq_t::rep::value_type
plainKHz(mp_units::QuantityOf<mp_units::isq::frequency> auto const freq) {
    return freq.numerical_value_in(u_kHz).freq();
}
[[nodiscard]] constexpr freq_t fromKHz(std::integral auto const khz) {
    return freq_t{static_cast<freq_t::rep::value_type>(khz) * u_kHz};
}
template<typename R>
[[nodiscard]] constexpr R plainKHz(mp_units::QuantityOf<mp_units::isq::frequency> auto const freq) {
    return static_cast<R>(plainKHz(freq));
}
static_assert(plainKHz(freq_t::min()) == radio_kHz_base_t::MIN);
static_assert(plainKHz(freq_t::max()) == radio_kHz_base_t::MAX);

[[nodiscard]] constexpr pressure_t::rep plainHPa(mp_units::QuantityOf<mp_units::isq::pressure> auto const pres) {
    return pres.numerical_value_in(u_hPa);
}
[[nodiscard]] constexpr pressure_t fromHPa(std::integral auto const hpa) {
    return pressure_t{static_cast<pressure_t::rep>(hpa) * u_hPa};
}

[[nodiscard]] constexpr temperature_t::rep
plainCelsius(mp_units::QuantityOf<mp_units::isq::thermodynamic_temperature> auto const temp) {
    return temp.numerical_value_in(u_cel);
}
[[nodiscard]] constexpr temperature_t fromCelsius(std::integral auto const cel) {
    return temperature_t{static_cast<temperature_t::rep>(cel) * u_cel};
}