#pragma once
#include <chrono>
#include <optional>
#include <boost/json/value_from.hpp>
#include <boost/json/value_to.hpp>
#include "vfrp-types.h"

/** relevant atmospheric values at an airfield at the point in time in question */
struct FieldAtmosphere {
    pressure_t qnh_{};
    temperature_t temperature_{};
};

class FlightContext {
  public:
    FlightContext() = default;
    //  FlightContext(std::chrono::sys_days,std::chrono::minutes);
    /** \param dof time part is ignored
     * @param sunset date part is ignored
     */
    FlightContext(std::chrono::system_clock::time_point dof,
                  std::optional<std::chrono::system_clock::time_point> sun_set);
    FlightContext(std::chrono::system_clock::time_point dof, std::optional<std::chrono::system_clock::time_point> e_obt,
                  std::optional<std::chrono::system_clock::time_point> sun_set, std::optional<FieldAtmosphere> o_rigin,
                  std::optional<FieldAtmosphere> dest);
    FlightContext(std::chrono::sys_days dof, std::optional<std::chrono::minutes> e_obt,
                  std::optional<std::chrono::minutes> sun_set, std::optional<FieldAtmosphere> o_rigin,
                  std::optional<FieldAtmosphere> dest);
    void dateOfFlight(std::chrono::system_clock::time_point);
    [[nodiscard]] std::chrono::system_clock::time_point dateOfFlight() const;
    [[nodiscard]] std::chrono::year_month_day dateOfFlight_ymd() const;
    [[nodiscard]] std::chrono::sys_days dateOfFlight_days() const;
    [[nodiscard]] bool past() const;
    void sunset(std::optional<std::chrono::system_clock::time_point>);
    [[nodiscard]] std::optional<std::chrono::system_clock::time_point> sunset() const;
    //    [[nodiscard]] std::chrono::hh_mm_ss<std::chrono::minutes> sunset_hms() const;
    [[nodiscard]] std::optional<std::chrono::minutes> sunset_min() const;
    void eobt(std::optional<std::chrono::system_clock::time_point>);
    [[nodiscard]] std::optional<std::chrono::system_clock::time_point> eobt() const;
    [[nodiscard]] std::optional<std::chrono::minutes> eobt_min() const;
    void origin(std::optional<FieldAtmosphere>);
    [[nodiscard]] std::optional<FieldAtmosphere> origin() const;
    void destination(std::optional<FieldAtmosphere>);
    [[nodiscard]] std::optional<FieldAtmosphere> destination() const;
    /** is sunset unset */
    //    [[nodiscard]] bool unset() const;
  private:
    std::chrono::sys_days dateOfFlight_{std::chrono::May / 23 / 1848};
    /** expected off-block time as durations since midnight */
    std::optional<std::chrono::minutes> eobt_{};
    /** time of sunset (on the DOF at destination) as durations since midnight */
    std::optional<std::chrono::minutes> sunset_{};
    std::optional<FieldAtmosphere> origin_{}, destination_{};
};

[[nodiscard]] std::optional<std::chrono::hh_mm_ss<std::chrono::minutes>>
    hours_mins(std::optional<std::chrono::minutes>);
[[nodiscard]] std::chrono::minutes xMinutes(std::chrono::system_clock::time_point);
[[nodiscard]] std::optional<std::chrono::minutes> xMinutes(std::optional<std::chrono::system_clock::time_point>);
[[nodiscard]] std::pair<true_altitude_t, pressure_altitude_t> lowestVfrFl(FieldAtmosphere);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &, FieldAtmosphere const &);
FieldAtmosphere tag_invoke(boost::json::value_to_tag<FieldAtmosphere> const &, boost::json::value const &);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &, FlightContext const &);
FlightContext tag_invoke(boost::json::value_to_tag<FlightContext> const &, boost::json::value const &);