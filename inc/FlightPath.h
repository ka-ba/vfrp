#pragma once
#include <filesystem>
#include <memory>
#include <numeric>
#include <optional>
#include <string>
#include <variant>
#include <vector>
#include "vfrp-types.h"
namespace boost::json {
class object;
class value;
}
class Aircraft;
class Airfield;
class AirfieldsViewModel;
using AIRFIELD_SHARED = std::shared_ptr<Airfield const>;
using CP_OR_FIELD     = std::variant<std::string, AIRFIELD_SHARED>;

struct Leg {
    explicit Leg(CP_OR_FIELD checkpoint = "[EMPTY]");
    Leg(std::string checkpoint, bool checkpoint_is_field, true_altitude_t secure, true_altitude_t altitude, wind_t,
        angle_t tc, angle_t var, distance_t,std::string notes);
    [[nodiscard]] std::string checkpointName() const noexcept;
    [[nodiscard]] bool checkpointIsField() const noexcept;
    [[nodiscard]] AIRFIELD_SHARED checkpointField() const noexcept;
    void recalc(distance_t, duration_t, true_altitude_t, true_altitude_t, Aircraft const &);
    static wind_t gVector(angle_t mc, speed_t tas, wind_t wind);
    CP_OR_FIELD checkpoint_;
    true_altitude_t secureAltitude_{mean_sea_level + 1 * r_ft_talt}, altitude_{mean_sea_level + 1 * r_ft_talt};
    speed_t gS_{};
    std::string tAS_,notes_;
    wind_t wind_{};
    angle_t tC_{}, wCA_{}, var_{}, mH_{}, mC_{};
    distance_t distance_{1 * u_NM}, distanceSum_{};
    duration_t preClimb_, legDuration_{}, postDescend_, durationSum_{};
  private:
    Leg(CP_OR_FIELD checkpoint, true_altitude_t secure_altitude, true_altitude_t altitude, wind_t wind, angle_t tc,
        angle_t var, distance_t distance);
    /*! this function operates on temporary sub-legs or on pure cruising legs only */
    enum class SubLegType { Climb, CruiseDescend };
    void recalc(true_altitude_t, true_altitude_t, Aircraft const &, SubLegType);
    static Leg addUp(Leg const &, Leg const &);
};

class FlightPath {
  public:
    static constexpr size_t ALTINDEX = std::numeric_limits<size_t>::max();
    FlightPath();
    FlightPath(AIRFIELD_SHARED origin, AIRFIELD_SHARED destination);
    [[deprecated]] FlightPath(std::filesystem::path const &path, Aircraft const &);
    FlightPath(boost::json::value const &, Aircraft const &);
    [[nodiscard]] AIRFIELD_SHARED origin() const;
    void origin(AIRFIELD_SHARED o_rigin);
    [[nodiscard]] size_t size() const noexcept;
    [[nodiscard]] Leg const &leg(std::optional<size_t> index = std::nullopt) const;
    [[nodiscard]] Leg &leg(std::optional<size_t> index = std::nullopt);
    void deleteLeg(size_t index);
    [[nodiscard]] bool isLast(size_t index) const;
    void insert(Leg const &l_eg, std::optional<size_t> index = std::nullopt);
    [[nodiscard]] Leg const &alternate() const;
    [[nodiscard]] Leg &alternate();
    void alternate(Leg a_lternate);
    void alternate(AIRFIELD_SHARED a_lternate);
    void recalc(Aircraft const &, std::optional<size_t> index = std::nullopt);
    [[nodiscard]] duration_t duration() const;
    [[nodiscard]] duration_t climbDuration() const;
    [[nodiscard]] duration_t alternateDuration() const;
    //    void store(std::filesystem::path const &path) const;
    [[nodiscard]] boost::json::object jsonise() const;
  private:
    AIRFIELD_SHARED origin_;
    std::vector<Leg> path_;
    Leg alternate_;
};