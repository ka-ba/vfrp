#pragma once
#include <boost/json/value_from.hpp>
#include <boost/json/value_to.hpp>
#include "vfrp-types.h"

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, wind_t const &wind);
wind_t tag_invoke(boost::json::value_to_tag<wind_t> const &, boost::json::value const &value);

namespace mp_units {
void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, speed_t const &speed);
speed_t tag_invoke(boost::json::value_to_tag<speed_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, duration_t const &time);
duration_t tag_invoke(boost::json::value_to_tag<duration_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, distance_t const &distance);
distance_t tag_invoke(boost::json::value_to_tag<distance_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, amount_t const &amount);
amount_t tag_invoke(boost::json::value_to_tag<amount_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, true_altitude_t const &altitude);
true_altitude_t tag_invoke(boost::json::value_to_tag<true_altitude_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, angle_t const &);
angle_t tag_invoke(boost::json::value_to_tag<angle_t> const &, boost::json::value const &value);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &, pressure_t const &);
pressure_t tag_invoke(boost::json::value_to_tag<pressure_t> const &, boost::json::value const &);

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &, temperature_t const &);
temperature_t tag_invoke(boost::json::value_to_tag<temperature_t> const &, boost::json::value const &);
}  // namespace mp_units

namespace std::chrono  // boost::json should work as well
{
// void tag_invoke(boost::json::value_from_tag const&, boost::json::value &value, minutes const &minu);
// minutes tag_invoke(boost::json::value_to_tag<minutes> const&, boost::json::value const &value);
}
