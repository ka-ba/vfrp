#pragma once
#include <cmath>
#include <cstdint>
#include <numbers>
// from issue#447 solution:
#include <mp-units/math.h>
#include <mp-units/ostream.h>
#include <mp-units/systems/si/si.h>
// issue#447 end
#include <stdexcept>
#include <mp-units/systems/international/international.h>
#include <mp-units/systems/isq/mechanics.h>
#include <mp-units/systems/isq/thermodynamics.h>

constexpr mp_units::Unit auto u_ft  = mp_units::international::foot;
constexpr mp_units::Unit auto u_m   = mp_units::si::metre;
constexpr mp_units::Unit auto u_NM  = mp_units::international::nautical_mile;
constexpr mp_units::Unit auto u_min = mp_units::non_si::minute;
constexpr mp_units::Unit auto u_h   = mp_units::non_si::hour;
constexpr mp_units::Unit auto u_kt  = mp_units::international::knot;
constexpr mp_units::Unit auto u_l   = mp_units::non_si::litre;
constexpr mp_units::Unit auto u_kHz = mp_units::si::kilo<mp_units::si::hertz>;
constexpr mp_units::Unit auto u_rad = mp_units::si::radian;
constexpr mp_units::Unit auto u_deg = mp_units::si::degree;
constexpr mp_units::Unit auto u_hPa = mp_units::si::hecto<mp_units::si::pascal>;
constexpr mp_units::Unit auto u_cel = mp_units::si::degree_Celsius;

// lengths
constexpr mp_units::Reference auto r_NM_dist = mp_units::isq::distance[u_NM];
using distance_t                             = mp_units::quantity<r_NM_dist, uint16_t>;

using length_t = mp_units::quantity<mp_units::isq::length[u_m], uint16_t>;

constexpr mp_units::Reference auto r_ft_talt = mp_units::isq::altitude[u_ft];
inline constexpr struct mean_sea_level : mp_units::absolute_point_origin<mp_units::isq::altitude> {
} mean_sea_level;  // from doc lib v2
// FIXME: maybe also add flightlevel, but beware of different origin!
using true_altitude_t = mp_units::quantity_point<r_ft_talt, mean_sea_level, int32_t>;
std::ostream &operator<<(std::ostream &out,
                         true_altitude_t palt);  // defined in util.cpp
// also used for elevation
using elevation_t = true_altitude_t;

inline constexpr auto pressure_altitude_spec = mp_units::isq::height;
constexpr mp_units::Reference auto r_ft_palt = pressure_altitude_spec[u_ft];
inline constexpr struct pressure_level_1013 : mp_units::absolute_point_origin<pressure_altitude_spec> {
} pressure_level_1013;
using pressure_altitude_t = mp_units::quantity_point<r_ft_palt, pressure_level_1013, int32_t>;
std::ostream &operator<<(std::ostream &out, pressure_altitude_t palt);
// add FL as pressure_altitude_spec[hecto-ft] ??

// auto zehn = true_altitude_t{10*u_ft+mean_sea_level};
// pressure_altitude_t diez = zehn;   fails as should do

inline constexpr auto density_altitude_spec  = mp_units::isq::height;
constexpr mp_units::Reference auto r_ft_dalt = density_altitude_spec[u_ft];
inline constexpr struct icao_std_atmo : mp_units::absolute_point_origin<density_altitude_spec> {
} icao_std_atmo;
using density_altitude_t = mp_units::quantity_point<r_ft_dalt, icao_std_atmo, int32_t>;
std::ostream &operator<<(std::ostream &out, density_altitude_t palt);

// time
constexpr mp_units::Reference auto r_min_dur = mp_units::isq::duration[u_min];
using duration_t                             = mp_units::quantity<r_min_dur, uint16_t>;

// speeds
constexpr mp_units::Reference auto r_h_dur = mp_units::isq::duration[u_h];
using speed_t                              = mp_units::quantity<r_NM_dist / r_h_dur, uint16_t>;

// TODO: vertical speed done differently in units example1
constexpr mp_units::Unit auto u_ft_per_min = u_ft / u_min;
using speed_vert_t                         = mp_units::quantity<r_ft_talt / r_min_dur, uint16_t>;

// volume
constexpr mp_units::Reference auto r_l_vol = mp_units::isq::volume[u_l];
using amount_t                             = mp_units::quantity<r_l_vol, uint16_t>;

// consumption
QUANTITY_SPEC(consumption_spec, mp_units::isq::volume / mp_units::isq::duration);
constexpr mp_units::Unit auto u_l_per_h = u_l / u_h;
using consumption_t                     = mp_units::quantity<consumption_spec[u_l_per_h], double>;

// angles
struct radian_base_t final {
    using value_type = double;
    value_type value{};
    constexpr radian_base_t() = default;
    constexpr explicit radian_base_t(double const r_ad) : value(confineInRange(r_ad)) {}
    static constexpr value_type confineInRange(value_type unbound) {
        // std::fmod not constexpr; TODO: consider std::remainder when switching to c++23
        constexpr auto two_pi = 2. * std::numbers::pi;
        if( unbound >= two_pi ) unbound -= std::floor(unbound / two_pi) * two_pi;
        if( unbound < 0. ) unbound += std::ceil(unbound / -two_pi) * two_pi;
        return unbound;
    }
    auto operator<=>(radian_base_t const &) const = default;
    constexpr radian_base_t operator/(double const rhs) const { return radian_base_t{value / rhs}; }
    constexpr radian_base_t operator-(radian_base_t const rhs) const { return radian_base_t{value - rhs.value}; }
    constexpr radian_base_t operator+(radian_base_t const rhs) const { return radian_base_t{value + rhs.value}; }
    constexpr explicit(false) operator double() const { return value; }
};
constexpr radian_base_t operator*(radian_base_t const lhs, double const rhs) {
    return radian_base_t{lhs.value * rhs};
}
constexpr radian_base_t operator*(double const lhs, radian_base_t const rhs) {
    return radian_base_t{lhs * rhs.value};
}
template<> inline constexpr bool mp_units::is_scalar<radian_base_t> = true;
// template<>
// inline constexpr bool mp_units::treat_as_floating_point<radian_base_t> = true;
//  TODO: make difference between course and heading - or between true vs magnetic? (only angle_t)
using angle_t                                                       = mp_units::quantity<u_rad, radian_base_t>;

struct degree_base_t final {
    using value_type = double;  // big troubles when this becomes an integral type
    value_type value{};
    constexpr degree_base_t() = default;
    template<typename NUM>
        requires std::integral<NUM>
    constexpr explicit degree_base_t(NUM const d_eg) : value(confineInRange(static_cast<long long>(d_eg))) {}
    template<typename NUM>
        requires std::floating_point<NUM>
    constexpr explicit degree_base_t(NUM const d_eg) : value(confineInRange(static_cast<long double>(d_eg))) {}
    static constexpr value_type confineInRange(long double unbound) {
        using std::round;
        // std::fmod not constexpr
        constexpr auto threesixty = 360.L;
        if( unbound >= threesixty ) unbound -= std::floor(unbound / threesixty) * threesixty;
        if( unbound < 0. ) unbound += std::ceil(unbound / -threesixty) * threesixty;
        return static_cast<value_type>(round(unbound));
    }
    static constexpr value_type confineInRange(long long unbound) {
        constexpr auto threesixty = 360LL;
        if( unbound >= threesixty ) unbound -= (unbound / threesixty) * threesixty;
        if( unbound < 0. ) unbound += (unbound / -threesixty) * threesixty;
        return static_cast<value_type>(unbound);
    }
    auto operator<=>(degree_base_t const &) const = default;
    constexpr degree_base_t operator/(double const rhs) const { return degree_base_t{value / rhs}; }
    constexpr degree_base_t operator-(degree_base_t const rhs) const { return degree_base_t{value - rhs.value}; }
    constexpr degree_base_t operator+(degree_base_t const rhs) const { return degree_base_t{value + rhs.value}; }
    constexpr explicit(false) operator double() const { return value; }
};
constexpr degree_base_t operator*(degree_base_t const lhs, double const rhs) {
    return degree_base_t{lhs.value * rhs};
}
constexpr degree_base_t operator*(double const lhs, degree_base_t const rhs) {
    return degree_base_t{lhs * rhs.value};
}
template<> inline constexpr bool mp_units::is_scalar<degree_base_t> = true;
// template<>
// inline constexpr bool mp_units::treat_as_floating_point<degree_base_t> = true;
using deg_t                                                         = mp_units::quantity<u_deg, degree_base_t>;

// frequencies
struct radio_kHz_base_t {
    using value_type = uint32_t;
    value_type freq_{MIN};
    constexpr radio_kHz_base_t() : radio_kHz_base_t(MIN) {}
    explicit constexpr radio_kHz_base_t(value_type freq) : freq_(freq) {
        if( (MIN > freq_) || (freq_ > MAX) ) throw std::invalid_argument("radio frequency out of allowed range");
    }
    explicit constexpr radio_kHz_base_t(long freq) : freq_(static_cast<value_type>(freq)) {}
    auto operator<=>(radio_kHz_base_t const &) const = default;
    // scaling operators required by mp_units for a base type:
    constexpr radio_kHz_base_t operator*(value_type const rhs) const { return radio_kHz_base_t{freq_ * rhs}; }
    constexpr radio_kHz_base_t operator/(value_type const rhs) const { return radio_kHz_base_t{freq_ / rhs}; }
    [[nodiscard]] constexpr value_type freq() const { return freq_; }
    static constexpr value_type MIN{117975};
    static constexpr value_type MAX{137000};
};
template<> inline constexpr bool mp_units::is_scalar<radio_kHz_base_t> = true;
template<> struct mp_units::quantity_values<radio_kHz_base_t> {
    static constexpr radio_kHz_base_t min() noexcept { return radio_kHz_base_t{radio_kHz_base_t::MIN}; }
    static constexpr radio_kHz_base_t max() noexcept { return radio_kHz_base_t{radio_kHz_base_t::MAX}; }
};
using freq_t = mp_units::quantity<mp_units::isq::frequency[u_kHz], radio_kHz_base_t>;

// pressure
constexpr mp_units::Reference auto r_hPa_p = mp_units::isq::pressure[u_hPa];
using pressure_t                           = mp_units::quantity<r_hPa_p, uint16_t>;

// temperature
constexpr mp_units::Reference auto r_cel_temp = mp_units::isq::Celsius_temperature[u_cel];
using temperature_t                           = mp_units::quantity<r_cel_temp, int16_t>;

namespace icao {
// https://de.wikipedia.org/wiki/Normatmosph%C3%A4re#Luftfahrt
constexpr pressure_t pressure_msl{1013 * u_hPa};  // ignoring .25
constexpr auto pressure_step{mp_units::quantity<mp_units::isq::height[u_ft]>{30 * u_ft} / pressure_t{1 * u_hPa}};
constexpr temperature_t temperature_msl{15 * u_cel};
constexpr auto temperature_gradient{temperature_t{2 * u_cel}
                                    / mp_units::quantity<mp_units::isq::height[u_ft]>{1000 * u_ft}};
constexpr auto temperature_step{mp_units::quantity<mp_units::isq::height[u_ft]>{120 * u_ft} / temperature_t{1 * u_cel}};
}  // namespace icao

// composite

struct wind_t {
    angle_t angle;
    speed_t speed;
    auto operator<=>(wind_t const &) const = default;
};

//// more basic math
template<auto RL, typename RepL, auto RR, typename RepR>
[[nodiscard]] constexpr auto min(mp_units::quantity<RL, RepL> const &lhs,
                                 mp_units::quantity<RR, RepR> const &rhs) noexcept
    requires requires { lhs < rhs; }
{
    return lhs < rhs ? lhs : rhs;
}
template<auto RL, typename RepL, auto RR, typename RepR>
[[nodiscard]] constexpr auto max(mp_units::quantity<RL, RepL> const &lhs,
                                 mp_units::quantity<RR, RepR> const &rhs) noexcept
    requires requires { lhs < rhs; }
{
    return lhs < rhs ? rhs : lhs;
}

inline pressure_altitude_t convert_altitude(true_altitude_t const talt, pressure_t const pressure) {
    auto above_1013 = talt.quantity_from(mean_sea_level) + (icao::pressure_msl - pressure) * icao::pressure_step;
    return pressure_altitude_t{pressure_level_1013 + mp_units::value_cast<pressure_altitude_t::rep>(above_1013)};
}

inline true_altitude_t convert_altitude(pressure_altitude_t const palt, pressure_t const pressure) {
    auto above_msl = palt.quantity_from(pressure_level_1013) - (icao::pressure_msl - pressure) * icao::pressure_step;
    return true_altitude_t{mean_sea_level + mp_units::value_cast<true_altitude_t::rep>(above_msl)};
}

inline density_altitude_t convert_altitude(pressure_altitude_t const palt, temperature_t const temperature) {
    auto temp_diff =
        temperature - (icao::temperature_msl - palt.quantity_from(pressure_level_1013) * icao::temperature_gradient);
    auto in_std_atmo = palt.quantity_from(pressure_level_1013) + temp_diff * icao::temperature_step;
    return density_altitude_t{icao_std_atmo + mp_units::value_cast<density_altitude_t::rep>(in_std_atmo)};
}