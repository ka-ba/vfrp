#pragma once

#include <wx/app.h>
class TopFrame;

class VFRpApp : public wxApp
{
public:
    bool OnInit() override;
    bool OnExceptionInMainLoop() override;
private:
    TopFrame *topFrame{};
};
