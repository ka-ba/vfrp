#pragma once
#include "vfrp-types.h"

// quite trivial function, but testable this way - worth the effort?
constexpr angle_t calcMC(angle_t const tc, angle_t const var) {
    return tc-var;
}

angle_t calcWCA(angle_t tc, wind_t,speed_t tas);

constexpr angle_t calcMH(angle_t const mc, angle_t const wca) {
    return mc+wca;
}

speed_t calcGS(angle_t tc, angle_t wca, speed_t tas, wind_t);