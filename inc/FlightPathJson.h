#pragma once
#include <boost/json/value_from.hpp>
#include <boost/json/value_to.hpp>
#include "vfrp-types.h"
class Leg;

/** intermediate struct to transit between Leg and json */
struct JsonLeg {
    explicit JsonLeg(Leg const &);
    JsonLeg(std::string checkpoint, bool is_field, true_altitude_t secure, true_altitude_t altitude, wind_t, angle_t tC,
            angle_t var, distance_t);
    [[nodiscard]] operator Leg() const;  // implicit
    std::string checkpoint_, notes_;
    bool isField_;
    true_altitude_t secureAltitude_{}, altitude_{mean_sea_level + 1 * r_ft_talt};
    wind_t wind_{};
    angle_t tC_{}, var_{};
    distance_t distance_{};
};

void tag_invoke(boost::json::value_from_tag const &, boost::json::value &value, Leg const &leg);
Leg tag_invoke(boost::json::value_to_tag<Leg> const &, boost::json::value const &value);