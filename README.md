# *vfrp*

is short for "VFR planning". It's a tool to ease parts of the planning for a VFR flight. 

## Description

Planning a VFR flight in detail can be time-consuming. *vfrp* aims to support part of this process, especially for repeated flights.
Parts that get tackled are: route and altitudes, fuel planning and a nice print-out that also includes information about the airfields.
![front page of printout](doc/shot-front.png "front page of printout")
![back page of printout](doc/shot-back.png "back page of printout")

Your databases of airfields and aircraft you have to create and maintain yourselves. Also, of course, you have to create your routes - when you plan each for the first time. But from then on you can re-use your plannings when you want to fly the same route the second time.

The idea for contents and layout is based on the "Flugdurchführungsplan" that was published by LBA, which you can find [here](https://www.segelfliegengrundausbildung.de/images/ppts/Formular%20Flugdurchfuehrungsplan_LBA.pdf "a secondary link to the Flugdurchführungsplan LBA"), for example.
[This german document](http://www.pilotundrecht.de/TEXTE/DOWNLOAD/FSM/fsm_87_1_3_auflage.pdf "filled in Flugdurchführungsplan with explanation") gives a short rationale and a elaborate example of a planning, using the document.

# Disclaimer

Usage of *vfrp* doesn't relieve any pilot of conducting a thorough planning as required by law. The data you see in the GUI or the print-out are indications at best that you might compare with your own detailed planning to do a cross-check.

There is no guarantee that any values in *vfrp* are accurate. The PIC retains sole responsibility for her/his flight.

# early

This is some fairly early state of development. While the tool runs quite stable locally for me, that might not be the case everywhere. If you want to have PDF print-outs, currently you have to compile the `pdf` branch (the print-out you will find in `/tmp/`).
The GUI currently exhibits a liberal mixture of english and german terms.

# Credits

As most decent sized software projects, *vfrp* is standing on the shoulders of giants. The names of the giants are
- the famous [**boost** C++ libraries](https://www.boost.org/). *vfrp* uses [Boost.JSON](https://www.boost.org/doc/libs/1_80_0/libs/json/), [Boost.Log](https://www.boost.org/doc/libs/1_80_0/libs/log/doc/html/) and [Boost.Test](https://www.boost.org/doc/libs/1_80_0/libs/test/doc/html/)
- the exciting [**mp-units** library](https://mpusz.github.io/mp-units/2.0/) that offers a comprehensive system of strong types for physical quantities (when you divide 100 nautical miles by 2 hours, you get 50 kts - including the units)
- the rock-solid [**PDF-Writer** library](https://www.pdfhummus.com/) that produces the PDF print-out

# ... for later ...

Following is the remainder of gitlabs template...


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ka-ba/vfrp/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
